# Gachou - your picture album

![logo](https://gitlab.com/gachou/artwork/-/raw/master/svg/gachou-logo.svg)

The goal of this project is management of personal photos and videos.
It is in very early stages of development. Very.. early...

## Links

- [Documentation](https://gachou.knappi.org)
- [Contributing guide](./CONTRIBUTING.md)
- [My personal blog](https://blog.knappi.org) also contains some
  background information about it.
