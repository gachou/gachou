import { Plugin } from "vite";
import cp from "child_process";

export function injectVersion(): Plugin {
  const virtualModuleId = "virtual:version";
  const resolvedVirtualModuleId = "\0" + virtualModuleId;

  return {
    name: "inject-version",
    resolveId(id) {
      if (id === virtualModuleId) {
        return resolvedVirtualModuleId;
      }
    },
    load(id) {
      if (id === resolvedVirtualModuleId) {
        return `export const gitCommitId = ${run("git rev-parse HEAD")}`;
      }
    },
  };
}

function run(command: string): string {
  const result = cp.execSync(command, {
    encoding: "utf-8",
  });
  return JSON.stringify(result);
}
