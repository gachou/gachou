import { Plugin } from "vite";
import fs from "fs";
import path from "path";

const configDir = path.resolve(__dirname, "..", "deploy-profiles");

/**
 * Make sure that a specified config is loaded and placed in the output directory
 * under `gachou-config.js`. This allows us to replace this file with
 * other contents, e.g. in the docker-setup
 */
export function injectDeployConfig(configName = "default-config"): Plugin {
  const pathToConfigFile = resolveDeployConfigFile(configName);

  return {
    name: "load-deploy-config",
    config() {
      return {
        resolve: {
          alias: {
            "deploy-config.js": pathToConfigFile,
          },
        },
        build: {
          rollupOptions: {
            output: [
              {
                manualChunks(chunkId) {
                  if (chunkId === pathToConfigFile) {
                    return "deploy-config";
                  }
                },
                chunkFileNames(chunk) {
                  return chunk.name === "deploy-config"
                    ? "[name].js"
                    : "assets/[name].[hash].js";
                },
              },
            ],
          },
        },
      };
    },
  };
}

function resolveDeployConfigFile(configName: string): string {
  const configFile = path.resolve(configDir, `${configName}.js`);

  if (fs.existsSync(configFile)) {
    // eslint-disable-next-line no-console
    console.log("Using config file: " + configFile);
    return configFile;
  }

  const existingNames = findExistingConfigNames().join(", ");
  throw new Error(
    `Config "${configName}" not found, possible values: ${existingNames}`
  );
}

function findExistingConfigNames(): string[] {
  return fs
    .readdirSync(configDir)
    .filter((name) => name.endsWith(".js"))
    .map((config) => config.replace(/\.js$/, ""));
}
