import { Plugin, ResolvedConfig } from "vite";
import fs from "fs";
import path from "path";
import { promisify } from "util";

const copyFile = promisify(fs.copyFile);

const MOCK_SERVICE_WORKER_FILE = "mockServiceWorker.js";

export function copyMockServiceWorker(): Plugin {
  let config: ResolvedConfig | null = null;

  return {
    name: "copy-mock-service-worker",
    apply: (config, { command }) => command === "build",
    configResolved(resolvedConfig) {
      config = resolvedConfig;
    },
    async writeBundle() {
      assertConfigNotNull(config);
      await copyFile(
        path.resolve(config.root, MOCK_SERVICE_WORKER_FILE),
        path.resolve(config.root, config.build.outDir, MOCK_SERVICE_WORKER_FILE)
      );
    },
    closeBundle() {
      assertConfigNotNull(config);
      config.logger.info("✓ Added Mock-Service-Worker");
    },
  };
}

function assertConfigNotNull<T>(config: T | null): asserts config is T {
  if (config == null) {
    throw new Error("Config is null, this should never happen!");
  }
}
