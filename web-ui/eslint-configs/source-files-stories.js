module.exports = {
  files: "src/**/*.stories.+(ts|tsx)",
  env: { jest: true },
  plugins: ["storybook"],
  extends: ["plugin:storybook/recommended"],
};
