module.exports = {
  parser: "@typescript-eslint/parser",
  parserOptions: {
    project: require.resolve("../tsconfig.json"),
  },
  plugins: [
    "@typescript-eslint/eslint-plugin",
    "react-hooks",
    "eslint-plugin-local-rules",
  ],
  extends: [
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:react-hooks/recommended",
  ],
  rules: {
    "@typescript-eslint/await-thenable": "error",
    "no-restricted-properties": [
      "error",
      {
        object: "window",
        property: "API_BASE_URL",
        message: "Use 'src/constants/web-ui-config.ts' to access deploy config",
      },
      {
        object: "window",
        property: "location",
        message: "Use functions in 'src/components/navigation' instead",
      },
      {
        object: "document",
        property: "location",
        message: "Use functions in 'src/components/navigation' instead",
      },
    ],
    "local-rules/resolve-routes-type": "error",
    "@typescript-eslint/no-unused-vars": [
      "error",
      { varsIgnorePattern: "^[iI]gnored" },
    ],
    "no-restricted-imports": [
      "error",
      {
        name: "react-toastify",
        message: "Use 'src/utils/toasts.tsx' instead",
      },
      {
        name: "react-bootstrap",
        message: "Use 'src/utils/bootstrap.tsx' instead",
      },
    ],
  },
};
