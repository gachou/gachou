module.exports = {
  parser: "@typescript-eslint/parser",
  parserOptions: {
    project: require.resolve("../tsconfig.node.json"),
  },
  env: {
    node: true,
  },
};
