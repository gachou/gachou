module.exports = {
  env: { jest: true },
  plugins: ["testing-library"],
  extends: ["plugin:testing-library/react"],
  rules: {
    "no-restricted-properties": ["warn", ...skippedAndExclusiveTests()],
    // In unit tests, we generally don't want 'require-await' because callbacks
    // that are just added for testing, may be required to be async, but not necessarily
    // do async work. We don't want to add extra code, just to satisfy the rule
    "require-await": "off",
  },
};

function skippedAndExclusiveTests() {
  return ["test", "it", "describe"].flatMap((object) => {
    return ["only", "skip"].map((property) => {
      return {
        object,
        property,
        message:
          "Tests should only be skipped during development, not checked in.",
      };
    });
  });
}
