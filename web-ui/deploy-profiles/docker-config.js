/* global Promise */
// This is a dummy file that is copied to the docker image.
Promise.reject(
  new Error(`
If you see this message, then something with the docker-setup
did not work correctly.
-----
The file docker-config.js should never be loaded and delivered to the user.

There should be a 'respond' directive in the Caddyfile that intercepts requests 
to this file and delivers the configuration from the $GACHOU_FRONTEND_CONFIG
environment variable
  `)
);
