const { mergeConfig } = require("vite");
const { default: svgr } = require("@honkhonk/vite-plugin-svgr");

const path = require("path");

const srcDir = path.resolve(__dirname, "..", "src");

module.exports = {
  stories: [
    {
      directory: "../src/components/icons",
      titlePrefix: "Icons",
      files: "**/*.stories.tsx",
    },
    {
      directory: "../src/components/atoms",
      titlePrefix: "Atoms",
      files: "**/*.stories.tsx",
    },
    {
      directory: "../src/components/molecules",
      titlePrefix: "Molecules",
      files: "**/*.stories.tsx",
    },
    {
      directory: "../src/utils",
      titlePrefix: "Utils",
      files: "**/*.stories.tsx",
    },
  ],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
  ],
  framework: "@storybook/react",
  core: {
    builder: "@storybook/builder-vite",
  },
  features: {
    storyStoreV7: true,
  },
  viteFinal(config) {
    return mergeConfig(config, {
      plugins: [svgr()],
      resolve: {
        alias: {
          src: srcDir,
        },
      },
    });
  },
};
