# gachou-web-ui

Web-Frontend for the Gachou picture album.

## Usage

### Docker-build

There is a docker-build that is created with each pipeline. The image can be downloaded from the local
docker-registry at https://gitlab.com/gachou/gachou-web-ui/-/packages

The build contains a `Caddyfile` with non-https configuration, and the compiled files of the web-ui.
The supposed use case is to have another reverse-proxy in front of the Caddy, which can then do https.

But you can also build a new docker-image based on this one, and replace the `Caddyfile`

### Configuring the backend location (API_BASE_URL, TUS_UPLOAD_URL)

A configuration file `deploy-config.js` in the same folder as "index.html" is read by the application.
It adds properties `API_BASE_URL` and `TUS_UPLOAD_URL` to the `window` object to inject the configuration.

The directory [deploy-profiles/](deploy-profiles/) contains predefined profiles
used in the build.

Example1: Connect to a Gachou backend at `localhost:8080`:

```js
window.API_BASE_URL = "http://localhost:8080";
window.TUS_UPLOAD_URL = "http://localhost:8080/files";
```

Example 2: Use a fake-api mocked with [mock-service-worker](http://mswjs.io):

```js
window.API_BASE_URL = "mock-api://";
```

The docker-build has a Caddy rule that intercepts requests to `deploy-config.js`
and injects the value of the environment variable `API_BASE_URL` instead.

## Developing

This project uses

- [yarn 3](https://yarnpkg.com) as package manager
- [vite](https://vitejs.dev) as build-system
- [react](https://reactjs.org/) as UI framework
- [bootstrap](https://getbootstrap.com/) and [react-bootstrap](https://react-bootstrap.github.io/) as component library
- [vitest](https://vitest.dev/) and [react-testing-library](https://testing-library.com/docs/react-testing-library/intro/) for unit/integration tests

### Install dependencies

```
corepack enable
```

```
yarn
```

### Development mode

The following command runs

- the dev-server
- the unit-tests in watch-mode
- the TypeScript compiler in watch-mode

```
yarn dev
```

Dev-Server with Mock-Backend (using msw.js)

```
VITE_MOCK_API=true yarn dev:server
```

### Rebuild openapi-client from backend in Quarkus development mode (http://localhost:8080/)

(backend must run in `quarkus dev`)

```
yarn generate:openapi
```

### Run tests, typechecks, lint, check format (like in ci-pipeline)

```
yarn test
```

### Run code-formatter

```
yarn format
```

(the code formatter and linter are also run on pre-commit using (husky and lint-staged))

## TypeScript configuration

- `src/tsconfig.json` contains config for files in the "src/"-directory
- `tsconfig.json` contains config for files not in the "src/"-directory

Both projects are unrelated. In an earlier version there was a `tsconfig.node.json`
and a `tsconfig.json`, both in the root directory. This configuration
suddenly didn't work anymore. My IDE seemed not to recognize that
`vite.config.ts` is coverery by `tsconfig.node.json`. The current
configuration seems to be more robust.
