module.exports = function extractRoutePatterns(node, context) {
  if (node.typeAnnotation.type !== "TSUnionType") {
    return context.report({
      node: node,
      message: node.id.name + " must be a union type",
    });
  }

  const result = [];
  for (const item of node.typeAnnotation.types) {
    if (isLiteralType(item) && hasStringValue(item)) {
      result.push(item.literal.value);
    } else {
      context.report({
        node: item,
        message: "RoutePattern must contain string literals",
      });
    }
  }
  return result;
};

function isLiteralType(node) {
  return node.type === "TSLiteralType";
}

function hasStringValue(node) {
  return typeof node.literal.value === "string";
}
