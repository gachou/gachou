module.exports = function buildResolveRoutesType(id, routePatterns) {
  return `interface ${id} {
    ${routePatterns.map(createResolveSignatureForPattern).join("\n")}
  }`;
};

function createResolveSignatureForPattern(pattern) {
  const params = pattern.match(/:\w+/g);
  let patternType = JSON.stringify(pattern);
  if (params == null) {
    return `(path: ${patternType}): string;`;
  }
  return `(path: ${patternType}, params: ${createParamType(params)}): string;`;
}

function createParamType(params) {
  return `Record<${createParamNameUnionType(params)}, string | number>`;
}

function createParamNameUnionType(params) {
  return params
    .map((param) => param.slice(1))
    .map(JSON.stringify)
    .join(" | ");
}
