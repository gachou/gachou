const prettier = require("prettier");
const extractRoutePatterns = require("./extractRoutePatterns");
const buildResolveRoutesType = require("./buildResolveRoutesType");

module.exports = {
  meta: {
    docs: {
      description:
        "This rule is an experiment. It makes sure that the " +
        "GeneratedResolveRoute-interface is set correctly, based on the RoutePatterns " +
        "provided in the RoutePattern type",
      category: "Possible Errors",
      recommended: false,
    },
    schema: [],
    fixable: "code",
  },
  create: function (context) {
    if (!context.getFilename().endsWith("src/routes/routes.ts")) {
      return {};
    }
    let routePatterns = [];
    return {
      TSTypeAliasDeclaration(node) {
        if (node.id.name === "RoutePattern") {
          routePatterns = extractRoutePatterns(node, context);
        }
      },
      TSInterfaceDeclaration(node) {
        const GeneratedResolveRoute = "GeneratedResolveRoute";
        if (node.id.name === GeneratedResolveRoute) {
          const currentCode = format(context.getSourceCode().getText(node));
          const expectedCode = format(
            buildResolveRoutesType(GeneratedResolveRoute, routePatterns)
          );
          if (currentCode !== expectedCode) {
            context.report({
              node,
              message: "GeneratedResolveRoute should be: " + expectedCode,
              fix: (fixer) => fixer.replaceText(node, expectedCode),
            });
          }
        }
      },
    };
  },
};

function format(code) {
  return prettier.format(code, { parser: "typescript" });
}
