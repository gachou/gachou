import { test } from "vitest";
import { act, screen, waitFor } from "@testing-library/react";
import App from "./App";
import "react";
import { customRender } from "./tests/utils/custom-render";
import { oas } from "src/tests/openapi-msw";
import { mockAnimationsApi } from "jsdom-testing-mocks";
import { publicStatus } from "src/global-state/publicBackendStatus";
import { selectAuthenticationAdapter } from "src/adapter/authentication";
import createJwtUserPasswordPlugin from "src/adapter/authentication/jwtUserPassword";
import { setupTestMockApi } from "src/tests/mock-backend/setup-test-mock-api";

mockAnimationsApi();
selectAuthenticationAdapter(createJwtUserPasswordPlugin());

const { useHandlers } = setupTestMockApi(
  oas.get("/api/status/public", (req, res, context) => {
    return res(context.json({ adminUserExists: true }));
  })
);

describe("the app", () => {
  test("should contain the logo", () => {
    customRender(<App />);

    expect(screen.getByTestId("gachou-logo")).toBeInTheDocument();
  });

  it("shows the error footer if and only if  the public backend status cannot be loaded", async () => {
    useHandlers(
      oas.get("/api/status/public", (req, res, context) => {
        return res(context.status(500));
      })
    );

    customRender(<App />);
    await act(() => publicStatus.updateNow());

    expect(screen.getByTestId("error-footer")).toHaveTextContent(
      "Error loading data from backend"
    );

    useHandlers(
      oas.get("/api/status/public", (req, res, context) => {
        return res(context.json({ adminUserExists: true }));
      })
    );

    await act(() => publicStatus.updateNow());

    await waitFor(() => {
      expect(screen.queryByTestId("error-footer")).toBeNull();
    });
  });
});
