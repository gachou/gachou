export type ValidKeys =
  | "gachou.auth.admin-user.encrypted-password"
  | "gachou.auth.admin-user.username";
export type ValidEnvVars =
  | "GACHOU_AUTH_ADMIN_USER_ENCRYPTED_PASSWORD"
  | "GACHOU_AUTH_ADMIN_USER_USERNAME";
