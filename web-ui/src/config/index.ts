import {
  ValidEnvVars,
  ValidKeys,
} from "src/config/__generated__/backend-config-schema";

export function backendConfigKey(prop: ValidKeys): ValidKeys {
  return prop;
}
export function backendConfigEnvVar(envVar: ValidEnvVars): ValidEnvVars {
  return envVar;
}
