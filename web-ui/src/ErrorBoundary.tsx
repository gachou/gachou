import React, { ReactNode } from "react";

interface Props {
  children: ReactNode;
}

export class ErrorBoundary extends React.Component<
  Props,
  { error: Error | null }
> {
  constructor(props: Props) {
    super(props);
    this.state = { error: null };
  }

  static getDerivedStateFromError(error: Error) {
    // Update state so the next render will show the fallback UI.
    return { error };
  }

  // In this place, we could have a "componentDidCatch" method to
  // log the error somewhere else, but currently we would only log it to the
  // console anyway, and react already does this by itself, so we skip it for now.

  render() {
    if (this.state.error != null) {
      // You can render any custom fallback UI
      return (
        <div>
          <h1>Something went wrong.</h1>
          <p>{this.state.error.toString()}</p>
        </div>
      );
    }

    return this.props.children;
  }
}
