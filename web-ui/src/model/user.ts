import { VRecord, Static, VString, VArray } from "src/utils/validation";
import { Role } from "src/model/role";

export const User = VRecord({
  username: VString,
  roles: VArray(Role),
});

export type User = Static<typeof User>;

export const UserRequest = VRecord({
  username: VString,
  password: VString,
});

export type UserRequest = Static<typeof UserRequest>;
