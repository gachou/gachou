import { VLiteral, Static, VUnion } from "src/utils/validation";

export const Role = VUnion(VLiteral("USER"), VLiteral("ADMIN"));
export type Role = Static<typeof Role>;
