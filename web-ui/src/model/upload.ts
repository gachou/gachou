import {
  VArray,
  VBoolean,
  VLiteral,
  VNumber,
  VOptional,
  VRecord,
  Static,
  VString,
  VUnion,
} from "src/utils/validation";

export const Audit = VRecord({
  createdBy: VString,
  createdAt: VString,
  updatedBy: VString,
  updatedAt: VString,
});

export const Upload = VRecord({
  id: VString,
  fileName: VString,
  fileMimeType: VOptional(VString),
  fileLastModified: VString,
  totalSize: VNumber,
  uploadedSize: VNumber,
  state: VUnion(VLiteral("RUNNING"), VLiteral("DONE")),
  audit: VOptional(Audit),
  activeOnClient: VOptional(VBoolean, { defaultValue: false }),
});

export const Uploads = VArray(Upload);

export type Upload = Static<typeof Upload>;
