import React from "react";
import { fetchUserInfo } from "src/backend/userInfo/fetchUserInfo";
import { resolveRoute } from "src/routes/routes";
import { ShowApiValue } from "src/components/atoms/ShowApiValue";
import { useApi } from "src/hooks/useApi";

const Homepage: React.FC = () => {
  const userInfo = useApi(fetchUserInfo);
  return (
    <div>
      <h1>Homepage</h1>
      <ShowApiValue apiValue={userInfo}>
        {(value) => (
          <div>
            I am {value.username}, I am a {value.roles}
          </div>
        )}
      </ShowApiValue>
      <h2>What is this?</h2>
      <p>
        This page is a demo for the{" "}
        <a href={"https://gachou.knappi.org"}>Gachou project</a>, which I
        started to manage my private photos and videos.
      </p>
      <p>
        There is not a lot to see yet, since I am still in very early stages of
        development. The most interesting part may yet be the{" "}
        <a href={"https://gitlab.com/gachou/gachou"}>project setup</a>
      </p>
      <p>
        <a href={resolveRoute("/users")}>Users</a>: Here you can add an remove
        users for the mock-api
      </p>
    </div>
  );
};

export default Homepage;
