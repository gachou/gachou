import React from "react";
import { Button } from "src/utils/bootstrap";
import { IconPlus } from "src/components/icons";
import { Link } from "wouter";
import { resolveRoute } from "src/routes/routes";
import { apiUsers } from "src/backend/users/userService";
import { ShowApiValue } from "src/components/atoms/ShowApiValue";
import { UserTable } from "src/pages/UserListPage/parts/UserTable";
import { useApi } from "src/hooks/useApi";

const UserListPage: React.FC = () => {
  const userList = useApi(apiUsers.list);

  return (
    <div>
      <ShowApiValue apiValue={userList}>
        {(value) => (
          <UserTable userList={value} updateUserList={userList.reload} />
        )}
      </ShowApiValue>
      <Link href={resolveRoute("/users/new")}>
        <Button className={"me-2"}>
          <IconPlus /> Add user
        </Button>
      </Link>
      <Button onClick={() => userList.reload()}>
        <IconPlus /> Reload
      </Button>
    </div>
  );
};

export default UserListPage;
