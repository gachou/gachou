import {
  setupTestMockApi,
  requests,
} from "src/tests/mock-backend/setup-test-mock-api";
import { screen, waitFor, within } from "@testing-library/react";
import { resolveRoute } from "src/routes/routes";
import { customRenderPage } from "src/tests/utils/custom-render-page";
import userEvent from "@testing-library/user-event";
import { currentTestLocation } from "src/tests/utils/useTestLocation";
import { createUsersHandlers } from "src/tests/mock-backend/handler/users.mock-api";
import { usersMockStore } from "src/tests/mock-backend/store/users.mock-store";

setupTestMockApi(...createUsersHandlers());

async function render() {
  await customRenderPage(resolveRoute("/users"));
}

describe("UserListPage", () => {
  beforeEach(() => {
    usersMockStore.init({
      users: [
        { username: "admin", roles: ["ADMIN"] },
        { username: "alice", roles: ["USER"] },
        { username: "bob", roles: ["USER"] },
      ],
    });
  });

  it("renders the usernames", async () => {
    await render();

    const userRow = await screen.findAllByTestId("user");
    expect(userRow).toHaveLength(3);
    expect(userRow[0]).toHaveTextContent("admin");
    expect(userRow[1]).toHaveTextContent("alice");
    expect(userRow[2]).toHaveTextContent("bob");
  });

  it("renders the roles", async () => {
    await render();
    const userRow = await screen.findAllByTestId("user");
    expect(userRow).toHaveLength(3);
    expect(userRow[0]).toHaveTextContent("ADMIN");
    expect(userRow[1]).toHaveTextContent("USER");
    expect(userRow[2]).toHaveTextContent("USER");
  });

  it("redirects to the new-user page when the 'Add user' button is clicked", async () => {
    await render();
    await userEvent.click(screen.getByText("Add user"));
    await waitFor(() => {
      expect(currentTestLocation()).toEqual("/users/new");
    });
  });

  it("redirects to the edit-user page when the 'Change Password' button is clicked", async () => {
    await render();
    const userRow = await screen.findAllByTestId("user");
    expect(userRow[2]).toHaveTextContent("bob");
    const deleteButton = within(userRow[2]).getByText("Change Password", {
      exact: false,
    });
    await userEvent.click(deleteButton);
    await waitFor(() => {
      expect(currentTestLocation()).toEqual("/users/edit/bob");
    });
  });

  it("Deletes a user, when it's delete-button is clicked", async () => {
    await render();
    const userRow = await screen.findAllByTestId("user");
    expect(userRow[2]).toHaveTextContent("bob");

    const deleteButton = within(userRow[2]).getByText("Delete");
    await userEvent.click(deleteButton);

    await requests.find({ method: "DELETE", pathname: "/api/users/bob" });
    await waitFor(() => {
      expect(userRow[2]).not.toBeInTheDocument();
    });
  });
});
