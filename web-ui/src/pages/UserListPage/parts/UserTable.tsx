import { List } from "src/model/list";
import { User } from "src/model/user";
import React from "react";
import { apiUsers } from "src/backend/users/userService";
import { Button, Table } from "src/utils/bootstrap";
import { Link } from "wouter";
import { resolveRoute } from "src/routes/routes";
import { IconKey, IconTrash } from "src/components/icons";

interface UserTableProps {
  userList: List<User>;
  updateUserList: () => void;
}

export const UserTable: React.FC<UserTableProps> = ({
  userList,
  updateUserList,
}) => {
  async function remove(user: User) {
    await apiUsers.remove(user.username);
    updateUserList();
  }

  return (
    <Table className={"align-middle"}>
      <thead>
        <tr>
          <th>Username</th>
          <th>Roles</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {userList.items.map((user) => {
          return (
            <tr key={user.username} data-testid={"user"}>
              <td>{user.username}</td>
              <td>{user.roles.join(", ")}</td>
              <td width="0" className={"d-flex flex-wrap gap-2"}>
                <Link
                  href={resolveRoute("/users/edit/:username", {
                    username: user.username,
                  })}
                >
                  <Button size={"sm"}>
                    <IconKey />
                    <span className={"d-none d-md-inline ms-2"}>
                      Change password
                    </span>
                  </Button>
                </Link>
                <Button
                  size={"sm"}
                  variant={"danger"}
                  onClick={() => remove(user)}
                >
                  <IconTrash />
                  <span className={"d-none d-md-inline ms-2"}> Delete</span>
                </Button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
};
