import {
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import {
  requests,
  setupTestMockApi,
} from "src/tests/mock-backend/setup-test-mock-api";
import userEvent from "@testing-library/user-event";
import { resolveRoute } from "src/routes/routes";
import { customRenderPage } from "src/tests/utils/custom-render-page";
import { currentTestLocation } from "src/tests/utils/useTestLocation";
import { createUsersHandlers, usersMockStore } from "src/tests/mock-backend";
import { enterPasswords } from "src/tests/ui-helpers/UserCredentialsForm";

setupTestMockApi(...createUsersHandlers());

beforeEach(() => {
  usersMockStore.init({
    users: [{ username: "bob", roles: ["USER"] }],
  });
});

async function render(username: string): Promise<void> {
  await customRenderPage(resolveRoute("/users/edit/:username", { username }));
  expect(
    await waitForElementToBeRemoved(() => screen.queryByText("Loading..."))
  );
}

describe("EditUserPage", () => {
  it("shows the correct header", async () => {
    await render("bob");
    expect(await screen.findByText(`Edit user "bob"`)).toBeInTheDocument();
  });

  it("shows an error if the user does not exist", async () => {
    await render("non-existing-user");
    expect(
      await screen.findByText("User not found: non-existing-user")
    ).toBeInTheDocument();
  });

  it("shows the username field as disabled", async () => {
    await render("bob");
    expect(screen.getByLabelText("Username:")).toHaveAttribute("disabled");
  });

  it("saves the users and redirects to the user list", async () => {
    await render("bob");
    await enterPasswords("pw-bob", "pw-bob");
    await submit();
    await requests.find({
      method: "PUT",
      pathname: "/api/users/bob",
      body: { password: "pw-bob" },
    });
    await waitFor(() => {
      expect(currentTestLocation()).toEqual("/users");
    });
  });

  it("shows an error if the passwords do not match", async () => {
    await render("bob");
    await enterPasswords("pw-bob", "pw-wrong");
    await submit();

    expect(
      screen.getByTestId("user-detail--password-confirm")
    ).toHaveTextContent("Passwords do not match");
  });

  it("redirects to the user list after the user clicks 'cancel'", async () => {
    await render("bob");
    await userEvent.click(screen.getByText("Cancel", { selector: "button" }));
    await waitFor(() => {
      expect(currentTestLocation()).toEqual("/users");
    });
  });
});

async function submit() {
  await userEvent.click(screen.getByText("Save", { selector: "button" }));
}
