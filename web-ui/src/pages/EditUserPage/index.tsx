import React, { useCallback } from "react";
import { RouteComponentProps, useLocation } from "wouter";
import { apiUsers } from "src/backend/users/userService";
import { resolveRoute } from "src/routes/routes";
import {
  UserCredentialsForm,
  UserDetailsFormProps,
} from "src/components/forms/UserDetailsForm";
import { Col, Row } from "src/utils/bootstrap";
import { ShowApiValue } from "src/components/atoms/ShowApiValue";
import { useApi } from "src/hooks/useApi";

const EditUserPage: React.FC<RouteComponentProps<{ username: string }>> = ({
  params: { username },
}) => {
  const [ignoredLocation, setLocation] = useLocation();

  const onSubmit: UserDetailsFormProps["onSubmit"] = useCallback(
    async ({ username, password }, setError) => {
      try {
        await apiUsers.update({ username, password });
        setLocation(resolveRoute("/users"));
      } catch (error) {
        setError("username", { message: "User not found" });
      }
    },
    [setLocation]
  );

  const onCancel = useCallback(() => {
    setLocation(resolveRoute("/users"));
  }, [setLocation]);

  const userDetails = useApi(apiUsers.fetch, username);

  return (
    <ShowApiValue apiValue={userDetails}>
      {(userDetails) => (
        <Row>
          <Col xs={12} md={9} lg={6}>
            <p className="fw-bold fs-1">Edit user "{userDetails.username}"</p>
            <UserCredentialsForm
              defaultValues={{ username }}
              disableUsername={true}
              onSubmit={onSubmit}
              onCancel={onCancel}
              submitButtonText={"Save"}
            />
          </Col>
          <Col />
        </Row>
      )}
    </ShowApiValue>
  );
};

export default EditUserPage;
