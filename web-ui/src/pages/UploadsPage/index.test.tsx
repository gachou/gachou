import { customRenderPage } from "src/tests/utils/custom-render-page";
import { resolveRoute } from "src/routes/routes";
import { screen, within } from "@testing-library/react";
import { useUploads } from "src/hooks/useUploads";
import { Upload } from "src/model/upload";
import { Mock } from "vitest";
import userEvent from "@testing-library/user-event";

vi.mock("src/hooks/useUploads");
if (!vi.isMockFunction(useUploads)) throw new Error("Mock expected");
const useUploadsMock = useUploads;

describe("UploadsPage", () => {
  it("renders a list of uploads", async () => {
    mockUseUploads([
      createUpload({
        id: "upload1",
        fileName: "file1.png",
        totalSize: 100,
        uploadedSize: 70,
      }),
      createUpload({
        id: "upload2",
        fileName: "file2.png",
        totalSize: 123456,
        uploadedSize: 123456 / 2,
      }),
    ]);

    await customRenderPage(resolveRoute("/uploads"));
    expect(getRow(0)).toHaveTextContent(/Name.*Size.*Progress.*Actions/);
    expect(getRow(1)).toHaveTextContent(/file1.png.*100 bytes.*70%.*Cancel/);
    expect(getRow(2)).toHaveTextContent(/file2.png.*123\.456 KB.*50%.*Cancel/);
  });

  it("calls 'cancelUpload' when the cancel button is clicked", async () => {
    const upload1 = createUpload({
      id: "upload1",
      fileName: "file1.png",
      totalSize: 100,
      uploadedSize: 70,
    });
    const upload2 = createUpload({
      id: "upload2",
      fileName: "file2.png",
      totalSize: 123456,
      uploadedSize: 123456 / 2,
    });
    const { cancelUpload, reload } = mockUseUploads([upload1, upload2]);

    await customRenderPage(resolveRoute("/uploads"));
    const cancelButtons = screen.getAllByRole("button", { name: "Cancel" });
    expect(cancelButtons).toHaveLength(2);
    await userEvent.click(cancelButtons[0]);
    expect(cancelUpload).toHaveBeenCalledWith(upload1);
    expect(reload).toHaveBeenCalled();
  });

  it("renders a file input button", async () => {
    mockUseUploads([]);

    await customRenderPage(resolveRoute("/uploads"));

    expect(screen.getByText("Add files")).toBeInTheDocument();
  });
});

interface MockUploadReturn {
  reload: Mock;
  cancelUpload: Mock;
}

function mockUseUploads(uploads: Upload[]): MockUploadReturn {
  const reload = vi.fn().mockResolvedValue(undefined);
  const cancelUpload = vi.fn().mockResolvedValue(undefined);
  useUploadsMock.mockReturnValue({
    uploads: {
      reload: reload,
      error: null,
      value: uploads,
    },
    cancelUpload,
  });
  return { reload, cancelUpload };
}

function createUpload(overrides: Partial<Upload>): Upload {
  return {
    id: "upload1",
    fileName: "avatar.png",
    fileMimeType: "image/png",
    state: "RUNNING",
    fileLastModified: "2022-07-10T13:15:17Z",
    totalSize: 100,
    uploadedSize: 70,
    activeOnClient: true,
    audit: {
      createdBy: "Nils",
      createdAt: "2022-07-10T13:15:17Z",
      updatedBy: "Nils",
      updatedAt: "2022-07-10T13:15:17Z",
    },
    ...overrides,
  };
}

function getRow(index: number) {
  return within(screen.getByRole("table")).getAllByRole("row")[index];
}
