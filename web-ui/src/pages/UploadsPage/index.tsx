import React from "react";
import { uploader } from "src/global-state/uploader";
import { Button, ProgressBar, Table } from "src/utils/bootstrap";
import { IconPlus, IconTrash, IconWarning } from "src/components/icons";
import { MyFileButton } from "src/components/atoms/MyFileButton";
import { ShowApiValue } from "src/components/atoms/ShowApiValue";
import { Upload } from "src/model/upload";
import { useUploads } from "src/hooks/useUploads";

const UploadsPage: React.FC = () => {
  const { uploads, cancelUpload } = useUploads();

  async function addFilesAndReload(files: FileList) {
    uploader.addFiles(files);
    await uploads.reload();
  }

  return (
    <div>
      <ShowApiValue apiValue={uploads} className={"mb-2"}>
        {(uploads, reload) => (
          <Table>
            <thead>
              <tr>
                <th></th>
                <th>Name</th>
                <th>Size</th>
                <th>Progress</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {uploads.map((upload) => (
                <tr key={upload.id} className={"align-middle"}>
                  <td>
                    {!upload.activeOnClient && (
                      <IconWarning className={"text-danger"} />
                    )}
                  </td>
                  <td>{upload.fileName}</td>
                  <td>{humanReadableBytes(upload.totalSize)}</td>
                  <td>
                    <ProgressBar
                      max={upload.totalSize}
                      now={upload.uploadedSize}
                      label={progressAsString(upload)}
                    />
                  </td>
                  <td width="0" className={"d-flex flex-wrap gap-2"}>
                    <Button
                      variant={"danger"}
                      onClick={async () => {
                        await cancelUpload(upload);
                        reload();
                      }}
                    >
                      <IconTrash /> Cancel
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        )}
      </ShowApiValue>
      <MyFileButton multiple onChange={(files) => addFilesAndReload(files)}>
        <IconPlus /> Add files{" "}
      </MyFileButton>
    </div>
  );
};

const percentFormat = new Intl.NumberFormat([], {
  maximumFractionDigits: 2,
  useGrouping: true,
});

function progressAsString(upload: Upload) {
  const done = upload.uploadedSize;
  const total = upload.totalSize;
  return percentFormat.format((done / total) * 100) + "%";
}

function humanReadableBytes(bytes: number) {
  return (
    tryHumanReadableBytes(bytes, 1000 * 1000 * 1000, "GB") ??
    tryHumanReadableBytes(bytes, 1000 * 1000, "MB") ??
    tryHumanReadableBytes(bytes, 1000, "KB") ??
    tryHumanReadableBytes(bytes, 1, "bytes")
  );
}

const sizeFormat = new Intl.NumberFormat([], {
  maximumFractionDigits: 3,
  useGrouping: true,
});

function tryHumanReadableBytes(
  bytes: number,
  divisor: number,
  unit: string
): string | undefined {
  if (bytes > divisor) return `${sizeFormat.format(bytes / divisor)} ${unit}`;
  return undefined;
}

export default UploadsPage;
