import { screen, waitFor } from "@testing-library/react";
import {
  requests,
  setupTestMockApi,
} from "src/tests/mock-backend/setup-test-mock-api";
import { oas, respondError } from "src/tests/openapi-msw";
import userEvent from "@testing-library/user-event";
import { resolveRoute } from "src/routes/routes";
import { customRenderPage } from "src/tests/utils/custom-render-page";
import { currentTestLocation } from "src/tests/utils/useTestLocation";
import { enterUserDetails } from "src/tests/ui-helpers/UserCredentialsForm";

const { useHandlers } = setupTestMockApi(
  oas.post("/api/users", (req, res, context) => {
    return res(
      context.json({
        username: req.body.username,
        roles: ["USER"],
      })
    );
  }),
  oas.get("/api/users", (req, res, context) => {
    return res(
      context.json({
        users: [],
      })
    );
  })
);

describe("CreateUserPage", () => {
  it("shows the correct header", async () => {
    await render();
    expect(
      screen.getByText("Create user", { selector: "p" })
    ).toBeInTheDocument();
  });

  it("has an 'Create user' button", async () => {
    await render();
    expect(
      screen.getByText("Create user", { selector: "button" })
    ).toBeInTheDocument();
  });

  it("saves the user and redirects to the user list", async () => {
    await render();
    await enterUserDetails("bob", "pw-bob", "pw-bob");
    await submit();
    await requests.find({
      method: "POST",
      pathname: "/api/users",
      body: { username: "bob", password: "pw-bob" },
    });
    await waitFor(() => {
      expect(currentTestLocation()).toEqual("/users");
    });
  });

  it("shows an error if the passwords do not match", async () => {
    await render();
    await enterUserDetails("bob", "pw-bob", "pw-wrong");
    await submit();
    expect(
      screen.getByTestId("user-detail--password-confirm")
    ).toHaveTextContent("Passwords do not match");
  });

  it("if the backend response with 'user already exists' shows an error", async () => {
    useHandlers(
      oas.post("/api/users", respondError(409, "User already exists"))
    );
    await render();
    await enterUserDetails("bob", "pw-bob", "pw-bob");
    await submit();
    await waitFor(() => {
      expect(screen.getByTestId("user-detail--username")).toHaveTextContent(
        "User already exists"
      );
    });
  });

  it("redirects to the user list after the user clicks 'cancel'", async () => {
    await render();
    await userEvent.click(screen.getByText("Cancel", { selector: "button" }));
    await waitFor(() => {
      expect(currentTestLocation()).toEqual("/users");
    });
  });
});

async function render(): Promise<void> {
  await customRenderPage(resolveRoute("/users/new"));
}

async function submit() {
  await userEvent.click(
    screen.getByText("Create user", { selector: "button" })
  );
}
