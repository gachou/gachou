import React from "react";
import { useLocation } from "wouter";
import { apiUsers } from "src/backend/users/userService";
import { resolveRoute } from "src/routes/routes";
import {
  UserCredentialsForm,
  UserDetailsFormProps,
} from "src/components/forms/UserDetailsForm";
import { Col, Row } from "src/utils/bootstrap";
import { logError } from "src/utils/logger";

const CreateUserPage: React.FC = () => {
  const [ignoredLocation, setLocation] = useLocation();

  const onSubmit: UserDetailsFormProps["onSubmit"] = async (
    { username, password },
    setError
  ) => {
    try {
      await apiUsers.create({ username, password });
      setLocation(resolveRoute("/users"));
    } catch (error) {
      logError("Error creating user", username, error);
      // TODO: provide error message based on actual error
      setError("username", { message: "User already exists" });
    }
  };

  const onCancel = () => {
    setLocation(resolveRoute("/users"));
  };

  return (
    <Row>
      <Col xs={12} md={9} lg={6}>
        <p className="fw-bold fs-1">Create user</p>
        <UserCredentialsForm
          onSubmit={onSubmit}
          onCancel={onCancel}
          submitButtonText={"Create user"}
        />
      </Col>
      <Col />
    </Row>
  );
};

export default CreateUserPage;
