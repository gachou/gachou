import { waitFor } from "@testing-library/react";
import {
  canceledUploads,
  mockUploadedFiles,
  progressByFile,
  resetMockTusUploader,
} from "src/adapter/upload/__mocks__";
import { uploader } from "./index";
import { resetGlobalStores } from "src/utils/globalStore";

vi.mock("src/adapter/upload");

const FILE_TIME_ISO = "2022-09-28T10:12:13.000Z";

function mockFile({
  contents = "abcd",
  name = "file.jpg",
  lastModified = FILE_TIME_ISO,
  type = "image/jpeg",
} = {}) {
  return new File([contents], name, {
    lastModified: new Date(lastModified).getTime(),
    type,
  });
}

beforeEach(async () => {
  resetGlobalStores();
  await resetMockTusUploader();
});

async function getSingleUploadId() {
  return await waitFor(() => {
    const keys = Object.keys(progressByFile);
    expect(keys).toHaveLength(1);
    return keys[0];
  });
}

describe("uploader", () => {
  it("is initially empty", () => {
    expect(uploader.getUploads()).toHaveLength(0);
  });

  it("contains an added file", async () => {
    const file = mockFile({ contents: "abcd", name: "file.jpg" });
    uploader.addFile(file);
    await waitFor(() => {
      expect(uploader.getUploads()).toHaveLength(1);
    });
    expect(uploader.getUploads()).toEqual([
      {
        id: expect.any(String),
        fileName: "file.jpg",
        fileLastModified: FILE_TIME_ISO,
        fileMimeType: "image/jpeg",
        activeOnClient: true,
        state: "RUNNING",
        totalSize: 4,
        uploadedSize: 0,
      },
    ]);
  });

  it("uploads files to the destination", async () => {
    const file = mockFile({ contents: "abc", name: "file.jpg" });
    uploader.addFile(file);
    const fileId = await getSingleUploadId();
    const control = progressByFile[fileId].control();
    await control.next();
    await control.next();
    await control.next();
    await mockUploadedFiles.find(file);
  });

  it("updates file progress during upload", async () => {
    const file = mockFile({ contents: "abcd", name: "file.jpg" });
    uploader.addFile(file);
    const id = await getSingleUploadId();

    const control = progressByFile[id].control();
    await control.next();
    expect(uploader.getUploads()).toEqual([
      {
        id: expect.any(String),
        fileName: "file.jpg",
        fileLastModified: FILE_TIME_ISO,
        fileMimeType: "image/jpeg",
        activeOnClient: true,
        state: "RUNNING",
        totalSize: 4,
        uploadedSize: 0,
      },
    ]);
    await control.next();
    expect(uploader.getUploads()).toEqual([
      {
        id: expect.any(String),
        fileName: "file.jpg",
        fileLastModified: FILE_TIME_ISO,
        fileMimeType: "image/jpeg",
        activeOnClient: true,
        state: "RUNNING",
        totalSize: 4,
        uploadedSize: 2,
      },
    ]);
    await control.next();
    expect(uploader.getUploads()).toEqual([
      {
        id: expect.any(String),
        fileName: "file.jpg",
        fileLastModified: FILE_TIME_ISO,
        fileMimeType: "image/jpeg",
        activeOnClient: true,
        state: "RUNNING",
        totalSize: 4,
        uploadedSize: 4,
      },
    ]);
  });

  it("does not contain a canceled file", async () => {
    const file = mockFile({ contents: "abcd", name: "file.jpg" });
    uploader.addFile(file);
    const id = await getSingleUploadId();

    await waitFor(() => {
      expect(uploader.getUploads()).toHaveLength(1);
    });
    uploader.cancelClientUpload(id);
    await waitFor(() => {
      expect(uploader.getUploads()).toHaveLength(0);
    });
    expect(canceledUploads).toHaveProperty(id);
  });
});
