import { proxy, ref, useSnapshot } from "valtio";
import { Upload } from "src/model/upload";
import { devtools } from "valtio/utils";
import { Immutable } from "src/utils/immutable";
import { uploadAdapter } from "src/adapter/upload";
import { defineGlobalStore } from "src/utils/globalStore";
import { mapObjIndexed } from "rambda";
import { errorToast } from "src/utils/toasts";

export interface UploadState {
  uploads: Record<string, { upload: Upload; abortController: AbortController }>;
}

export interface Uploader {
  getUploads(): Upload[];
  useClientUploadsById(): Immutable<Record<string, Upload>>;
  addFiles(files: FileList): Promise<void>;
  addFile(file: File): Promise<void>;
  cancelClientUpload(id: string): void;
}

export const uploader = defineGlobalStore<Uploader>(
  "uploader",
  (abortSignal): Uploader => {
    const state = proxy<{ obj: UploadState }>({
      obj: { uploads: {} },
    });
    const unsubscribeDevTool = registerDevTools(state);

    abortSignal.addEventListener("abort", () => {
      for (const uploadEntry of Object.values(state.obj.uploads)) {
        uploadEntry.abortController.abort();
      }
      unsubscribeDevTool && unsubscribeDevTool();
    });

    return {
      getUploads(): Upload[] {
        return Object.values(state.obj.uploads).map((value) => value.upload);
      },
      useClientUploadsById() {
        const snapshot = useSnapshot(state);
        return mapObjIndexed((value) => value.upload, snapshot.obj.uploads);
      },
      async addFile(file: File): Promise<void> {
        const abortController = new AbortController();
        const upload = await uploadAdapter.startUpload(file, {
          abortSignal: abortController.signal,
          onError: (error) =>
            errorToast("Error while uploading file", String(error)),
        });
        state.obj.uploads[upload.id] = {
          upload,
          abortController: ref(abortController),
        };
      },
      async addFiles(files: FileList): Promise<void> {
        for (const file of files) {
          await this.addFile(file);
        }
      },
      cancelClientUpload(id: string) {
        const upload = state.obj.uploads[id];
        if (upload != null) {
          upload.abortController.abort();
          delete state.obj.uploads[id];
        }
      },
    };
  }
);

function registerDevTools(state: { obj: UploadState }) {
  if (import.meta.env.DEV && !import.meta.env.VITEST) {
    return devtools(state, { name: "uploader", enabled: true });
  }
}
