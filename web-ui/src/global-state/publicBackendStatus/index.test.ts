import { renderHook } from "@testing-library/react";
import { setupTestMockApi } from "src/tests/mock-backend/setup-test-mock-api";
import { oas } from "src/tests/openapi-msw";
import { createTestEntity } from "src/tests/utils/createTestEntity";
import FakeTimers from "@sinonjs/fake-timers";
import { resetGlobalStores } from "src/utils/globalStore";
import { publicStatus } from "src/global-state/publicBackendStatus/index";

vi.mock("src/utils/toasts");

const { useHandlers } = setupTestMockApi(
  oas.get("/api/status/public", (req, res, context) => {
    return res(context.json({ adminUserExists: true }));
  })
);
describe("publicBackendStatus", () => {
  const clock = createTestEntity({
    create: () => FakeTimers.install(),
    destroy: (clock) => clock.uninstall(),
  });

  beforeEach(() => resetGlobalStores());

  it("calls the backend repeatedly to obtain the current status", async () => {
    const { result } = renderHook(publicStatus.useStatus);

    await clock.current.tickAsync(100);

    expect(result.current).toEqual({
      adminUserExists: true,
    });

    useHandlers(
      oas.get("/api/status/public", (req, res, context) => {
        return res(context.json({ adminUserExists: false }));
      })
    );

    await clock.current.tickAsync(5000);
    expect(result.current).toEqual({
      adminUserExists: false,
    });
  });

  describe("useBackendError", () => {
    it("returns null if nothing bad happens", () => {
      const { result } = renderHook(publicStatus.useError);
      expect(result.current).toBeNull();
    });

    it("returns an error if an error occurs and reverts to null once the error vanishes", async () => {
      useHandlers(
        oas.get("/api/status/public", (req, res, context) => {
          return res(context.status(500));
        })
      );

      const { result } = renderHook(publicStatus.useError);
      await clock.current.tickAsync(5000);
      expect(result.current).toEqual(expect.objectContaining({ status: 500 }));

      useHandlers(
        oas.get("/api/status/public", (req, res, context) => {
          return res(context.json({ adminUserExists: true }));
        })
      );

      await clock.current.tickAsync(6000);
      expect(result.current).toBeNull();
    });
  });
});
