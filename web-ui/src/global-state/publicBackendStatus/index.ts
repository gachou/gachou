import { proxy, useSnapshot } from "valtio";
import { fetchPublicStatus } from "src/backend/status/fetchPublicStatus";
import { setRepeatingTimeout } from "src/utils/setRepeatingTimeout";
import { logError } from "src/utils/logger";
import { defineGlobalStore } from "src/utils/globalStore";

export interface PublicBackendStatus {
  adminUserExists: boolean;
}

export interface CreatePublicStatusReturn {
  useStatus: () => PublicBackendStatus | null;
  useError: () => Error | null;
  updateNow: () => Promise<void>;
}

const STATUS_UPDATE_DELAY = 5000;

export const publicStatus: CreatePublicStatusReturn = defineGlobalStore(
  "public-status",
  (abortSignal) => {
    const state = proxy<{
      obj: PublicBackendStatus | null;
      error: Error | null;
    }>({ obj: null, error: null });

    async function updateStatusNow(): Promise<void> {
      try {
        state.obj = await fetchPublicStatus();
        state.error = null;
      } catch (error) {
        state.error = error instanceof Error ? error : new Error(String(error));
      }
    }

    function usePublicBackendStatus() {
      return useSnapshot(state).obj;
    }
    function useBackendError() {
      return useSnapshot(state).error;
    }

    setRepeatingTimeout(updateStatusNow, STATUS_UPDATE_DELAY, {
      abortSignal,
      onError(error) {
        logError("Cannot load backend state", error);
      },
    });

    return {
      useStatus: usePublicBackendStatus,
      useError: useBackendError,
      updateNow: updateStatusNow,
    };
  }
);
