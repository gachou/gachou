import { renderHook } from "@testing-library/react";
import { TransitionSpec, useWebTransition } from "src/hooks/useWebTransition";
import { mockAnimationsApi } from "jsdom-testing-mocks";
import { waitFor } from "@testing-library/react";

mockAnimationsApi();

const mockTransition = new TransitionSpec(
  (ref) =>
    new Animation(
      new KeyframeEffect(ref, [{ opacity: 0 }, { opacity: 1 }], {
        duration: 100,
      })
    )
);

describe("useWebTransition", () => {
  it("is invisible without animations, if show=false", async () => {
    const element = document.createElement("div");
    const { result } = renderHook(useWebTransition, {
      initialProps: {
        ref: { current: element },
        show: false,
        spec: mockTransition,
      },
    });
    expect(document.getAnimations()).toHaveLength(0);
    expect(result.current.visible).toEqual(false);
  });

  it("animates the element if show=true", async () => {
    vi.spyOn(mockTransition, "enter");
    const element = document.createElement("div");
    const { result } = renderHook(useWebTransition, {
      initialProps: {
        ref: { current: element },
        show: true,
        spec: mockTransition,
      },
    });
    expect(result.current.visible).toEqual(true);
    expect(mockTransition.enter).toHaveBeenCalledWith(element);
  });

  it("animates the element when transitioning to show=false", async () => {
    vi.spyOn(mockTransition, "leave");
    const element = document.createElement("div");
    const ref = { current: element };
    const { result, rerender } = renderHook(useWebTransition, {
      initialProps: {
        ref,
        show: true,
        spec: mockTransition,
      },
    });
    expect(result.current.visible).toBe(true);

    rerender({
      ref,
      show: false,
      spec: mockTransition,
    });

    expect(mockTransition.leave).toHaveBeenCalledWith(element);
    await waitFor(() => {
      expect(result.current.visible).toBe(false);
    });
  });
});
