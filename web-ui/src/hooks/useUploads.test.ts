import { act, renderHook, waitFor } from "@testing-library/react";
import { useUploads } from "./useUploads";

import { oas } from "src/tests/openapi-msw";
import { uploader } from "src/global-state/uploader";
import { Upload } from "src/model/upload";
import { UploadResponse } from "src/backend/__generated__/openapi-client";
import { waitForNotNull } from "src/tests/utils/expectNotNull";
import {
  requests,
  setupTestMockApi,
} from "src/tests/mock-backend/setup-test-mock-api";
import { waitMillis } from "src/tests/utils/waitMillis";
import { Immutable } from "src/utils/immutable";

vi.mock("src/global-state/uploader", () => {
  return {
    uploader: {
      useClientUploadsById: vi.fn(),
      cancelClientUpload: vi.fn(),
    },
  };
});

const audit = {
  createdBy: "Nils",
  createdAt: "2022-07-10T13:15:17Z",
  updatedBy: "Nils",
  updatedAt: "2022-07-10T13:15:17Z",
};

const baseUpload: Upload = {
  id: "server-side-id",
  uploadedSize: 50,
  totalSize: 100,
  fileLastModified: "2022-07-10T13:15:17Z",
  fileName: "avatar.png",
  fileMimeType: "image/png",
  state: "RUNNING",
};

const uploadResponse: UploadResponse = {
  ...baseUpload,
  fileMimeType: "image/png",
  s3Bucket: "b",
  s3ObjectKey: "11111111111111111111111111111111",
  audit,
};

const { useHandlers } = setupTestMockApi(
  oas.get("/api/uploads", (req, res, context) => {
    return res(
      context.json({
        uploads: [uploadResponse],
      })
    );
  }),
  oas.delete("/api/uploads/:id", (req, res, context) => {
    return res(context.json(uploadResponse));
  })
);

describe("useUploads", () => {
  beforeEach(() => {
    localStorage.clear();
    mockClientUploads({});
  });

  it("if no client uploads are present, returns a list of uploads from the server", async () => {
    mockClientUploads({});
    const { result } = renderHook(useUploads);
    await waitFor(() => {
      expect(result.current.uploads.value).toEqual([
        {
          ...baseUpload,
          id: "server-side-id",
          activeOnClient: false,
          audit,
        },
      ]);
    });
  });

  it("merges client-upload if it has the same id", async () => {
    mockClientUploads({
      "server-side-id": {
        ...baseUpload,
        activeOnClient: true,
        uploadedSize: 70,
      },
    });

    const { result } = renderHook(useUploads);
    await waitFor(() => {
      expect(result.current.uploads.value).toEqual([
        {
          ...baseUpload,
          id: "server-side-id",
          uploadedSize: 70,
          activeOnClient: true,
          audit,
        },
      ]);
    });
  });

  describe("cancelUpload", async () => {
    it("calls the cancel methods", async () => {
      mockClientUploads({
        "server-side-id": {
          ...baseUpload,
          activeOnClient: true,
          uploadedSize: 70,
        },
      });

      const { result } = renderHook(useUploads);
      const upload = await waitForNotNull(
        () => result.current.uploads.value?.[0]
      );
      await act(async () => {
        await result.current.cancelUpload(upload);
      });
      expect(uploader.cancelClientUpload).toHaveBeenCalledWith(
        "server-side-id"
      );
      requests.get({
        method: "DELETE",
        pathname: "/api/uploads/server-side-id",
      });
    });

    it("tries to find the server-side upload, if no 'id' is stored locally", async () => {
      mockClientUploads({
        "server-side-id": {
          ...baseUpload,
          activeOnClient: true,
          uploadedSize: 70,
        },
      });
      mockUploadList([]);

      const { result } = renderHook(useUploads);
      await waitMillis(1);

      mockUploadList([uploadResponse]);

      const upload = await waitForNotNull(
        () => result.current.uploads.value?.[0]
      );

      await result.current.cancelUpload(upload);

      expect(uploader.cancelClientUpload).toHaveBeenCalledWith(
        "server-side-id"
      );
      requests.get({
        method: "DELETE",
        pathname: "/api/uploads/server-side-id",
      });
    });
  });
});

function mockUploadList(uploads: UploadResponse[]) {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useHandlers(
    oas.get("/api/uploads", (req, res, context) => {
      return res(
        context.json({
          uploads: uploads,
        })
      );
    })
  );
}

function mockClientUploads(uploads: Immutable<Record<string, Upload>>) {
  const useClientUploadsBy = uploader.useClientUploadsById;
  if (!vi.isMockFunction(useClientUploadsBy))
    throw new Error("expected mock, but got " + useClientUploadsBy);
  useClientUploadsBy.mockReturnValue(uploads);
}
