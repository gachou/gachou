import {
  MutableRefObject,
  useEffect,
  useLayoutEffect,
  useRef,
  useState,
} from "react";

type AnimationFactory = (ref: HTMLElement | null) => Animation;

export class TransitionSpec {
  readonly enter: AnimationFactory;
  readonly leave: AnimationFactory;

  constructor(enter: AnimationFactory, leave?: AnimationFactory) {
    this.enter = enter;
    this.leave = leave ?? TransitionSpec.reverse(enter);
  }

  static reverse(enter: AnimationFactory) {
    return (element: HTMLElement | null) => {
      const result = enter(element);
      result.reverse();
      return result;
    };
  }
}

interface AnimationOptions<T extends HTMLElement> {
  show: boolean;
  spec: TransitionSpec;
  ref: MutableRefObject<T | null>;
}

interface AnimationReturn {
  visible: boolean;
}

export function useWebTransition<T extends HTMLElement>({
  show,
  spec,
  ref,
}: AnimationOptions<T>): AnimationReturn {
  const [visible, setVisible] = useState(false);
  const lastAnimation = useRef<Animation | null>(null);

  useEffect(() => {
    if (show) {
      setVisible(true);
    } else if (visible) {
      lastAnimation.current?.cancel();
      const animation = spec.leave(ref.current);
      animation.play();
      animation.finished.then(() => setVisible(false));
      lastAnimation.current = animation;
    }
  }, [ref, show, spec, visible]);

  useLayoutEffect(() => {
    if (visible) {
      lastAnimation.current?.cancel();
      const animation = spec.enter(ref.current);
      animation.play();
      lastAnimation.current = animation;
    }
  }, [ref, spec, visible]);

  return { visible };
}
