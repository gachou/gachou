import { ApiValue, mapApiValue, useApi } from "src/hooks/useApi";
import { uploader } from "src/global-state/uploader";
import { Upload } from "src/model/upload";
import { indexBy } from "src/utils/collections";
import { listAllUploads } from "src/backend/uploads/listAllUploads";
import { mergeDeepRight } from "rambda";
import { useCallback } from "react";
import { apiCancelUpload } from "src/backend/uploads/cancelUpload";

export interface UseUploadsReturn {
  uploads: ApiValue<Upload[]>;
  cancelUpload(upload: Upload): Promise<void>;
}

export function useUploads(): UseUploadsReturn {
  const clientUploads = uploader.useClientUploadsById();
  const serverUploads = useApi(listServerUploadsById);

  const uploads = mapApiValue(serverUploads, (serverUploads) => {
    return Object.values(mergeUploads(serverUploads, clientUploads));
  });

  const cancelUpload = useCallback(async (upload: Upload) => {
    uploader.cancelClientUpload(upload.id);
    await apiCancelUpload(upload.id);
  }, []);

  return {
    uploads,
    cancelUpload,
  };
}

async function listServerUploadsById(): Promise<Record<string, Upload>> {
  return indexBy((upload) => upload.id, await listAllUploads());
}

function mergeUploads(
  serverUploads: Record<string, Upload>,
  clientUploads: Record<string, Upload>
): Record<string, Upload> {
  return mergeDeepRight(serverUploads, clientUploads);
}
