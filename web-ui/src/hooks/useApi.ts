import { useCallback, useEffect, useRef, useState } from "react";

export interface ApiValue<T> {
  value: T | null;
  error: Error | null;
  reload: () => Promise<void>;
}

interface State<R> {
  value: R | null;
  error: Error | null;
}

export function useApi<Args extends unknown[], Result>(
  apiFn: (...args: Args) => Promise<Result>,
  ...args: Args
): ApiValue<Result> {
  const [state, setState] = useState<State<Result>>({
    value: null,
    error: null,
  });
  const mounted = useRef(true);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const loader = useCallback(() => apiFn(...args), [apiFn, ...args]);

  const setIfMounted = useCallback(
    (error: Error | null, value: Result | null) => {
      if (mounted.current) {
        setState({ error, value });
      }
    },
    []
  );

  const reload = useCallback(async () => {
    try {
      await loader().then(
        (value) => setIfMounted(null, value),
        (error) => setIfMounted(error, null)
      );
    } catch (error) {
      if (error instanceof Error) {
        setIfMounted(error, null);
      } else {
        setIfMounted(new Error(String(error)), null);
      }
    }
  }, [loader, setIfMounted]);

  useEffect(() => {
    mounted.current = true;
    reload();
    return () => {
      mounted.current = false;
    };
  }, [reload]);

  return {
    ...state,
    reload,
  };
}

export function mapApiValue<T, R>(
  apiValue: ApiValue<T>,
  fn: (value: T) => R
): ApiValue<R> {
  return {
    value: apiValue.value == null ? null : fn(apiValue.value),
    error: apiValue.error,
    reload: apiValue.reload,
  };
}
