import { renderHook } from "@testing-library/react";
import { mapApiValue, useApi } from "src/hooks/useApi";
import { waitMillis } from "src/tests/utils/waitMillis";
import { waitFor } from "@testing-library/react";
import { vitest } from "vitest";

const TEST_ERROR = new Error("Test-Error");

describe("useApi", () => {
  afterEach(async () => {
    vitest.restoreAllMocks();
  });

  it("has value and error null initially", async () => {
    const loader = () => waitMillis(10).then(() => "done");
    const { result } = renderHook(() => useApi(loader));

    expect(result.current.value).toBeNull();
    expect(result.current.error).toBeNull();

    await waitMillis(50);
  });

  it("shows the resulting value when done", async () => {
    const loader = () => waitMillis(10).then(() => "done");
    const { result } = renderHook(() => useApi(loader));

    await waitFor(() => expect(result.current.value).toEqual("done"));
  });

  it("does not set state in unmounted components", async () => {
    const consoleSpy = vitest.spyOn(console, "error");
    const loader = () => waitMillis(5).then(() => "done");
    const { unmount } = renderHook(() => useApi(loader));
    unmount();
    await waitMillis(10);
    // eslint-disable-next-line no-console
    expect(consoleSpy).not.toHaveBeenCalled();
  });

  it("sets the error if the result is a rejected promise", async () => {
    const loader = () => Promise.reject(TEST_ERROR);
    const { result } = renderHook(() => useApi(loader));
    await waitMillis(10);
    await waitFor(() => expect(result.current.error).toEqual(TEST_ERROR));
    expect(result.current.value).toBeNull();
  });

  it("sets the error if loader throws a synchronous error", async () => {
    const loader = () => {
      throw TEST_ERROR;
    };
    const { result } = renderHook(() => useApi(loader));
    await waitMillis(0);
    expect(result.current.value).toBeNull();
    expect(result.current.error).toEqual(TEST_ERROR);
  });

  it("reloads the value if 'reload' is called", async () => {
    let value = "initialValue";
    const loader = () => Promise.resolve(value);
    const { result } = renderHook(() => useApi(loader));
    await waitFor(() => expect(result.current.value).toEqual("initialValue"));

    value = "nextValue";
    await result.current.reload();
    await waitFor(() => expect(result.current.value).toEqual("nextValue"));
  });

  it("passes args to the apiFn", async () => {
    const apiFn = (arg1: string, arg2: number) =>
      Promise.resolve(arg1 + "-" + arg2);
    const { result, rerender } = renderHook(
      ({ arg1, arg2 }) => useApi(apiFn, arg1, arg2),
      {
        initialProps: { arg1: "a", arg2: 2 },
      }
    );
    await waitFor(() => expect(result.current.value).toEqual("a-2"));
    rerender({ arg1: "b", arg2: 2 });
    await waitFor(() => expect(result.current.value).toEqual("b-2"));
  });
});

describe("the mapApiValue-method", () => {
  it("maps values to via the callback function", async () => {
    const loader = async () => "done";
    const { result } = renderHook(() =>
      mapApiValue(useApi(loader), (x) => x.length)
    );

    await waitFor(() => expect(result.current.value).toEqual(4));
    expect(result.current.error).toEqual(null);
  });

  it("passes errors along", async () => {
    const loader = (): Promise<string> => Promise.reject(TEST_ERROR);

    const { result } = renderHook(() =>
      mapApiValue(useApi(loader), (x) => x.length)
    );

    await waitFor(() => expect(result.current.error).toEqual(TEST_ERROR));
    await waitFor(() => expect(result.current.value).toEqual(null));
  });

  it("reloads the value if 'reload' is called", async () => {
    let value = "a";
    const loader = () => Promise.resolve(value);
    const { result } = renderHook(() =>
      mapApiValue(useApi(loader), (x) => x.length)
    );

    await waitFor(() => expect(result.current.value).toEqual(1));
    value = "aaaaaaaaaa";
    await result.current.reload();

    await waitFor(() => expect(result.current.value).toEqual(10));
  });
});
