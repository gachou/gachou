import { afterEach, beforeAll, expect, vitest } from "vitest";
import "whatwg-fetch";
import "src/tests/polyfills/stream";

import matchers from "@testing-library/jest-dom/matchers";
import { cleanup } from "@testing-library/react";

vitest.mock("src/utils/reloadPage.ts");
vitest.mock("src/plugins/index.ts");

beforeAll(() => {
  expect.extend(matchers);
});

afterEach(cleanup);

export {};
