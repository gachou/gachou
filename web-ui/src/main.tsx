import React from "react";

import "./styles/index.scss";
import App from "./App";
import { initApi } from "./init/init-api";

import { registerApplicationActions } from "src/init/register-application-actions";
import { gitCommitId } from "virtual:version";
import { logError, logInfo } from "src/utils/logger";
import { initErrorToasts } from "src/init/init-error-toasts";
import { createRoot } from "react-dom/client";
import { selectAuthenticationAdapter } from "src/adapter/authentication";
import createJwtUserPasswordAdapter from "src/adapter/authentication/jwtUserPassword";
import { selectUploadAdapter } from "src/adapter/upload";
import { tusUploadAdapter } from "src/adapter/upload/tus";

logInfo("git-commit-id:", gitCommitId);

async function runApplication() {
  initErrorToasts();
  initApi();
  selectAuthenticationAdapter(createJwtUserPasswordAdapter());
  selectUploadAdapter(tusUploadAdapter);

  if ("Cypress" in window) {
    registerApplicationActions();
  }

  if (import.meta.env.VITE_MOCK_API === "true") {
    const { initMockApi } = await import("src/init/init-mock-api");
    await initMockApi();
  }
  createRoot(document.body).render(
    <React.StrictMode>
      <App />
    </React.StrictMode>
  );
}

runApplication().catch((error) => {
  logError("Error while starting application", error);
  createRoot(document.body).render(
    <React.StrictMode>
      <h1>Oh no, I couldn't start Gachou? Sooorry...</h1>
    </React.StrictMode>
  );
});
