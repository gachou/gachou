export declare global {
  interface Window {
    updateStatusNow?: () => Promise<void>;
  }
}
