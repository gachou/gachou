export declare global {
  interface Window {
    API_BASE_URL: string;
    TUS_UPLOAD_URL: string;
  }
}

declare module "deploy-config.js" {
  // This module is implemented by the files in 'src/deploy-profiles'
  // and injected by a local vite-plugin
}
