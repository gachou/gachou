// The following properties in tus-js-client are available, but not declared in the types.
// We need them in order to properly show the progress of resumed uploads
import "tus-js-client";

declare module "tus-js-client" {
  export interface PreviousUpload {
    uploadUrl: string;
    urlStorageKey: string;
  }
}
