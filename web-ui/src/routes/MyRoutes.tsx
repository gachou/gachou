import React, { lazy, ReactElement, Suspense } from "react";
import { RoutePattern } from "src/routes/routes";
import { Route, RouteProps } from "wouter";
import { ErrorBoundary } from "src/ErrorBoundary";

type MyRouteProps<T extends RoutePattern> = RouteProps<undefined, T>;
const MyRoute = Route as <T extends RoutePattern>(
  props: MyRouteProps<T>
) => ReactElement | null;

export const MyRoutes: React.FC = () => {
  return (
    <ErrorBoundary>
      <Suspense fallback={"Loading Page"}>
        <MyRoute
          path={"/"}
          component={lazy(() => import("src/pages/HomePage"))}
        />
        <MyRoute
          path={"/test/error"}
          component={lazy(() => import("src/pages/testing/ErrorTestPage"))}
        />
        <MyRoute
          path={"/uploads"}
          component={lazy(() => import("src/pages/UploadsPage"))}
        />
        <MyRoute
          path={"/users"}
          component={lazy(() => import("src/pages/UserListPage"))}
        />
        <MyRoute
          path={"/users/new"}
          component={lazy(() => import("src/pages/CreateUserPage"))}
        />
        <MyRoute
          path={"/users/edit/:username"}
          component={lazy(() => import("src/pages/EditUserPage"))}
        />
      </Suspense>
    </ErrorBoundary>
  );
};
