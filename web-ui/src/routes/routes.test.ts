import { resolveRoute } from "./routes";
import { describe, expect, it } from "vitest";

describe("to", () => {
  it("returns the parameter as value if there is no param", () => {
    expect(resolveRoute("/")).toBe("/");
  });

  it("fills in the provided params", () => {
    expect(resolveRoute("/users/edit/:username", { username: "bob" })).toBe(
      "/users/edit/bob"
    );
  });

  it("uri-encoded params the provided params", () => {
    expect(resolveRoute("/users/edit/:username", { username: "a/b" })).toBe(
      "/users/edit/a%2Fb"
    );
  });
});
