export type RoutePattern =
  | "/"
  | "/uploads"
  | "/users"
  | "/users/new"
  | "/users/edit/:username"
  | "/test/error";

interface GeneratedResolveRoute {
  (path: "/"): string;
  (path: "/uploads"): string;
  (path: "/users"): string;
  (path: "/users/new"): string;
  (
    path: "/users/edit/:username",
    params: Record<"username", string | number>
  ): string;
  (path: "/test/error"): string;
}

const PARAM_REGEX = /:(\w+)/;
export const resolveRoute: GeneratedResolveRoute = (
  path: string,
  params?: Record<string, string | number>
): string => {
  if (params == null) {
    return path;
  }
  return path.replace(PARAM_REGEX, (ignoredMatch, name) =>
    encodeURIComponent(params[name])
  );
};
