import { indexBy } from "src/utils/collections";

describe("indexBy", () => {
  it("indexes arrays by a function", () => {
    const array = [
      { id: "1", name: "one" },
      { id: "2", name: "two" },
      { id: "3", name: "three" },
    ];
    expect(indexBy("id", array)).toEqual({
      "1": { id: "1", name: "one" },
      "2": { id: "2", name: "two" },
      "3": { id: "3", name: "three" },
    });
  });

  it("uses the last entry on duplicate keys", () => {
    const array = [
      { id: "1", name: "one" },
      { id: "2", name: "two (1)" },
      { id: "2", name: "two (2)" },
    ];
    expect(indexBy("id", array)).toEqual({
      "1": { id: "1", name: "one" },
      "2": { id: "2", name: "two (2)" },
    });
  });
});
