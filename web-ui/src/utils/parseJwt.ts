import { VArray, VRecord, Static, VString } from "src/utils/validation";

const Token = VRecord({
  upn: VString,
  groups: VArray(VString),
});

export type Token = Static<typeof Token>;

export function parseJwt(token: string): Token {
  const [ignoredHeader, payload, ignoredSignature] = token.split(".");
  return Token.check(JSON.parse(atob(payload)));
}
