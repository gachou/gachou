import { noop } from "src/utils/noop";

interface RepeatingTimeoutArgs {
  abortSignal?: AbortSignal;
  onError?: (error: Error) => void;
}

export function setRepeatingTimeout(
  fn: () => Promise<void>,
  delay: number,
  { abortSignal = new AbortSignal(), onError = noop }: RepeatingTimeoutArgs = {}
): void {
  async function run() {
    if (abortSignal.aborted) return;
    try {
      await fn();
    } catch (error) {
      onError(error instanceof Error ? error : new Error(String(error)));
    }
    setTimeout(run, delay);
  }
  run();
}
