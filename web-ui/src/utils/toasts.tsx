/* eslint-disable no-restricted-imports */
import { toast, ToastContainer, Zoom } from "react-toastify";
import React from "react";

export function errorToast(title: string, description: string) {
  toast(
    <div>
      <strong>{title}</strong>
      <p>{description}</p>
    </div>,
    { type: "error", autoClose: false }
  );
}

export const MyToastContainer: React.FC = () => (
  <ToastContainer position={"bottom-right"} transition={Zoom} />
);
