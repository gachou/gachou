/* eslint-disable no-console */

export function logDebug(...args: unknown[]): void {
  if (logDebug.enabled) {
    console.debug(...args);
  }
}
logDebug.enabled = false;

export function logInfo(...args: unknown[]): void {
  console.log(...args);
}

export function logWarning(...args: unknown[]): void {
  console.warn(...args);
}

export function logError(...args: unknown[]): void {
  console.error(...args);
}
