import { setRepeatingTimeout } from "src/utils/setRepeatingTimeout";
import FakeTimers from "@sinonjs/fake-timers";
import { createTestEntity } from "src/tests/utils/createTestEntity";
import { waitMillis } from "src/tests/utils/waitMillis";

vi.mock("./logger");

describe("setRepeatingTimeout", () => {
  const clock = createTestEntity({
    create: () => FakeTimers.install(),
    destroy: (clock) => clock.uninstall(),
  });

  it("repeatedly calls a function until aborted", async () => {
    const abortController = new AbortController();
    const fn = vi.fn();
    setRepeatingTimeout(fn, 200, { abortSignal: abortController.signal });
    await clock.current.tickAsync(500);
    abortController.abort();
    await clock.current.tickAsync(500);
    // first call at 0ms, second call at about 200ms, third call at about 400ms
    expect(fn).toHaveBeenCalledTimes(3);
  });

  it("waits for the given delay *after* the function is done, before calling again", async () => {
    const abortController = new AbortController();
    const fn = vi.fn(() => waitMillis(200));
    setRepeatingTimeout(fn, 200, { abortSignal: abortController.signal });
    await clock.current.tickAsync(500);
    abortController.abort();
    await clock.current.tickAsync(500);

    // first call at 0ms, delay at 200ms, second call at 400ms
    expect(fn).toHaveBeenCalledTimes(2);
  });

  it("calls the error callback on errors", async () => {
    const abortController = new AbortController();
    const fn = vi.fn().mockRejectedValue(new Error("Test-Error"));
    const onError = vi.fn();
    setRepeatingTimeout(fn, 200, {
      abortSignal: abortController.signal,
      onError,
    });
    await clock.current.tickAsync(500);
    abortController.abort();
    // first call at 0ms, second call at 200ms, third at 400ms
    expect(onError).toHaveBeenCalledTimes(3);
    expect(onError).toHaveBeenCalledWith(new Error("Test-Error"));
  });
});
