export function toBeInitialized<T extends object>(name: string): T {
  return new Proxy<T>({ _uninitialized_adapter_: name } as T, {
    get() {
      throw new Error(`adapter "${name}" is not initialized yet.`);
    },
  });
}
