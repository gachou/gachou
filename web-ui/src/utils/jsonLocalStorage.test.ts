import { JsonLocalStorage } from "src/utils/jsonLocalStorage";

const KEY = "test-key";

describe("jsonLocalStorage", () => {
  beforeEach(() => {
    localStorage.clear();
  });

  it("stores a value in as JSON local storage", () => {
    const storage = new JsonLocalStorage<number>(KEY);
    storage.setItem(2);

    expect(localStorage.getItem(KEY)).toEqual("2");
  });

  it("retrieves a value as JSON from local storage", () => {
    localStorage.setItem(KEY, "2");

    const storage = new JsonLocalStorage<number>(KEY);
    expect(storage.getItem()).toEqual(2);
  });

  it("retrieves null if no value is stored", () => {
    const storage = new JsonLocalStorage<number>(KEY);
    expect(storage.getItem()).toBeNull();
  });

  it("clears a value in local storage", () => {
    localStorage.setItem(KEY, "2");

    const storage = new JsonLocalStorage<number>(KEY);
    storage.removeItem();

    expect(localStorage.getItem(KEY)).toBeNull();
  });
});
