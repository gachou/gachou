export class JsonLocalStorage<T> {
  private readonly key: string;

  constructor(key: string) {
    this.key = key;
  }

  getItem(): T | null {
    return parseIfNotNull(localStorage.getItem(this.key));
  }

  setItem(value: T): void {
    localStorage.setItem(this.key, JSON.stringify(value));
  }

  removeItem(): void {
    localStorage.removeItem(this.key);
  }
}

function parseIfNotNull(jsonString: string | null) {
  if (jsonString == null) {
    return null;
  }
  return JSON.parse(jsonString);
}
