import { fileChecksum } from "src/utils/fileChecksum";

describe("fileChecksum", () => {
  it("creates the checksum of a file", async () => {
    const blob = new window.Blob([
      new Uint8Array([1, 2, 3]),
      new Uint8Array([4, 5, 6]),
    ]);
    const checksum = await fileChecksum(blob);
    expect(checksum).toBe(30569571);
  });
});
