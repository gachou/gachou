let stores = new Map();
let abortController: AbortController = new AbortController();

export function defineGlobalStore<T extends Record<never, unknown>>(
  name: string,
  setup: (abortSignal: AbortSignal) => T
): T {
  return new Proxy<T>({} as T, {
    get(_, prop) {
      const target = lazyGetStore(name, () => setup(abortController.signal));
      return target[prop as keyof T];
    },
  });
}

function lazyGetStore<T>(name: string, provider: () => T): T {
  let result = stores.get(name);
  if (result == null) {
    result = provider();
    stores.set(name, result);
  }
  return result;
}

export function resetGlobalStores() {
  stores = new Map();
  abortController.abort();
  abortController = new AbortController();
}
