export function deepEqual<T>(x: T, y: T): boolean {
  if (x == null || y == null) {
    return Object.is(x, y);
  }
  if (Array.isArray(x)) {
    if (!Array.isArray(y)) return false;
    if (x.length !== y.length) return false;
    for (let i = 0; i < x.length; i++) {
      if (!deepEqual(x[i], y[i])) {
        return false;
      }
    }
    return true;
  }
  if (typeof x === "object") {
    return deepEqual(Object.entries(x), Object.entries(y));
  }
  return Object.is(x, y);
}
