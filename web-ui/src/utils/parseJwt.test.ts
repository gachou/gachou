import { parseJwt } from "./parseJwt";

const ADMIN_TOKEN =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1cG4iOiJhZG1pbiIsImdyb3VwcyI6WyJBRE1JTiJdLCJpYXQiOjE2NTQ5NTkwNjMsImV4cCI6MTY1NTA0NTQ2MywianRpIjoiNDA5ZDJkMTgtMTY5OS00MTJiLThkOTYtM2Q1Njk0ZTNlYjNlIn0.pjhO5vW59EGTanJIw5ms7cX0mYJBEVtSM0LFHdiHDpQ";
const BOB_USER_TOKEN =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1cG4iOiJib2IiLCJncm91cHMiOlsiVVNFUiJdLCJpYXQiOjE2NTQ5NTk0NjEsImV4cCI6MTY1NTA0NTg2MSwianRpIjoiNTQxMjkwY2MtNzYxYS00ZDYzLWJmZTctMGFkNWNhN2I3MTIxIn0.ys3t9Zp18HXgbOdzuhX7Osb9pWJ3HX1BorY1pG_3nS0";
const BOB_USER_AND_ADMIN_TOKEN =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1cG4iOiJib2IiLCJncm91cHMiOlsiQURNSU4iLCJVU0VSIl0sImlhdCI6MTY1NDk1OTA2MywiZXhwIjoxNjU1MDQ1NDYzLCJqdGkiOiI0MDlkMmQxOC0xNjk5LTQxMmItOGQ5Ni0zZDU2OTRlM2ViM2UifQ.y2jL797O3E62DE-oV93WOcQ3AQdOIeC9goCRpVPkte0";

describe("parseJwt", () => {
  it("parses an admin token", () => {
    expect(parseJwt(ADMIN_TOKEN)).toEqual({
      upn: "admin",
      groups: ["ADMIN"],
    });
  });

  it("parses a user token", () => {
    expect(parseJwt(BOB_USER_TOKEN)).toEqual({
      upn: "bob",
      groups: ["USER"],
    });
  });

  it("parses a token with multiple roles", () => {
    expect(parseJwt(BOB_USER_AND_ADMIN_TOKEN)).toEqual({
      upn: "bob",
      groups: ["ADMIN", "USER"],
    });
  });
});
