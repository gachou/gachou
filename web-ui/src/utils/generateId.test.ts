import { generateId } from "src/utils/generateId";

describe("generateId", () => {
  it("creates a unique numbers", () => {
    const generatedNumbers: Record<number, boolean> = {};
    for (let i = 0; i < 1000; i++) {
      const nextNumber = generateId();
      expect(generatedNumbers[nextNumber]).toBeFalsy();
      generatedNumbers[nextNumber] = true;
    }
  });
});
