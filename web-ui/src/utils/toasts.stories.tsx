import { Story } from "@storybook/react";
import { Button } from "src/utils/bootstrap";
import { errorToast, MyToastContainer } from "src/utils/toasts";

export default {
  component: Button,
};

export const ErrorToast: Story = () => {
  return (
    <div>
      <MyToastContainer />
      <Button
        onClick={() => errorToast("Test", "This is a test toast ")}
        variant={"danger"}
      >
        Error Toast
      </Button>
    </div>
  );
};
