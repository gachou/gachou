import { defineGlobalStore, resetGlobalStores } from "src/utils/globalStore";
import { proxy } from "valtio";

let sideEffect = 0;

const counter = defineGlobalStore<{ value: number }>(
  "counter",
  (abortSignal) => {
    const state = proxy<{ counter: number }>({ counter: 0 });
    const interval = setInterval(() => {
      state.counter++;
      sideEffect++;
    }, 100);
    abortSignal.addEventListener("abort", () => clearInterval(interval));
    return {
      get value() {
        return state.counter;
      },
    };
  }
);

vi.useFakeTimers();

describe("globalStore", () => {
  beforeEach(() => {
    resetGlobalStores();
    sideEffect = 0;
  });
  it("creates the store upon lazy on first access", () => {
    vi.advanceTimersByTime(150);
    expect(counter.value).toEqual(0);
    vi.advanceTimersByTime(150);
    expect(counter.value).toEqual(1);
  });

  it("resetGlobalStores triggers the cancels running tasks", () => {
    expect(counter.value).toEqual(0);
    vi.advanceTimersByTime(150);
    expect(counter.value).toBe(1);
    expect(sideEffect).toEqual(1);
    resetGlobalStores();
    vi.advanceTimersByTime(500);
    expect(sideEffect).toEqual(1);
  });

  it("resetGlobalStores reset stores to initial state", () => {
    expect(counter.value).toEqual(0);
    vi.advanceTimersByTime(150);
    expect(counter.value).toBe(1);
    resetGlobalStores();
    expect(counter.value).toEqual(0);
  });
});
