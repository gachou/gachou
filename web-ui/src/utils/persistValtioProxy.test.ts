import { proxy, subscribe, useSnapshot } from "valtio";
import { persistValtioProxy } from "./persistValtioProxy";
import { vi } from "vitest";
import { waitFor } from "@testing-library/react";
import { renderHook } from "@testing-library/react";

const KEY = "test-storage-key";

describe("createLocalStorageBackedProxy", () => {
  let cleanupMethods: Array<() => void> = [];

  beforeEach(() => {
    cleanupMethods = [];
    localStorage.clear();
  });

  afterEach(() => {
    cleanupMethods.forEach((cleanup) => cleanup());
  });

  function setupPersistedState<T>(initialValue: T): { obj: T } {
    const state = proxy({ obj: initialValue });
    const { detachFromStorage } = persistValtioProxy(state, KEY);
    cleanupMethods.push(detachFromStorage);
    return state;
  }

  it("creates a valtio proxy wrapping an object", () => {
    const state = setupPersistedState({ value: 1 });
    expect(state.obj.value).toBe(1);
  });

  it("allows useSnapshot", async () => {
    const state = setupPersistedState({ value: 1 });
    const { result } = renderHook(() => useSnapshot(state.obj));
    expect(result.current.value).toBe(1);
    state.obj.value++;
    await waitFor(() => {
      expect(result.current.value).toBe(2);
    });
  });

  it("allows subscribers", async () => {
    const state = setupPersistedState({ value: 1 });
    const subscriber = vi.fn();
    subscribe(state.obj, subscriber);
    expect(subscriber).not.toHaveBeenCalled();
    state.obj.value++;
    await waitFor(() => {
      expect(subscriber).toHaveBeenCalledWith([["set", ["value"], 2, 1]]);
    });
  });

  it("persists its value", async () => {
    const state1 = setupPersistedState({ value: 1 });
    state1.obj.value++;
    await waitFor(() => {
      expect(state1.obj.value).toBe(2);
    });
    const state2 = setupPersistedState({ value: 1 });
    expect(state2.obj.value).toBe(2);
  });

  it("updates on localStorage-changes", async () => {
    const state = setupPersistedState({ value: 1 });
    localStorage.setItem(KEY, '{"value": 2}');
    window.dispatchEvent(new Event("storage"));

    await waitFor(() => {
      expect(state.obj.value).toBe(2);
    });
  });

  it("does not persist, if update was due to change in localStorage", async () => {
    // This particular test makes sure that there are no endless loop
    // of updates when two tabs are open and the state-value changes.
    // In that case, "storage" events would bounce back and forth
    // if localStorage-updates weren't prevented.
    setupPersistedState({ value: 1 });
    localStorage.setItem(KEY, '{"value": 2}');
    const setItemSpy = vi.spyOn(Storage.prototype, "setItem");
    window.dispatchEvent(new Event("storage"));

    await Promise.resolve();
    expect(setItemSpy).not.toHaveBeenCalled();
  });
});
