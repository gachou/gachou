import { deepEqual } from "src/utils/deepEqual";

describe("deepEqual", () => {
  it.each([
    [1, 1],
    ["a", "a"],
    [
      [[1, 2], 2],
      [[1, 2], 2],
    ],
    [{ a: [1, 2] }, { a: [1, 2] }],
    [{ a: [{ b: 1 }, 2] }, { a: [{ b: 1 }, 2] }],
    [null, null],
    [undefined, undefined],
  ])("%j and %j are deep-equal", (x, y) => {
    expect(deepEqual(x, y)).toBe(true);
  });

  it.each([
    [1, 2],
    [
      [[1, 2], 2],
      [[1, 3], 2],
    ],
    [[[1, 3]], [[1, 3], 2]],
    [["a", "b"], "ab"],
    ["a", "ab"],
    [{ a: [{ b: 1 }, 2] }, { a: [{ b: 1, c: 1 }, 2] }],
    [null, "a"],
    ["a", null],
    [undefined, "a"],
    ["a", undefined],
  ])("%j and %j are not deep-equal", (x, y) => {
    expect(deepEqual(x, y)).toBe(false);
  });
});
