import { logInfo } from "src/utils/logger";

export async function fileChecksum(blob: Blob): Promise<number> {
  const start = performance.now();
  let checksum = 0;
  await blob.stream().pipeTo(
    new WritableStream<Uint8Array>({
      write(chunk) {
        for (const byte of chunk) {
          checksum = (checksum << 5) - checksum + byte;
          checksum |= 0;
        }
      },
    })
  );

  const duration = performance.now() - start;
  logInfo("checksum computation of " + blob.name + " took " + duration + "ms");
  return checksum;
}
