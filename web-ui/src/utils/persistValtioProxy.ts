import { snapshot, subscribe } from "valtio";
import { JsonLocalStorage } from "./jsonLocalStorage";
import { deepEqual } from "src/utils/deepEqual";

interface PersistStateResult {
  detachFromStorage: () => void;
}

export function persistValtioProxy<T>(
  state: { obj: T },
  localStorageKey: string
): PersistStateResult {
  const defaultValue = snapshot(state).obj;

  const storage = new JsonLocalStorage<T>(localStorageKey);
  state.obj = storage.getItem() ?? state.obj;

  const stopSyncToStorage = subscribe(state, () => {
    if (!deepEqual(state.obj, storage.getItem())) {
      storage.setItem(state.obj);
    }
  });

  const stopSyncFromStorage = addStorageListener(() => {
    state.obj = storage.getItem() ?? (defaultValue as T);
  });

  return {
    detachFromStorage() {
      stopSyncFromStorage();
      stopSyncToStorage();
    },
  };
}

// Returns the unsubscribe function
function addStorageListener(callback: () => void): () => void {
  window.addEventListener("storage", callback);
  return () => window.removeEventListener("storage", callback);
}
