import { Validator } from "src/utils/validation/helpers/Validator";

export function VArray<T>(itemValidator: Validator<T>): Validator<T[]> {
  return new Validator(
    "array",

    (x): x is T[] =>
      x != null &&
      Array.isArray(x) &&
      x.every((item) => itemValidator.guard(item)),

    { transform: (x) => x.map((item) => itemValidator.transform(item)) }
  );
}
