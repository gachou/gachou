import { Validator } from "src/utils/validation/helpers/Validator";

export type Static<T extends Validator<unknown>> = T extends Validator<infer X>
  ? X
  : never;
