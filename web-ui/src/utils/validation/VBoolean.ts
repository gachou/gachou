import { Validator } from "src/utils/validation/helpers/Validator";

export const VBoolean = new Validator(
  "boolean",
  (x): x is boolean => typeof x === "boolean"
);
