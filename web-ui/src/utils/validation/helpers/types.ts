export type Path = (string | number)[];
export type Check<T> = (x: unknown, path?: Path) => T;
export type Assertion<T> = (x: unknown, path?: Path) => asserts x is T;

export type Guard<T> = (x: unknown) => x is T;
export type Transform<T> = <I extends T>(x: I) => T;
