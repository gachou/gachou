import { Guard, Transform } from "./types";

export class Validator<T> {
  name: string;
  guard: Guard<T>;
  transform: Transform<T>;

  constructor(
    name: string,
    guard: Guard<T>,
    { transform = ((x) => x) as Transform<T> } = {}
  ) {
    this.name = name;
    this.guard = guard;
    this.transform = transform;
  }

  setName(name: string): this {
    this.name = name;
    return this;
  }

  check(x: unknown): T {
    if (this.guard(x)) {
      return this.transform(x);
    } else {
      throw new Error(
        `Value ${JSON.stringify(x)} does not match "${this.name}"`
      );
    }
  }
}
