import { Validator } from "src/utils/validation/helpers/Validator";

export function VLiteral<T extends string>(literal: T): Validator<T> {
  return new Validator(
    "literal " + JSON.stringify(literal),
    (x): x is T => x === literal
  );
}
