import {
  VArray,
  VRecord,
  VString,
  VBoolean,
  VNumber,
  VLiteral,
  VOptional,
  VUnion,
  Static,
} from "src/utils/validation";
import { assertType, expectTypeOf } from "vitest";
import { VNamed } from "src/utils/validation/VNamed";

describe("validation", () => {
  describe("String", () => {
    it("a number is not a string", () => {
      expect(VString.guard(1)).toBe(false);
    });

    it("a string is a valid string", () => {
      expect(VString.guard("a")).toBe(true);
    });
  });

  describe("VRecord", () => {
    const abRecord = VRecord({ a: VString, b: VString });

    it("a number is not a record", () => {
      expect(abRecord.guard(1)).toBe(false);
    });

    it("an invalid type in a property invalidates the record", () => {
      expect(abRecord.guard({ a: 1, b: "c" })).toBe(false);
    });

    it("a missing property invalidates the record", () => {
      expect(abRecord.guard({ a: "c" })).toBe(false);
    });

    it("an object with all valid properties is valid", () => {
      expect(abRecord.guard({ a: "c", b: "d" })).toBe(true);
    });

    it("an objet with additional properties is valid", () => {
      expect(abRecord.guard({ a: "c", b: "d", c: "e" })).toBe(true);
    });

    it("transform removes additional properties", () => {
      expect(abRecord.transform({ a: "c", b: "d", c: "e" })).toEqual({
        a: "c",
        b: "d",
      });
    });

    it("apply transform to child props", () => {
      const nestedRecord = VRecord({ x: VRecord({ a: VString, b: VString }) });
      expect(nestedRecord.transform({ x: { a: "c", b: "d", c: "e" } })).toEqual(
        {
          x: {
            a: "c",
            b: "d",
          },
        }
      );
    });
  });

  describe("VArray", () => {
    const arrayOfStrings = VArray(VString);
    it("a string is not an array", () => {
      expect(arrayOfStrings.guard("abc")).toBe(false);
    });

    it("an item of wrong type invalidates the array", () => {
      expect(arrayOfStrings.guard([1])).toBe(false);
      expect(arrayOfStrings.guard(["a", 2])).toBe(false);
    });

    it("if every item-type is correct, the array is valid", () => {
      expect(arrayOfStrings.guard(["a", "b", "c"])).toBe(true);
    });

    it("applies the transform method to each item (picking only valid properties", () => {
      const arrayOfABs = VArray(VRecord({ a: VString, b: VString }));
      expect(
        arrayOfABs.transform([
          { a: "c", b: "d", c: "e" },
          { a: "x", b: "y", c: "z" },
        ])
      ).toEqual([
        { a: "c", b: "d" },
        { a: "x", b: "y" },
      ]);
    });
  });

  describe("Boolean", () => {
    it("a boolean is valid", () => {
      expect(VBoolean.guard(true)).toBe(true);
      expect(VBoolean.guard(false)).toBe(true);
    });

    it("other types are not valid", () => {
      expect(VBoolean.guard("a")).toBe(false);
      expect(VBoolean.guard(1)).toBe(false);
      expect(VBoolean.guard({ a: 1 })).toBe(false);
    });
  });

  describe("Number", () => {
    it("a number is valid", () => {
      expect(VNumber.guard(1)).toBe(true);
    });

    it("other types are not valid", () => {
      expect(VNumber.guard("a")).toBe(false);
      expect(VNumber.guard(true)).toBe(false);
      expect(VNumber.guard({ a: 1 })).toBe(false);
    });
  });

  describe("Literal", () => {
    const abc = VLiteral("abc");
    it("returns the input if it has the expected literal value", () => {
      expect(abc.guard("abc")).toBe(true);
    });

    it("throws an error if the input has a different value", () => {
      expect(abc.guard("a")).toBe(false);
    });
  });

  describe("Optional", () => {
    const OptionalString = VOptional(VString);

    it("a value matching the child validator is valid", () => {
      expect(OptionalString.guard("abc")).toBe(true);
    });

    it("undefined is valid", () => {
      expect(OptionalString.guard(undefined)).toBe(true);
    });

    it("transform returns the default value if one is provided", () => {
      const StringWithDefault = VOptional(VString, { defaultValue: "ab" });
      expect(StringWithDefault.transform(undefined)).toBe("ab");
    });

    it("null is not valid", () => {
      expect(OptionalString.guard(null)).toBe(false);
    });

    it("non-null types are invalid if they don't match the child validator", () => {
      expect(OptionalString.guard(1)).toBe(false);
    });
  });

  describe("VUnion", () => {
    const aOrB = VUnion(VLiteral("a"), VLiteral("B"));

    it("returns true if any validator is successful", () => {
      expect(aOrB.guard("a")).toBe(true);
      expect(aOrB.guard("B")).toBe(true);
    });

    it("returns false if the input does not match any validator", () => {
      expect(aOrB.guard("c")).toBe(false);
    });

    it("returns a union type", () => {
      expectTypeOf<Static<typeof aOrB>>().toMatchTypeOf<"a" | "B">();
    });
  });

  describe("Static", () => {
    const OptionalRecord = VRecord({
      a: VOptional(VString),
      b: VOptional(VString),
    });

    it("computes types with optional properties correctly", () => {
      assertType<Static<typeof OptionalRecord>>({ a: "a" });
      assertType<Static<typeof OptionalRecord>>({ a: "a", b: "b" });
      // @ts-expect-error "c" is not part of type
      assertType<Static<typeof OptionalRecord>>({ a: "a", b: "b", c: "c" });
    });
  });

  describe("Validator.check", () => {
    const arrayOfABs = VNamed(
      "arrayOfABs",
      VArray(VRecord({ a: VString, b: VString }))
    );
    it("throws an exception if the type does not match", () => {
      expect(() => arrayOfABs.check([{ a: "c", x: "d" }])).toThrow(
        new Error('Value [{"a":"c","x":"d"}] does not match "arrayOfABs"')
      );
    });
  });
});
