/* eslint-disable @typescript-eslint/ban-ts-comment */
import { OptionalUndefined } from "./helpers/OptionalUndefined";
import { Validator } from "src/utils/validation/helpers/Validator";

type PropValidators<T> = {
  [Key in keyof T]: Validator<T[Key]>;
};

type RecordValidator<T> = Validator<OptionalUndefined<T>>;

export function VRecord<T>(propVal: PropValidators<T>): RecordValidator<T> {
  function guard(x: unknown): x is OptionalUndefined<T> {
    return x != null && typeof x === "object" && _allPropsValid(x);
  }

  function _allPropsValid(x: unknown) {
    return Object.entries(propVal).every(([key, validator]) => {
      // @ts-ignore
      return validator.guard(x[key]);
    });
  }

  function transform<I extends OptionalUndefined<T>>(x: I): T {
    const result: Partial<T> = {};
    for (const key of Object.keys(propVal)) {
      // @ts-ignore
      result[key] = propVal[key].transform(x[key]);
    }
    return result as T;
  }

  return new Validator("record", guard, { transform });
}
