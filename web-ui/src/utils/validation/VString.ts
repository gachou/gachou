import { Validator } from "src/utils/validation/helpers/Validator";

export const VString = new Validator(
  "string",
  (x): x is string => typeof x === "string"
);
