import { Validator } from "src/utils/validation/helpers/Validator";

export const VNumber = new Validator(
  "number",
  (x): x is number => typeof x === "number"
);
