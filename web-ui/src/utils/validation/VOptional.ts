import { Validator } from "src/utils/validation/helpers/Validator";

export function VOptional<T>(
  childValidator: Validator<T>,
  { defaultValue = undefined as T | undefined } = {}
): Validator<T | undefined> {
  return new Validator(
    "optional(" + childValidator.name + ")",
    (x): x is T | undefined => x === undefined || childValidator.guard(x),
    { transform: (x) => (x === undefined ? defaultValue : x) }
  );
}
