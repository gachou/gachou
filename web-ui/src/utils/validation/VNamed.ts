import { Validator } from "src/utils/validation/helpers/Validator";

export function VNamed<T>(name: string, validator: Validator<T>): Validator<T> {
  return new Validator(name, (x): x is T => validator.guard(x), {
    transform: (x) => validator.transform(x),
  });
}
