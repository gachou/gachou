import { Validator } from "src/utils/validation/helpers/Validator";

type ValidatedUnion<T> = T extends [Validator<infer First>, ...infer Rest]
  ? First | ValidatedUnion<Rest>
  : never;
type UnionValidator<T extends Validator<unknown>[]> = Validator<
  ValidatedUnion<T>
>;

export function VUnion<T extends Validator<unknown>[]>(
  ...children: T
): UnionValidator<T> {
  return new Validator("union", (x): x is ValidatedUnion<T> =>
    children.some((c) => c.guard(x))
  );
}
