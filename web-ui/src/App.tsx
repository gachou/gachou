import Container from "react-bootstrap/Container";
import React from "react";
import { GachouNavbar } from "./components/molecules/MyNavbar";
import { GModal } from "src/components/atoms/MyModal";
import { Router } from "wouter";
import { MyRoutes } from "src/routes/MyRoutes";
import { MyToastContainer } from "src/utils/toasts";
import { ErrorFooter } from "src/components/atoms/ErrorFooter/ErrorFooter";
import { auth } from "./adapter/authentication";
import { publicStatus } from "src/global-state/publicBackendStatus";

const App: React.FC = () => {
  const { currentUser } = auth.useAuthentication();
  const publicBackendStatus = publicStatus.useStatus();
  const backendError = publicStatus.useError();

  return (
    <div>
      <GModal
        show={
          publicBackendStatus != null && !publicBackendStatus.adminUserExists
        }
        title={"Create an admin user"}
      >
        <auth.MissingAdminUserInstructions />
      </GModal>
      <MyToastContainer />
      <GachouNavbar className={"mb-3"} />
      <Container>
        {currentUser == null ? <auth.LoginPage /> : <RouterView />}
      </Container>
      <ErrorFooter
        show={backendError != null}
        text={"Error loading data from backend"}
        spin={true}
      />
    </div>
  );
};

const RouterView: React.FC = () => {
  return (
    <Router>
      <MyRoutes />
    </Router>
  );
};

export default App;
