import { Upload } from "src/model/upload";
import { QueryableList } from "src/tests/utils/queryable-list";
import { generateId } from "src/utils/generateId";
import { ControlledProgress } from "src/tests/utils/ControlledProgress";
import {
  StartUploadOptions,
  UploadAdapter,
} from "src/adapter/upload/interface";
import { proxy } from "valtio";

export const progressByFile: Record<string, ControlledProgress> = {};
export const canceledUploads: Record<string, true> = {};
export const mockUploadedFiles: QueryableList<File> = new QueryableList<File>();

export const uploadAdapter: UploadAdapter = {
  async startUpload(file: File, options: StartUploadOptions): Promise<Upload> {
    const upload: Upload = proxy(createTestUpload(file));
    options.abortSignal?.addEventListener("abort", () => {
      canceledUploads[upload.id] = true;
    });
    runUpload(upload, file).catch(options.onError);
    return upload;
  },
};

async function runUpload(upload: Upload, file: File): Promise<void> {
  const progress = new ControlledProgress(upload.id, [0, 50, 100]);
  progressByFile[upload.id] = progress;
  for await (const step of progress.run()) {
    if (canceledUploads[upload.id]) return;
    upload.uploadedSize = (upload.totalSize * step) / 100;
  }
  mockUploadedFiles.add(file);
}

export async function resetMockTusUploader(): Promise<void> {
  for (const [fileId, progress] of Object.entries(progressByFile)) {
    await progress.finish();
    delete progressByFile[fileId];
  }
  for (const fileId of Object.keys(canceledUploads)) {
    delete canceledUploads[fileId];
  }
}

function createTestUpload(file: File): Upload {
  return {
    id: "server-side-id-" + generateId(),
    fileName: file.name,
    fileMimeType: file.type,
    fileLastModified: new Date(file.lastModified).toISOString(),
    totalSize: file.size,
    uploadedSize: 0,
    state: "RUNNING",
    activeOnClient: true,
  };
}
