import { Upload } from "src/model/upload";

export type OnUploadStarted = (upload: Upload) => void;
export type OnProgress = (id: string, bytesUploaded: number) => void;

export interface StartUploadOptions {
  onError: (error: Error) => void;
  abortSignal: AbortSignal;
}

export interface UploadAdapter {
  startUpload(file: File, options: StartUploadOptions): Promise<Upload>;
}
