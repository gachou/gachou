import { extractUploadFromTusPost } from "./extractUploadFromTusPost";
import { MockRequest, MockResponse } from "./test-helpers";
import { Upload } from "src/model/upload";

const metadata =
  "lastModified MjAyMi0xMS0xM1QxMTo0MDoxOC4yNDFa,name ZmFocnJhZC5qcGc=,type aW1hZ2UvanBlZw==,filetype aW1hZ2UvanBlZw==,filename ZmFocnJhZC5qcGc=,lastModified MjAyMi0xMS0xM1QxMTo0MDoxOC4yNDFa,name ZmFocnJhZC5qcGc=,type aW1hZ2UvanBlZw==,filetype aW1hZ2UvanBlZw==,filename ZmFocnJhZC5qcGc=";
const request = new MockRequest();
const response = new MockResponse();

describe("extractUploadFromTusPost", () => {
  beforeEach(() => {
    request.clear();
    response.clear();
  });

  it("returns an upload", () => {
    request.setHeader("Upload-Metadata", metadata);
    request.setHeader("Upload-Length", "123");
    response.setHeader(
      "Location",
      "Location: http://localhost:1080/files/9ab9a64ebc39eef0f73e3f094e16e36b+ZDY5MTdkNjktY2ZmMS00MjY2LWFiNTctZTU2N2Y4ODM5NTZjLmZiZjk4YmY4LTYxZjEtNDIxMC1hNzBiLTIzZTNiMzZjMGUyZA"
    );
    const upload: Upload = extractUploadFromTusPost(request, response);
    const expectedUpload: Upload = {
      id: "9ab9a64ebc39eef0f73e3f094e16e36b+ZDY5MTdkNjktY2ZmMS00MjY2LWFiNTctZTU2N2Y4ODM5NTZjLmZiZjk4YmY4LTYxZjEtNDIxMC1hNzBiLTIzZTNiMzZjMGUyZA",
      activeOnClient: true,
      fileMimeType: "image/jpeg",
      fileLastModified: "2022-11-13T11:40:18.241Z",
      fileName: "fahrrad.jpg",
      state: "RUNNING",
      totalSize: 123,
      uploadedSize: 0,
    };
    expect(upload).toEqual(expectedUpload);
  });
});
