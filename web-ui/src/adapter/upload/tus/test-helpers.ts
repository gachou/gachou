import { NecessaryRequestData } from "./types";

export class MockRequest implements NecessaryRequestData {
  private headers: Record<string, string> = {};
  getHeader(header: string): string {
    return this.headers[header];
  }

  setHeader(header: string, value: string): void {
    this.headers[header] = value;
  }

  clear() {
    this.headers = {};
  }
}

// For now, we only need the headers, so MockResponse and MockRequest
// are equal. This may change in the future
export const MockResponse = MockRequest;
