import { PreviousUpload } from "tus-js-client";
import { Upload } from "src/model/upload";

export function extractUploadFromPreviousUpload(
  file: File,
  previousUpload: PreviousUpload
): Upload {
  const uploadUrlSplit = previousUpload.uploadUrl.split("/");
  const uploadId = uploadUrlSplit[uploadUrlSplit.length - 1];
  return {
    id: uploadId,
    state: "RUNNING",
    activeOnClient: true,
    fileName: file.name,
    fileMimeType: file.type,
    fileLastModified: new Date(file.lastModified).toISOString(),
    uploadedSize: previousUpload.size ?? 0,
    totalSize: file.size,
  };
}
