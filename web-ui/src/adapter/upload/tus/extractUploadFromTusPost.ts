import { Upload } from "src/model/upload";
import { TusMetadataHeader } from "./tusMetadataHeader";
import { NecessaryRequestData, NecessaryResponseData } from "./types";

export function extractUploadFromTusPost(
  request: NecessaryRequestData,
  response: NecessaryResponseData
): Upload {
  const metadata = TusMetadataHeader.from(request).parse();
  const size = request.getHeader("Upload-Length");
  const location = response.getHeader("Location");
  const id = location.slice(location.lastIndexOf("/") + 1);
  return Upload.check({
    id,
    fileName: metadata.filename,
    fileMimeType: metadata.filetype,
    fileLastModified: metadata.lastModified,
    name: metadata.name,
    type: metadata.type,
    totalSize: parseInt(size),
    state: "RUNNING",
    uploadedSize: 0,
    activeOnClient: true,
  });
}
