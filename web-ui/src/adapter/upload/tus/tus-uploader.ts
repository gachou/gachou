import { tusUploadUrl } from "src/constants/tusUploadUrl";
import { defaults } from "src/backend/__generated__/openapi-client";
import { StartUploadOptions } from "src/adapter/upload/interface";
import { HttpRequest, Upload as TusUpload, UploadOptions } from "tus-js-client";
import { extractUploadFromTusPost } from "./extractUploadFromTusPost";
import { Upload } from "src/model/upload";
import { assertNotNull } from "src/utils/validation";
import { extractUploadFromPreviousUpload } from "src/adapter/upload/tus/extractUploadFromPreviousUpload";
import { proxy } from "valtio";
import { Deferred } from "src/utils/Deferred";

export class TusUploader {
  private readonly file: File;
  private readonly options: StartUploadOptions;
  private readonly tusOptions: UploadOptions;

  constructor(file: File, options: StartUploadOptions) {
    this.file = file;
    this.options = options;
    this.tusOptions = {
      endpoint: tusUploadUrl,
      metadata: {
        filename: this.file.name,
        filetype: this.file.type,
        lastModified: String(this.file.lastModified),
      },
      storeFingerprintForResuming: true,
      chunkSize: 10 * 1024 * 1024,
      removeFingerprintOnSuccess: false,
      onBeforeRequest: (request) => addDefaultHeaders(request),
    };
  }

  async startUpload(): Promise<Upload> {
    const deferredUpload = new Deferred<Upload>();
    const tusUpload = new TusUpload(this.file, {
      ...this.tusOptions,
      onAfterResponse: (request, response) => {
        if (request.getMethod() === "POST") {
          // This only happens if the upload is new
          deferredUpload.resolve(
            proxy(extractUploadFromTusPost(request, response))
          );
        }
      },
      onProgress: (bytesSent: number) => {
        assertNotNull(deferredUpload);
        // "upload" is a valtio proxy. If we change properties here, the returned value is reactive.
        deferredUpload.promise.then((upload) => {
          upload.uploadedSize = bytesSent;
        });
      },
      onError: this.options.onError,
    });

    const previousUploads = await tusUpload.findPreviousUploads();
    if (previousUploads.length > 0) {
      // This only happens if the upload is resumed
      deferredUpload.resolve(
        proxy(extractUploadFromPreviousUpload(this.file, previousUploads[0]))
      );
      tusUpload.resumeFromPreviousUpload(previousUploads[0]);
    }

    tusUpload.start();
    return deferredUpload.promise;
  }
}

function addDefaultHeaders(request: HttpRequest) {
  if (defaults.headers) {
    for (const [key, value] of Object.entries(defaults.headers)) {
      if (value != null) {
        request.setHeader(key, value);
      }
    }
  }
}
