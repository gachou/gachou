import { NecessaryRequestData } from "./types";

const UPLOAD_METADATA = "Upload-Metadata";

export class TusMetadataHeader {
  private request: NecessaryRequestData;

  static from(request: NecessaryRequestData) {
    return new TusMetadataHeader(request);
  }

  private constructor(request: NecessaryRequestData) {
    this.request = request;
  }

  parse(): Record<string, unknown> {
    const strings = this.request.getHeader(UPLOAD_METADATA).split(",");
    const entries = strings.map(parseEntry);
    return Object.fromEntries(entries);
  }

  addValue(key: string, value: string): this {
    const oldHeader = this.request.getHeader(UPLOAD_METADATA);
    const newHeader = `${oldHeader},${key} ${btoa(value)}`;
    this.request.setHeader(UPLOAD_METADATA, newHeader);
    return this;
  }
}

function parseEntry(entry: string) {
  const [key, value] = entry.split(" ");
  return [key, atob(value)];
}
