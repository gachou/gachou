import { HttpRequest, HttpResponse } from "tus-js-client";

export type NecessaryRequestData = Pick<HttpRequest, "getHeader" | "setHeader">;
export type NecessaryResponseData = Pick<HttpResponse, "getHeader">;
