import { TusMetadataHeader } from "./tusMetadataHeader";
import { MockRequest } from "./test-helpers";

const metadata =
  "lastModified MjAyMi0xMS0xM1QxMTo0MDoxOC4yNDFa,name ZmFocnJhZC5qcGc=,type aW1hZ2UvanBlZw==,filetype aW1hZ2UvanBlZw==,filename ZmFocnJhZC5qcGc=,lastModified MjAyMi0xMS0xM1QxMTo0MDoxOC4yNDFa,name ZmFocnJhZC5qcGc=,type aW1hZ2UvanBlZw==,filetype aW1hZ2UvanBlZw==,filename ZmFocnJhZC5qcGc=";

const request = new MockRequest();

const UPLOAD_METADATA = "Upload-Metadata";

describe("TusMetadataHeader", () => {
  beforeEach(() => {
    request.clear();
  });

  it("parses the metadata", () => {
    request.setHeader(UPLOAD_METADATA, metadata);
    expect(TusMetadataHeader.from(request).parse()).toEqual({
      filename: "fahrrad.jpg",
      filetype: "image/jpeg",
      lastModified: "2022-11-13T11:40:18.241Z",
      name: "fahrrad.jpg",
      type: "image/jpeg",
    });
  });

  it("augments the metadata", () => {
    request.setHeader(
      UPLOAD_METADATA,
      "lastModified MjAyMi0xMS0xM1QxMTo0MDoxOC4yNDFa"
    );
    TusMetadataHeader.from(request).addValue("customField", "some value");
    expect(request.getHeader(UPLOAD_METADATA)).toEqual(
      "lastModified MjAyMi0xMS0xM1QxMTo0MDoxOC4yNDFa,customField c29tZSB2YWx1ZQ=="
    );
  });
});
