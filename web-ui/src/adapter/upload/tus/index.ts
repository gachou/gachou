import { TusUploader } from "./tus-uploader";
import { UploadAdapter } from "src/adapter/upload/interface";

export const tusUploadAdapter: UploadAdapter = {
  async startUpload(file, options) {
    return new TusUploader(file, options).startUpload();
  },
};
