import { UploadAdapter } from "src/adapter/upload/interface";
import { toBeInitialized } from "src/utils/toBeInitialized";

export let uploadAdapter = toBeInitialized<UploadAdapter>("upload");

export function selectUploadAdapter(newUploadAdapter: UploadAdapter) {
  uploadAdapter = newUploadAdapter;
}
