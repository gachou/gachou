import { toBeInitialized } from "src/utils/toBeInitialized";
import { AuthAdapter } from "src/adapter/authentication/interface";

export let auth = toBeInitialized<AuthAdapter>("authentication");

export function selectAuthenticationAdapter(adapter: AuthAdapter) {
  auth = adapter;
}
