import React from "react";
import { Immutable } from "src/utils/immutable";
import { User } from "src/model/user";

export interface AuthAdapter {
  LoginPage: React.FC;
  MissingAdminUserInstructions: React.FC;
  useAuthentication: UseAuthenticationHook;
  logout: () => Promise<void>;
  destroy: () => Promise<void>;
}

export type UseAuthenticationHook = () => UseAuthResult;

export interface UseAuthResult {
  authenticationInitialized: boolean;
  logout(): Promise<void>;
  currentUser: Immutable<User | null>;
}
