import React, { useState } from "react";
import { Button, Form, InputGroup } from "src/utils/bootstrap";
import { encryptPassword } from "src/backend/__generated__/openapi-client";
import { SubmitHandler, useForm } from "react-hook-form";
import { backendConfigEnvVar, backendConfigKey } from "src/config";
import { CopyToClipboard } from "src/components/molecules/CopyToClipboard";

interface Inputs {
  password: string;
}

const adminUserKey = backendConfigKey(
  "gachou.auth.admin-user.encrypted-password"
);
const adminUserEnvVar = backendConfigEnvVar(
  "GACHOU_AUTH_ADMIN_USER_ENCRYPTED_PASSWORD"
);

export const MissingAdminUserInstructions: React.FC = () => {
  const [encryptedPassword, setEncryptedPassword] = useState<string>();
  const { register, handleSubmit } = useForm<Inputs>();

  const onSubmit: SubmitHandler<Inputs> = async ({ password }) => {
    const response = await encryptPassword({ password });
    setEncryptedPassword(response.data.encryptedPassword);
  };

  return (
    <div>
      <p>To finish the setup of Gachou, you need to create an admin user.</p>
      <ul>
        <li>Please enter a password into the text-field below.</li>
        <li>Click "encrypt password"</li>
      </ul>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Form.Floating>
          <InputGroup>
            <Form.Control
              type="password"
              placeholder="Password"
              {...register("password")}
            />
            <Button variant="primary" type="submit">
              Encrypt password
            </Button>
          </InputGroup>
        </Form.Floating>
      </Form>
      <hr />
      {encryptedPassword != null && (
        <div>
          You encrypted password is:
          <CopyToClipboard
            className={"my-2"}
            text={encryptedPassword}
            title={"Copy encrypted password to clipboard"}
          />
          <p className={"mt-2"}>
            You can now add the property to the backend configuration:
          </p>
          <h5>System property or application.properties</h5>
          <CopyToClipboard
            className={"my-2"}
            text={adminUserKey + "=" + encryptedPassword}
            title={"Copy config entry to clipboard"}
          />
          <h5>Environment variable</h5>
          <CopyToClipboard
            className={"my-2"}
            text={adminUserEnvVar + "=" + encryptedPassword}
            title={"Copy environment variable to clipboard"}
          />
        </div>
      )}
    </div>
  );
};
