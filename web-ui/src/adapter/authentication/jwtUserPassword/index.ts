import {
  initTokenListeners,
  setLoggedOut,
} from "src/adapter/authentication/jwtUserPassword/store/userToken";
import { resetFailures } from "src/adapter/authentication/jwtUserPassword/store/failureCount";
import { AuthAdapter } from "src/adapter/authentication/interface";

import { LoginPage } from "./LoginPage";
import { useAuthentication } from "./useAuthentication";
import { MissingAdminUserInstructions } from "./MissingAdminUserInstructions";
export { setLoggedOut } from "./store/userToken";

export default function createJwtUserPasswordAdapter(): AuthAdapter {
  setLoggedOut();
  resetFailures();
  const { cleanupTokenListeners } = initTokenListeners();
  return {
    LoginPage,
    useAuthentication,
    MissingAdminUserInstructions,
    async logout() {
      setLoggedOut();
    },
    async destroy() {
      cleanupTokenListeners();
    },
  };
}
