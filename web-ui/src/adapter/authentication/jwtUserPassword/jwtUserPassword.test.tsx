import { describe, expect, it } from "vitest";
import { act, fireEvent, screen, waitFor } from "@testing-library/react";
import { oas } from "src/tests/openapi-msw";
import userEvent from "@testing-library/user-event";
import { customRender } from "src/tests/utils/custom-render";
import React from "react";
import {
  requests,
  setupTestMockApi,
} from "src/tests/mock-backend/setup-test-mock-api";
import { LoginPage } from "src/adapter/authentication/jwtUserPassword/LoginPage";
import { fetchUserInfo } from "src/backend/userInfo/fetchUserInfo";
import { reloadPage } from "src/utils/reloadPage";
import createJwtUserPasswordAdapter from "src/adapter/authentication/jwtUserPassword/index";
import { AuthAdapter } from "src/adapter/authentication/interface";
import { expectNotNull } from "src/tests/utils/expectNotNull";
import { selectAuthenticationAdapter } from "src/adapter/authentication";

const BOBS_PASSWORD = "pw-bob";
const BOB_USER_TOKEN =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1cG4iOiJib2IiLCJncm91cHMiOlsiVVNFUiJdLCJpYXQiOjE2NTQ5NTk0NjEsImV4cCI6MTY1NTA0NTg2MSwianRpIjoiNTQxMjkwY2MtNzYxYS00ZDYzLWJmZTctMGFkNWNhN2I3MTIxIn0.ys3t9Zp18HXgbOdzuhX7Osb9pWJ3HX1BorY1pG_3nS0";
const EXPECTED_AUTH_HEADER = "Bearer " + BOB_USER_TOKEN;

selectAuthenticationAdapter(createJwtUserPasswordAdapter());

setupTestMockApi(
  oas.post("/api/login", (req, res, context) => {
    if (req.body.password === BOBS_PASSWORD) {
      return res(
        context.json({
          token: BOB_USER_TOKEN,
        })
      );
    }
    return res(context.status(401));
  }),
  oas.get("/api/me/user", (req, res, context) => {
    return res(
      context.json({
        username: "test",
        roles: ["USER"],
      })
    );
  })
);

let adapter: AuthAdapter | null = null as unknown as AuthAdapter;

async function simulatePageReload() {
  if (adapter != null) {
    await adapter.destroy();
  }
  adapter = createJwtUserPasswordAdapter();
}

describe("authentication", () => {
  beforeEach(async () => {
    localStorage.clear();
    await simulatePageReload();
  });

  it("parses the correct roles from the token", async () => {
    customRender(<LoginPageTestWrapper />);
    await submitLoginForm("bob", BOBS_PASSWORD);
    await waitForLoginSuccess();
    expect(screen.getByTestId("roles")).toHaveTextContent("USER");
  });

  it("sends access token for requests after successful login", async () => {
    customRender(<LoginPageTestWrapper />);
    await submitLoginForm("bob", BOBS_PASSWORD);
    await waitForLoginSuccess();

    const usersMe = await captureUserInfoRequest();
    expect(usersMe.headers).toEqual(
      expect.objectContaining({ authorization: EXPECTED_AUTH_HEADER })
    );
  });

  it("sends no access token if login failed", async () => {
    customRender(<LoginPageTestWrapper />);
    await submitLoginForm("bob", "wrong-password");
    await waitForFailedLogin();

    const usersMe = await captureUserInfoRequest();
    expect(usersMe.headers).toEqual(
      expect.not.objectContaining({ authorization: EXPECTED_AUTH_HEADER })
    );
  });

  it("sends no access token after logout", async () => {
    await loginAndLogout();
    const usersMe = await captureUserInfoRequest();
    expect(usersMe.headers).toEqual(
      expect.not.objectContaining({ authorization: EXPECTED_AUTH_HEADER })
    );
  });

  it("reloads page after logout", async () => {
    await loginAndLogout();
    expect(reloadPage).toHaveBeenCalled();
  });

  it("clears session-storage after logout", async () => {
    sessionStorage.setItem("test-item", "test-value");
    await loginAndLogout();
    await waitFor(() => {
      expect(sessionStorage.getItem("test-item")).toBe(null);
    });
  });

  it("keeps authentication across page reloads", async () => {
    const { unmount } = customRender(<LoginPageTestWrapper />);

    await submitLoginForm("bob", BOBS_PASSWORD);
    await waitForLoginSuccess();

    unmount();
    await simulatePageReload();
    customRender(<LoginPageTestWrapper />);
    await waitForAuthInitialized();
    const usersMe = await captureUserInfoRequest();
    expect(usersMe.headers).toEqual(
      expect.objectContaining({ authorization: EXPECTED_AUTH_HEADER })
    );
  });

  it("keeps logged-out state in localstorage", async () => {
    const { unmount } = customRender(<LoginPageTestWrapper />);
    await submitLoginForm("bob", BOBS_PASSWORD);
    await waitForLoginSuccess();
    clickLogoutButton();
    await waitForLogout();
    unmount();
    await simulatePageReload();

    customRender(<LoginPageTestWrapper />);
    await waitForAuthInitialized();
    const usersMe = await captureUserInfoRequest();
    expect(usersMe.headers).toEqual(
      expect.not.objectContaining({ authorization: EXPECTED_AUTH_HEADER })
    );
  });
});

const LoginPageTestWrapper: React.FC = () => {
  expectNotNull(adapter);
  const { currentUser, logout, authenticationInitialized } =
    adapter.useAuthentication();
  return (
    <div>
      <LoginPage />
      <button onClick={() => logout()}>Logout</button>
      <button onClick={fetchUserInfo}>Fetch user-info</button>
      <div data-testid={"currentUser"}>{currentUser?.username}</div>
      <div data-testid={"roles"}>{currentUser?.roles}</div>
      <div data-testid={"initialized"}>
        {authenticationInitialized ? "yes" : "no"}
      </div>
    </div>
  );
};

async function submitLoginForm(username: string, password: string) {
  const usernameInput = screen.getByPlaceholderText("username");
  await userEvent.type(usernameInput, username);

  const passwordInput = screen.getByPlaceholderText("password");
  await userEvent.type(passwordInput, password);

  fireEvent.click(screen.getByText("Login"));
}

async function waitForAuthInitialized() {
  await waitFor(() => {
    expect(screen.getByTestId("initialized")).toHaveTextContent("yes");
  });
}

async function waitForLoginSuccess() {
  await waitFor(() => {
    expect(screen.getByTestId("currentUser")).toHaveTextContent("bob");
  });
}

async function waitForFailedLogin() {
  await screen.findByText("Login failed", { exact: false });
}

async function captureUserInfoRequest() {
  screen.getByText("Fetch user-info").click();
  return await requests.find({
    pathname: "/api/me/user",
    method: "GET",
  });
}

async function waitForLogout() {
  await waitFor(() => {
    expect(screen.getByTestId("currentUser")).toBeEmptyDOMElement();
  });
}
function clickLogoutButton() {
  const logoutButton = screen.getByText("Logout");
  act(() => logoutButton.click());
}

async function loginAndLogout() {
  customRender(<LoginPageTestWrapper />);
  await submitLoginForm("bob", BOBS_PASSWORD);
  await waitForLoginSuccess();
  clickLogoutButton();
  await waitForLogout();
}
