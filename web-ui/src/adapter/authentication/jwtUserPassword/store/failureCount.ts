import { proxy, useSnapshot } from "valtio";

const state = proxy<{ count: number }>({
  count: 0,
});

export function addFailure() {
  state.count++;
}

export function resetFailures() {
  state.count = 0;
}

export function useFailureCount() {
  return useSnapshot(state).count;
}
