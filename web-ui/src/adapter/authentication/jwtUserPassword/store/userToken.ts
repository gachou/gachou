import { proxy, useSnapshot } from "valtio";
import { persistValtioProxy } from "src/utils/persistValtioProxy";
import { watch } from "valtio/utils";
import { defaults } from "src/backend/__generated__/openapi-client";
import { User } from "src/model/user";
import { Immutable } from "src/utils/immutable";

export interface LoggedInState {
  authenticated: true;
  user: User;
  accessToken: string;
}

export interface LoggedOutState {
  authenticated: false;
}

export type AuthenticationState = LoggedInState | LoggedOutState;

const state = proxy<{ obj: AuthenticationState }>({
  obj: { authenticated: false },
});

export function setLoggedIn(user: User, accessToken: string): void {
  state.obj = { user, accessToken, authenticated: true };
}

export function setLoggedOut(): void {
  state.obj = { authenticated: false };
}

export function useAuthState(): Immutable<AuthenticationState> {
  return useSnapshot(state).obj;
}

export function initTokenListeners(): {
  cleanupTokenListeners: () => void;
} {
  state.obj = { authenticated: false };
  const { detachFromStorage } = persistValtioProxy(state, "gachou-auth");
  const unwatch = watch((get) => updateAuthHeaders(get(state).obj));

  return {
    cleanupTokenListeners() {
      detachFromStorage();
      unwatch();
    },
  };
}

function updateAuthHeaders(authState: AuthenticationState) {
  defaults.headers = defaults.headers ?? {};
  if (authState?.authenticated) {
    defaults.headers.Authorization = "Bearer " + authState.accessToken;
  } else {
    delete defaults.headers.Authorization;
  }
}
