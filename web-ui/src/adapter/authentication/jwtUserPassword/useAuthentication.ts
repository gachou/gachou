import { doLogin } from "src/backend/auth/login";
import { UnauthorizedError } from "src/backend/errors";
import { setLoggedIn, setLoggedOut, useAuthState } from "./store/userToken";
import {
  addFailure,
  resetFailures,
  useFailureCount,
} from "./store/failureCount";
import { parseJwt } from "src/utils/parseJwt";
import { User } from "src/model/user";
import { useLocation } from "wouter";
import { resolveRoute } from "src/routes/routes";
import { reloadPage } from "src/utils/reloadPage";
import { UseAuthResult } from "src/adapter/authentication/interface";

interface UseJwtUserPasswordResult extends UseAuthResult {
  authenticationInitialized: boolean;

  login(username: string, password: string): Promise<void>;

  failureCount: number;
}

export function useAuthentication(): UseJwtUserPasswordResult {
  const authState = useAuthState();
  const failureCount = useFailureCount();
  const [ignoredLocation, setLocation] = useLocation();

  return {
    failureCount,
    authenticationInitialized: authState != null,
    logout() {
      setLoggedOut();
      setLocation(resolveRoute("/"));
      sessionStorage.clear();
      reloadPage();
      return Promise.resolve();
    },
    async login(username, password) {
      try {
        const accessToken = await doLogin(username, password);
        const user = parseUser(accessToken);
        setLoggedIn(user, accessToken);
        resetFailures();
      } catch (error) {
        if (error instanceof UnauthorizedError) {
          setLoggedOut();
          addFailure();
        }
      }
    },
    currentUser: authState.authenticated ? authState.user : null,
  };
}

function parseUser(accessToken: string) {
  const parsedToken = parseJwt(accessToken);
  return User.check({
    username: parsedToken.upn,
    roles: parsedToken.groups,
  });
}
