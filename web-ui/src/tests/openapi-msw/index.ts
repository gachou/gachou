import {
  ResponseResolver,
  RestContext,
  RestHandler,
  RestRequest,
  setupWorker,
  SetupWorkerApi,
} from "msw";
import {
  GachouResponseTypes,
  GachouRestMsw,
  RestHandlerConfig,
} from "./__generated__/openapi-msw-types";
import { Mutable } from "src/utils/immutable";

export type { RestHandlerConfig } from "./__generated__/openapi-msw-types";

type RestResolver = ResponseResolver<RestRequest, RestContext>;

function cast(config: RestHandlerConfig): RestHandlerConfig {
  return config as RestHandlerConfig;
}

export const oas: GachouRestMsw = {
  post: (path, resolver) =>
    cast({ method: "POST", path, resolver: resolver as ResponseResolver }),
  get: (path, resolver) =>
    cast({ method: "GET", path, resolver: resolver as ResponseResolver }),
  put: (path, resolver) =>
    cast({ method: "PUT", path, resolver: resolver as ResponseResolver }),
  delete: (path, resolver) =>
    cast({ method: "DELETE", path, resolver: resolver as ResponseResolver }),
};

export const untypedRest = {
  post: (path: string, resolver: RestResolver): RestHandlerConfig =>
    cast({ method: "POST", path, resolver }),
  get: (path: string, resolver: RestResolver): RestHandlerConfig =>
    cast({ method: "GET", path, resolver }),
  patch: (path: string, resolver: RestResolver): RestHandlerConfig =>
    cast({ method: "PATCH", path, resolver }),
  put: (path: string, resolver: RestResolver): RestHandlerConfig =>
    cast({ method: "PUT", path, resolver }),
  delete: (path: string, resolver: RestResolver): RestHandlerConfig =>
    cast({ method: "DELETE", path, resolver }),
  head: (path: string, resolver: RestResolver): RestHandlerConfig =>
    cast({ method: "HEAD", path, resolver }),
};

export function createHandler(config: RestHandlerConfig): RestHandler {
  return new RestHandler(config.method, config.path, config.resolver);
}

export function respondError(status: number, message: string): RestResolver {
  return (req, res, context) =>
    res(context.status(status), context.json({ status, message }));
}

export type OpenApiResponseType<
  Path extends keyof GachouResponseTypes,
  Method extends keyof GachouResponseTypes[Path]
> = Mutable<GachouResponseTypes[Path][Method]>;

export function mySetupWorker(
  ...handlerConfigs: RestHandlerConfig[]
): SetupWorkerApi {
  return setupWorker(...handlerConfigs.map(createHandler));
}
