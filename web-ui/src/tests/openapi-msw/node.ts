import { createHandler, RestHandlerConfig } from "src/tests/openapi-msw/index";
import { setupServer, SetupServerApi } from "msw/node";

export function mySetupServer(
  ...handlerConfigs: RestHandlerConfig[]
): SetupServerApi {
  return setupServer(...handlerConfigs.map(createHandler));
}
