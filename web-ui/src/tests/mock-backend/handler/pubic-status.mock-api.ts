import { oas, RestHandlerConfig } from "src/tests/openapi-msw";

export function createPublicStatusHandler(): RestHandlerConfig {
  return oas.get("/api/status/public", (req, res, context) => {
    return res(context.json({ adminUserExists: true }));
  });
}
