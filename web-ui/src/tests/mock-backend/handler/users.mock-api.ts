import { RestHandlerConfig, oas, respondError } from "src/tests/openapi-msw";
import { usersMockStore } from "src/tests/mock-backend/store/users.mock-store";

export function createUsersHandlers(): RestHandlerConfig[] {
  return [
    oas.get("/api/users", (req, res, context) =>
      res(context.json(usersMockStore.listMockUsers()))
    ),
    oas.get("/api/users/:username", (req, res, context) => {
      const username: string = req.params.username;
      const user = usersMockStore.findMockUser(username);
      if (user == null) {
        return respondError(404, "User not found: " + username)(
          req,
          res,
          context
        );
      }
      return res(context.json(user));
    }),
    oas.put("/api/users/:username", (req, res, context) => {
      const username: string = req.params.username;
      const user = usersMockStore.findMockUser(username);
      if (user == null) {
        return respondError(404, "User not found: " + username)(
          req,
          res,
          context
        );
      }
      // We don't store passwords in the mock backend yet, so there is nothing to do here.
      return res(context.json(user));
    }),
    oas.post("/api/users", (req, res, context) => {
      const username = req.body.username;
      const user = usersMockStore.findMockUser(username);
      if (user != null) {
        return respondError(409, "User already exists: " + username)(
          req,
          res,
          context
        );
      }
      return res(context.json(usersMockStore.addMockUser(username)));
    }),
    oas.delete("/api/users/:username", (req, res, context) => {
      const username: string = req.params.username;
      const user = usersMockStore.removeMockUser(username);
      if (user == null) {
        return respondError(404, "User not found: " + username)(
          req,
          res,
          context
        );
      }
      return res(context.json(user));
    }),
  ];
}
