import { RestHandlerConfig, oas } from "src/tests/openapi-msw";
import { User } from "src/model/user";
import { parseJwt } from "src/utils/parseJwt";

export function createUsersMeHandler(): RestHandlerConfig {
  return oas.get("/api/me/user", (req, res, context) => {
    const authorization = req.headers.get("authorization");
    if (authorization == null) {
      return res(context.status(401));
    }
    const token = authorization.replace(/^Bearer /, "");
    const parsedToken = parseJwt(token);
    return res(
      context.json(
        User.check({
          username: parsedToken.upn,
          roles: parsedToken.groups,
        })
      )
    );
  });
}
