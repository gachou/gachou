import { RestHandlerConfig, oas } from "src/tests/openapi-msw";

export function createAuthenticationHandlers(): RestHandlerConfig[] {
  const users: Record<string, { password: string; token: string }> = {
    bob: {
      password: "bob",
      token:
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1cG4iOiJib2IiLCJncm91cHMiOlsiVVNFUiJdLCJpYXQiOjE2NTQ5NTkwNjMsImV4cCI6MTY1NTA0NTQ2MywianRpIjoiNDA5ZDJkMTgtMTY5OS00MTJiLThkOTYtM2Q1Njk0ZTNlYjNlIn0.kFkqH0EjN2Bxge5EwHLzgr9pnZDYKlDI4XO8ys_gfxc",
    },
    alice: {
      password: "alice",
      token:
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1cG4iOiJhbGljZSIsImdyb3VwcyI6WyJVU0VSIl0sImlhdCI6MTY1NDk1OTA2MywiZXhwIjoxNjU1MDQ1NDYzLCJqdGkiOiI0MDlkMmQxOC0xNjk5LTQxMmItOGQ5Ni0zZDU2OTRlM2ViM2UifQ.X2GeyKGY5RV0mZ9S2oQ21ddCdVE5AumE2VHKBOFoSnk",
    },
    admin: {
      password: "admin",
      token:
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1cG4iOiJhZG1pbiIsImdyb3VwcyI6WyJBRE1JTiJdLCJpYXQiOjE2NTQ5NTkwNjMsImV4cCI6MTY1NTA0NTQ2MywianRpIjoiNDA5ZDJkMTgtMTY5OS00MTJiLThkOTYtM2Q1Njk0ZTNlYjNlIn0.B1tH6vK0KQIQ0CfrS6SC8yb7WgT4fJi-RN9yy1nScxM",
    },
  };

  return [
    oas.post("/api/login", (req, res, context) => {
      const username = req.body.username;
      const user = users[username];
      if (user == null) {
        return res(context.status(401));
      }
      if (user.password !== req.body.password) {
        return res(context.status(401));
      }
      return res(context.json({ token: user.token }));
    }),
  ];
}
