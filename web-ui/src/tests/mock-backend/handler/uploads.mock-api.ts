import { oas, RestHandlerConfig } from "src/tests/openapi-msw";
import {
  UploadNotFoundError,
  uploadsMockStore,
} from "src/tests/mock-backend/store/uploads.mock-store";
import { logError } from "src/utils/logger";

export function createUploadsHandlers(): RestHandlerConfig[] {
  return [
    oas.get("/api/uploads", async (req, res, context) => {
      return res(
        context.status(200),
        context.json({
          uploads: await uploadsMockStore.listAllUploads(),
        })
      );
    }),
    oas.delete("/api/uploads/:id", async (req, res, context) => {
      try {
        await uploadsMockStore.cancelUpload(req.params.id);
        return res(context.status(200));
      } catch (e) {
        if (e instanceof UploadNotFoundError) {
          return res(context.status(404));
        }
        logError(e);
        return res(context.status(500));
      }
    }),
  ];
}
