import { createAuthenticationHandlers } from "src/tests/mock-backend/handler/authentication.mock-api";
import { createUsersMeHandler } from "src/tests/mock-backend/handler/users-me.mock-api";
import { createPublicStatusHandler } from "src/tests/mock-backend/handler/pubic-status.mock-api";
import { createUsersHandlers } from "src/tests/mock-backend/handler/users.mock-api";
import { usersMockStore } from "src/tests/mock-backend/store/users.mock-store";
import { allowedRoles } from "src/tests/mock-backend/wrapper/allowRoles";
import { mySetupWorker } from "src/tests/openapi-msw";
import { createUploadsHandlers } from "src/tests/mock-backend/handler/uploads.mock-api";
import { logError } from "src/utils/logger";

export async function setupDevServerMockApi(): Promise<void> {
  usersMockStore.init({
    users: [
      { username: "admin", roles: ["ADMIN"] },
      { username: "alice", roles: ["USER"] },
      { username: "bob", roles: ["USER"] },
    ],
  });
  usersMockStore.persist();

  const worker = mySetupWorker(
    ...createAuthenticationHandlers(),
    createUsersMeHandler(),
    createPublicStatusHandler(),
    ...allowedRoles(["ADMIN"], createUsersHandlers()),
    ...allowedRoles(["USER"], createUploadsHandlers())
  );

  await worker.start({ onUnhandledRequest: "bypass", waitUntilReady: true });
  worker.events.on("request:unhandled", (request) => {
    if (request.url.pathname.match(/^\/api/))
      logError("Unmocked request", request.method + " " + request.url.pathname);
  });
}
