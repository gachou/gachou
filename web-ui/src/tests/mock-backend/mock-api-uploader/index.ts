import {
  StartUploadOptions,
  UploadAdapter,
} from "src/adapter/upload/interface";
import { MockUpload } from "src/tests/mock-backend/store/uploads.mock-store";
import { proxy } from "valtio";
import { Upload } from "src/model/upload";
import { waitMillis } from "src/tests/utils/waitMillis";

const MOCK_UPLOAD_BYTES_PER_SECOND = 10000000;
const MOCK_UPLOAD_INTERVAL_MILLIS = 100;

const MOCK_UPLOAD_CHUNK_SIZE =
  (MOCK_UPLOAD_BYTES_PER_SECOND * MOCK_UPLOAD_INTERVAL_MILLIS) / 1000;

export const mockApiUploadAdapter: UploadAdapter = {
  async startUpload(file: File, options: StartUploadOptions): Promise<Upload> {
    return new MockApiUpload(file, options).startUpload();
  },
};

class MockApiUpload {
  private file: File;
  private options: StartUploadOptions;
  private mockUpload: MockUpload;
  private upload: Upload;
  constructor(file: File, options: StartUploadOptions) {
    this.file = file;
    this.options = options;
    this.mockUpload = new MockUpload(file);
    this.upload = proxy(this.mockUpload.asInitialUpload());
  }

  public async startUpload(): Promise<Upload> {
    await this.mockUpload.save();
    this.simulateProgress().catch(this.options.onError);
    return this.upload;
  }

  async simulateProgress(): Promise<void> {
    for (let i = 0; i < this.upload.totalSize; i += MOCK_UPLOAD_CHUNK_SIZE) {
      await this.mockUpload.saveUploadedSize(i);
      this.upload.uploadedSize = i;
      await waitMillis(MOCK_UPLOAD_INTERVAL_MILLIS);
      if (this.options.abortSignal.aborted) return;
    }
    await this.mockUpload.saveUploadedSize(this.upload.totalSize);
    this.upload.uploadedSize = this.upload.totalSize;
  }
}
