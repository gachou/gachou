import { proxy, snapshot } from "valtio";
import { persistValtioProxy } from "src/utils/persistValtioProxy";
import { OpenApiResponseType } from "src/tests/openapi-msw";
import { Immutable } from "src/utils/immutable";

type ApiUserList = OpenApiResponseType<"/api/users", "get">;
type ApiUser = OpenApiResponseType<"/api/users/{username}", "get">;

const store = proxy<{ obj: ApiUserList }>({
  obj: {
    users: [],
  },
});

export const usersMockStore = {
  init(value: ApiUserList) {
    store.obj = value;
  },
  persist() {
    return persistValtioProxy(store, "mock-api-users");
  },
  addMockUser(username: string): ApiUser {
    const newUser: ApiUser = {
      username: username,
      roles: [username === "admin" ? "ADMIN" : "USER"],
    };
    store.obj.users.push(newUser);
    return newUser;
  },
  findMockUser(username: string): ApiUser | undefined {
    return store.obj.users.find((user) => user.username === username);
  },
  listMockUsers(): Immutable<ApiUserList> {
    return snapshot(store.obj);
  },
  removeMockUser(username: string): ApiUser | undefined {
    try {
      return this.findMockUser(username);
    } finally {
      store.obj.users = store.obj.users.filter(
        (user) => user.username !== username
      );
    }
  },
};
