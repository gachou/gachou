import {
  entries as idbEntries,
  set as idbSet,
  update as idbUpdate,
  del as idbDel,
  get as idbGet,
} from "idb-keyval";
import { GachouResponseTypes } from "src/tests/openapi-msw/__generated__/openapi-msw-types";
import { Upload } from "src/model/upload";
import { generateId } from "src/utils/generateId";

const IDB_GACHOU_FILE_DATA_PREFIX = "gachou-mock-upload-data-";
const IDB_GACHOU_FILE_METADATA_PREFIX = "gachou-mock-upload-meta-data-";

type ListUploadsResponse = GachouResponseTypes["/api/uploads"]["get"];
type UploadResponse = ListUploadsResponse["uploads"][number];

export class UploadNotFoundError extends Error {}

export const uploadsMockStore = {
  async listAllUploads(): Promise<UploadResponse[]> {
    const result: UploadResponse[] = [];
    for (const [key, value] of await idbEntries<string, UploadResponse>()) {
      if (key.startsWith(IDB_GACHOU_FILE_METADATA_PREFIX)) {
        result.push(value);
      }
    }
    return result;
  },
  async cancelUpload(id: string): Promise<UploadResponse | null> {
    const idbMetadataKey = IDB_GACHOU_FILE_METADATA_PREFIX + id;
    const idbDataKey = IDB_GACHOU_FILE_DATA_PREFIX + id;
    const result = await idbGet<UploadResponse>(idbMetadataKey);
    if (result == null)
      throw new UploadNotFoundError("Upload not found: " + id);
    await Promise.all([idbDel(idbMetadataKey), idbDel(idbDataKey)]);
    return result;
  },
};

export class MockUpload {
  readonly id: string;
  readonly file: File;
  private readonly idbDataKey: string;
  private readonly idbMetadataKey: string;

  constructor(file: File) {
    this.id = "upload_" + generateId();
    this.file = file;
    this.idbDataKey = IDB_GACHOU_FILE_DATA_PREFIX + this.id;
    this.idbMetadataKey = IDB_GACHOU_FILE_METADATA_PREFIX + this.id;
  }

  async save(): Promise<void> {
    await Promise.all([
      idbSet(this.idbDataKey, await this.file.arrayBuffer()),
      idbSet(this.idbMetadataKey, this.asInitialUploadResponse()),
    ]);
  }

  async saveUploadedSize(offsetBytes: number): Promise<void> {
    await idbUpdate<UploadResponse>(this.idbMetadataKey, (oldValue) => {
      if (oldValue == null)
        throw new Error(
          "Unexpected missing metadata for " + this.idbMetadataKey
        );
      return {
        ...oldValue,
        uploadedSize: offsetBytes,
      };
    });
  }

  private asInitialUploadResponse(): UploadResponse {
    return {
      id: this.id,
      uploadedSize: 0,
      totalSize: this.file.size,
      state: "RUNNING",
      audit: {
        createdAt: new Date().toISOString(),
        createdBy: "Nils",
        updatedAt: new Date().toISOString(),
        updatedBy: "Nils",
      },
      fileName: this.file.name,
      fileMimeType: this.file.type,
      fileLastModified: new Date(this.file.lastModified).toISOString(),
      s3ObjectKey: "s3" + this.id,
      s3Bucket: "uploads",
    };
  }

  asInitialUpload(): Upload {
    return {
      id: this.id,
      fileName: this.file.name,
      fileMimeType: this.file.type,
      fileLastModified: new Date(this.file.lastModified).toISOString(),
      totalSize: this.file.size,
      uploadedSize: 0,
      state: "RUNNING",
      activeOnClient: true,
    };
  }
}
