import { Role } from "src/model/role";
import { parseJwt } from "src/utils/parseJwt";
import type { RestHandlerConfig } from "src/tests/openapi-msw";

export function allowedRoles(
  allowedRoles: Role[],
  handlerConfigs: RestHandlerConfig[]
): RestHandlerConfig[] {
  return handlerConfigs.map(({ method, path, resolver }) => {
    return {
      method,
      path,
      resolver: (req, res, context) => {
        const authorizationHeader = req.headers.get("authorization");
        if (authorizationHeader == null) {
          return res(context.status(401));
        }
        const roles = getRolesFromAuthToken(authorizationHeader);
        if (!hasAnyRole(roles, allowedRoles)) {
          return res(context.status(403));
        }
        return resolver(req, res, context);
      },
    };
  });
}

function getRolesFromAuthToken(authorization: string) {
  const token = authorization.replace(/^Bearer /, "");
  const parsedToken = parseJwt(token);
  return parsedToken.groups;
}

function hasAnyRole(actualRoles: string[], allowedRoles: string[]) {
  return actualRoles.some((group) => allowedRoles.includes(group));
}
