import { DefaultBodyType } from "msw";
import { beforeEach } from "vitest";
import { Queries, QueryableList } from "src/tests/utils/queryable-list";
import { createHandler, RestHandlerConfig } from "src/tests/openapi-msw";
import { mySetupServer } from "src/tests/openapi-msw/node";
import { logInfo } from "src/utils/logger";
import { createTestEntity } from "src/tests/utils/createTestEntity";
import { SetupServerApi } from "msw/node";

interface SetupMockApiResult {
  useHandlers(...requestHandlers: RestHandlerConfig[]): void;
}

interface CapturedRequest {
  method: string;
  pathname: string;
  headers: Record<string, string>;
  body?: DefaultBodyType;
}

const capturedRequests = new QueryableList<CapturedRequest>();
export const requests: Queries<CapturedRequest> = capturedRequests;

let requestLogEnabled = false;
export function setMockBackendLogEnabled(logEnabled: boolean) {
  requestLogEnabled = logEnabled;
}

export function setupTestMockApi(
  ...handlerConfigs: RestHandlerConfig[]
): SetupMockApiResult {
  const server = createTestEntity({
    create() {
      const server = mySetupServer(...handlerConfigs);
      server.listen({ onUnhandledRequest: "error" });
      setupRequestLogging(server);
      captureRequests(server);
      return server;
    },
    destroy(server) {
      server.close();
    },
  });
  beforeEach(() => {
    capturedRequests.clear();
  });

  return {
    useHandlers(...requestHandlers) {
      server.current.use(...requestHandlers.map(createHandler));
    },
  };
}

function setupRequestLogging(server: SetupServerApi) {
  if (requestLogEnabled) {
    server.events.on("request:start", (request) => {
      logInfo("Request started", request);
    });
    server.events.on("response:mocked", (response, requestId) => {
      logInfo("Response mocked for " + requestId, response);
    });
  }
}

function captureRequests(server: SetupServerApi) {
  server.events.on("request:end", (request) => {
    capturedRequests.add({
      method: request.method,
      pathname: request.url.pathname,
      headers: request.headers.all(),
      body: request.body,
    });
  });
}
