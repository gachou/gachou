import "web-streams-polyfill";
import { ReadableStream, UnderlyingSource } from "web-streams-polyfill";
import { toBeInitialized } from "src/utils/toBeInitialized";

const BLOB_STREAM_CHUNK_SIZE = 5;
Blob.prototype.stream = function stream() {
  return new ReadableStream(new BlobSource(this));
};

class BlobSource implements UnderlyingSource {
  private offset = 0;
  private contents: Uint8Array = toBeInitialized("BlobStream#contents");
  private controller: ReadableStreamDefaultController = toBeInitialized(
    "BlobStream#controller"
  );
  private readonly blob: Blob;

  constructor(blob: Blob) {
    this.blob = blob;
  }

  async start(controller: ReadableStreamDefaultController): Promise<void> {
    const buffer = await new Response(this.blob).arrayBuffer();
    this.contents = new Uint8Array(buffer);
    this.controller = controller;
  }

  async pull(): Promise<void> {
    this.enqueueCurrentChunk();
    this.switchToNextChunk();
    this.closeIfDone();
  }

  private enqueueCurrentChunk() {
    const from = this.offset;
    const to = this.offset + BLOB_STREAM_CHUNK_SIZE;
    const chunk = this.contents.slice(from, to);
    this.controller.enqueue(chunk);
  }

  private switchToNextChunk() {
    this.offset += BLOB_STREAM_CHUNK_SIZE;
  }

  private closeIfDone() {
    if (this.offset > this.contents.byteLength) {
      this.controller.close();
    }
  }
}
