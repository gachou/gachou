import { test, expect } from "vitest";
import { setupServer } from "msw/node";
import { rest } from "msw";

/**
 * Node 18 implements its own "fetch" api, which seems to be incompatible with mock-service-worker
 * It also implements its own "FileReader" appearently, because if you run this test in Node 18,
 * with NODE_OPTIONS="--no-experimental-fetch", it still fails with some errors concerning the FileReader
 */
test("fetch should return the mocked response", async () => {
  const server = setupServer(
    rest.get("/test", (req, res, context) => {
      return res(context.json({ success: true }));
    })
  );

  server.listen();
  const response = await fetch("/test");
  const bodyJson = await response.json();
  expect(bodyJson).toEqual({ success: true });
});
