import { proxy, snapshot, subscribe, useSnapshot } from "valtio";
import { act, renderHook } from "@testing-library/react";

interface TestValue {
  obj: { value: number };
}

describe("valtio", () => {
  it("calls subscribers on change", async () => {
    const store = proxy<TestValue>({ obj: { value: 1 } });
    const subscriber = vi.fn();
    subscribe(store.obj, subscriber);
    store.obj.value = 2;
    await Promise.resolve();
    expect(subscriber).toHaveBeenCalledWith([["set", ["value"], 2, 1]]);
  });

  it("calls subscribers on change only if really changed", async () => {
    const store = proxy<TestValue>({ obj: { value: 1 } });
    const subscriber = vi.fn();
    subscribe(store.obj, subscriber);
    store.obj.value = 1;
    await Promise.resolve();
    expect(subscriber).not.toHaveBeenCalled();
  });

  it("calls subscribers with new, deeply-equal objects", async () => {
    const store = proxy<TestValue>({ obj: { value: 1 } });
    const subscriber = vi.fn();
    subscribe(store, subscriber);
    store.obj = { value: 1 };
    await Promise.resolve();
    expect(subscriber).toHaveBeenCalledWith([
      ["set", ["obj"], { value: 1 }, { value: 1 }],
    ]);
  });

  it("set a snapshot as value and try to overwrite parts of it", async () => {
    const store: TestValue = proxy<TestValue>({
      obj: { value: 1 },
    });
    const snap = snapshot(store);
    store.obj = snap.obj;
    const subscriber = vi.fn();
    subscribe(store, subscriber);
    store.obj.value = 2;
    await Promise.resolve();
    expect(subscriber).toHaveBeenCalledWith([["set", ["obj", "value"], 2, 1]]);
  });

  it("subscribers are not called it the exact same object is assigned", async () => {
    const store: TestValue = proxy<TestValue>({
      obj: { value: 1 },
    });
    const subscriber = vi.fn();
    subscribe(store, subscriber);
    store.obj = null ?? store.obj;
    await Promise.resolve();
    expect(subscriber).not.toHaveBeenCalled();
  });

  it("observes nested properties of a valtio proxy assigned to the store", async () => {
    const store: TestValue = proxy<TestValue>({ obj: { value: 1 } });
    const { result } = renderHook(useSnapshot, { initialProps: store });

    expect(result.current.obj.value).toBe(1);

    const newObject = proxy({ value: 2 });
    await act(() => {
      store.obj = newObject;
    });
    expect(result.current.obj.value).toBe(2);

    await act(() => {
      newObject.value = 3;
    });
    expect(result.current.obj.value).toBe(3);
  });
});
