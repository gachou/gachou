export function waitMillis(millis: number): Promise<void> {
  return new Promise<void>((resolve) => setTimeout(resolve, millis));
}
