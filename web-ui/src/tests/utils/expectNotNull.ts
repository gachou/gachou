import { waitFor } from "@testing-library/react";

export function expectNotNull<T>(
  value: T | null | undefined
): asserts value is T {
  expect(value).not.toBeNull();
  expect(value).not.toBeUndefined();
}

export async function waitForNotNull<T>(
  provider: () => T | null | undefined
): Promise<T> {
  return await waitFor(() => {
    const result = provider();
    expectNotNull(result);
    return result;
  });
}
