import { waitMillis } from "src/tests/utils/waitMillis";
import { logDebug } from "src/utils/logger";
import { Deferred } from "src/utils/Deferred";

/**
 * For use in tests where we need to simulate some kind of progress, but want to have control
 * over when to run the actual steps.
 *
 */
export class ControlledProgress {
  readonly name: string;
  readonly steps: ProgressStep[];

  constructor(name: string, steps: number[]) {
    this.name = name;
    this.steps = steps.map((percent) => new ProgressStep(name, percent));
  }

  async *run(): AsyncGenerator<number> {
    for (const step of this.steps) {
      const percent = await step.wait();
      yield percent;
    }
  }

  async *control(): AsyncGenerator<void> {
    for (const step of this.steps) {
      step.resolve();
      await waitMillis(10);
      yield;
    }
  }

  async finish(): Promise<void> {
    await Promise.all(this.steps.map((step) => step.resolve()));
  }
}

class ProgressStep {
  private readonly deferred: Deferred<number>;
  private readonly name: string;
  readonly percent: number;

  constructor(name: string, percent: number) {
    this.name = name;
    this.percent = percent;
    this.deferred = new Deferred<number>();
  }

  async wait(): Promise<number> {
    logDebug("wait ", this.name);
    try {
      return await this.deferred.promise;
    } finally {
      logDebug("wait done", this.name);
    }
  }

  resolve() {
    logDebug("resolve ", this.name);
    this.deferred.resolve(this.percent);
  }
}
