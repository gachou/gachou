import { proxy, snapshot, useSnapshot } from "valtio";

const testLocation = proxy({ current: "/" });

export function useTestLocation(): [string, (value: string) => void] {
  const currentValue = useSnapshot(testLocation).current;
  const setCurrentValue = (newPath: string) => {
    return (testLocation.current = newPath);
  };
  return [currentValue, setCurrentValue];
}

export function testNavigateTo(path: string) {
  testLocation.current = path;
}

export function currentTestLocation(): string {
  return snapshot(testLocation).current;
}
