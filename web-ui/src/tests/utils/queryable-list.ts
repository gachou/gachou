import { expect } from "vitest";
import { waitFor } from "@testing-library/react";

// Testing-library-style queries for arbitrary lists of objects
// see https://testing-library.com/docs/queries/about for semantics

export interface Queries<T> {
  queryAll(needle: Partial<T>): T[];
  getAll(needle: Partial<T>): T[];
  findAll(needle: Partial<T>): Promise<T[]>;
  query(needle: Partial<T>): T | null;
  get(needle: Partial<T>): T;
  find(needle: Partial<T>): Promise<T>;
}

export class QueryableList<T> implements Queries<T> {
  private items: T[] = [];

  add(item: T) {
    this.items.push(item);
  }

  clear() {
    this.items = [];
  }

  queryAll(needle: Partial<T>): T[] {
    const matcher = expect.objectContaining(needle);
    return this.items.filter((item) => matcher.asymmetricMatch(item));
  }
  getAll(needle: Partial<T>): T[] {
    const result = this.queryAll(needle);
    this.assertNotEmpty(result, needle);
    return result;
  }
  findAll(needle: Partial<T>): Promise<T[]> {
    return waitFor(() => this.getAll(needle));
  }
  query(needle: Partial<T>): T | null {
    const result = this.queryAll(needle);
    this.assertSingle(result);
    if (result.length === 0) {
      return null;
    }
    return result[0];
  }
  get(needle: Partial<T>): T {
    const result = this.queryAll(needle);
    this.assertNotEmpty(result, needle);
    this.assertSingle(result);
    return result[0];
  }
  find(needle: Partial<T>): Promise<T> {
    return waitFor(() => this.get(needle));
  }

  private assertNotEmpty(result: T[], needle: Partial<T>): void {
    if (result.length === 0) {
      throw new Error(
        "Nothing found: " +
          JSON.stringify({ NEEDLE: needle, ALL: this.items }, null, 2)
      );
    }
  }

  private assertSingle(result: T[]): void {
    if (result.length > 1) {
      throw new Error("Multiple found: " + JSON.stringify(result, null, 2));
    }
  }
}
