import {
  RenderResult,
  screen,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import React from "react";
import {
  customRender,
  CustomRenderOptions,
} from "src/tests/utils/custom-render";
import {
  testNavigateTo,
  useTestLocation,
} from "src/tests/utils/useTestLocation";
import { Router } from "wouter";
import { MyRoutes } from "src/routes/MyRoutes";

export async function customRenderPage(
  initialPath: string,
  options?: CustomRenderOptions
): Promise<RenderResult> {
  vitest.resetModules();
  testNavigateTo(initialPath);

  const result = customRender(
    <Router hook={() => useTestLocation()}>
      <MyRoutes />
    </Router>,
    options
  );

  await waitForElementToBeRemoved(() => screen.queryByText("Loading Page"), {
    timeout: 5000,
  });
  return result;
}
