import { logInfo } from "src/utils/logger";

export interface TestRef<T> {
  current: T;
}

interface CreateBeforeEachOpts<T> {
  create: () => Promise<T> | T;
  destroy?: (value: T) => void | Promise<void>;
}

export function createTestEntity<T>({
  create,
  destroy,
}: CreateBeforeEachOpts<T>): TestRef<T> {
  let currentValue: T | null = null;
  let errorInBeforeEach: Error | null = null;

  beforeEach(async () => {
    try {
      currentValue = await create();
      errorInBeforeEach = null;
    } catch (error) {
      errorInBeforeEach = error as Error;
      throw error;
    }
  });

  if (destroy != null) {
    afterEach(async () => {
      if (errorInBeforeEach != null) {
        logInfo(
          "createTestEntity: Ignoring 'destroy'" +
            " method, because of error during 'create'"
        );
        return;
      }
      assertNotNull(currentValue);
      await destroy(currentValue);
      currentValue = null;
    });
  }

  return {
    get current() {
      assertNotNull(currentValue);
      return currentValue;
    },
  };
}

function assertNotNull<T>(currentValue: T | null): asserts currentValue is T {
  if (currentValue == null) {
    throw new Error("createTestEntity: Nothing created or factory not called!");
  }
}
