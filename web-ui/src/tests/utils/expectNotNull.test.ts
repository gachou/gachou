import { expectNotNull } from "./expectNotNull";

describe("assertNotNull", () => {
  it("passes, if the value is not null or undefined", () => {
    expect(() => expectNotNull(1)).not.toThrow();
    expect(() => expectNotNull(0)).not.toThrow();
    expect(() => expectNotNull("")).not.toThrow();
    expect(() => expectNotNull([])).not.toThrow();
    expect(() => expectNotNull({})).not.toThrow();
  });

  it("throws if the value is undefined", () => {
    expect(() => expectNotNull(undefined)).toThrow(
      new Error("expected undefined not to be undefined")
    );
  });

  it("throws if the value is null", () => {
    expect(() => expectNotNull(null)).toThrow(
      new Error("expected null not to be null")
    );
  });
});
