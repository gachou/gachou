import { ControlledProgress } from "src/tests/utils/ControlledProgress";
import { logError } from "src/utils/logger";

vi.mock("src/utils/logger");

describe("ControlledProgress", () => {
  it("is controlled programatically", async () => {
    const progress = new ControlledProgress("test", [0, 2, 4, 5, 6]);
    const control = progress.control();

    const results: number[] = [];
    runProgressAndCollect(progress, results).catch(logError);

    expect(results).toEqual([]);
    await control.next();
    expect(results).toEqual([0]);
    await control.next();
    expect(results).toEqual([0, 2]);
    await control.next();
    expect(results).toEqual([0, 2, 4]);
    await control.next();
    expect(results).toEqual([0, 2, 4, 5]);
    await control.next();
    expect(results).toEqual([0, 2, 4, 5, 6]);

    expect(logError).not.toHaveBeenCalled();
  });
});

async function runProgressAndCollect(
  progress: ControlledProgress,
  results: number[]
) {
  for await (const result of progress.run()) {
    results.push(result);
  }
}
