import { render, RenderOptions, RenderResult } from "@testing-library/react";
import React from "react";

export type CustomRenderOptions = Omit<RenderOptions, "queries">;

export function customRender(
  ui: React.ReactElement,
  options?: CustomRenderOptions
): RenderResult {
  return render(ui, options);
}
