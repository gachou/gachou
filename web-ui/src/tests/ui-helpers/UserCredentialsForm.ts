import userEvent from "@testing-library/user-event";
import { screen } from "@testing-library/react";

export async function enterUserDetails(
  username: string,
  password: string,
  passwordConfirm: string
) {
  await userEvent.type(screen.getByLabelText("Username:"), username);
  await enterPasswords(password, passwordConfirm);
}

export async function enterPasswords(
  password: string,
  passwordConfirm: string
) {
  await userEvent.type(screen.getByLabelText("Password:"), password);
  await userEvent.type(
    screen.getByLabelText("Password confirmation:"),
    passwordConfirm
  );
}
