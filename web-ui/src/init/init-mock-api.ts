import { defaults } from "src/backend/__generated__/openapi-client";
import { setupDevServerMockApi } from "src/tests/mock-backend/setup-dev-server-mock-api";
import { selectUploadAdapter } from "src/adapter/upload";
import { mockApiUploadAdapter } from "src/tests/mock-backend/mock-api-uploader";

export async function initMockApi() {
  await setupDevServerMockApi();
  defaults.baseUrl = "/";
  selectUploadAdapter(mockApiUploadAdapter);
}
