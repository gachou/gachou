// Global window methods that are only used (and available in cypress)
// see https://www.cypress.io/blog/2019/01/03/stop-using-page-objects-and-start-using-app-actions/#application-actions

import { publicStatus } from "src/global-state/publicBackendStatus";
import { logInfo } from "src/utils/logger";

export function registerApplicationActions() {
  logInfo("Running cypress, registering application actions");
  window.updateStatusNow = publicStatus.updateNow;
}
