import { errorToast } from "src/utils/toasts";

export function initErrorToasts() {
  window.addEventListener("unhandledrejection", (event) => {
    errorToast("Unexpected error", String(event.reason));
  });
  window.addEventListener("error", (event) => {
    errorToast("Unexpected error", String(event.error));
  });
}
