import { defaults } from "src/backend/__generated__/openapi-client";
import { apiBaseUrl } from "src/constants/apiBaseUrl";
export function initApi() {
  defaults.baseUrl = apiBaseUrl;
}
