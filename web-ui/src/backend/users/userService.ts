import {
  createUser as apiCreateUser,
  deleteUser,
  findUser,
  listUsers,
  updateUser,
} from "src/backend/__generated__/openapi-client";
import { User, UserRequest } from "src/model/user";
import { handleErrors } from "src/backend/errors";
import { crudService } from "src/backend/tools/crudService";

export const apiUsers = crudService<string, UserRequest, User>({
  create: ({ username, password }) =>
    handleErrors(apiCreateUser({ username, password })),
  update: ({ username, password }) =>
    handleErrors(updateUser(username, { password })),
  list: async () => {
    const response = await handleErrors(listUsers());
    return {
      items: response.users,
    };
  },
  fetch: (username) => handleErrors(findUser(username)),
  remove: (username) => handleErrors(deleteUser(username)),
} as const);
