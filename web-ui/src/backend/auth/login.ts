import { login } from "src/backend/__generated__/openapi-client";
import { handleErrors } from "../errors";

export async function doLogin(
  username: string,
  password: string
): Promise<string> {
  const response = await handleErrors(login({ username, password }));
  return response.token;
}
