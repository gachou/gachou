import { cancelUpload } from "src/backend/__generated__/openapi-client";
import { handleErrors } from "src/backend/errors";

export function apiCancelUpload(uploadId: string) {
  return handleErrors(cancelUpload(uploadId));
}
