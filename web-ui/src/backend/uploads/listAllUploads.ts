import { Upload, Uploads } from "src/model/upload";
import { listAllCurrentUploads as apiListAllUploads } from "src/backend/__generated__/openapi-client";
import { handleErrors } from "src/backend/errors";

export async function listAllUploads(): Promise<Upload[]> {
  const response = await handleErrors(apiListAllUploads());
  return Uploads.check(response.uploads);
}
