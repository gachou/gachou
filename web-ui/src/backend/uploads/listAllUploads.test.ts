import { oas } from "src/tests/openapi-msw";
import { listAllUploads } from "src/backend/uploads/listAllUploads";
import { setupTestMockApi } from "src/tests/mock-backend/setup-test-mock-api";

setupTestMockApi(
  oas.get("/api/uploads", (req, res, context) => {
    return res(
      context.json({
        uploads: [
          {
            id: "upload1",
            fileName: "pexels-nick-mark-mayer-10448623.jpg",
            fileMimeType: "image/jpeg",
            fileLastModified: "2022-09-02T13:13:01.732Z",
            s3Bucket: "uploads",
            s3ObjectKey: "ff3e325e6883cfe7d4b2b4ca5c165303",
            totalSize: 3589243,
            uploadedSize: 3589243,
            state: "RUNNING",
            audit: {
              createdBy: "bob",
              createdAt: "2022-10-02T21:32:38.749634Z",
              updatedBy: "bob",
              updatedAt: "2022-10-02T21:32:39.286755Z",
            },
          },
          {
            id: "upload2",
            fileName: "pexels-maria-loznevaya-13445202.jpg",
            fileMimeType: "image/jpeg",
            fileLastModified: "2022-09-02T13:12:33.432Z",
            s3Bucket: "uploads",
            s3ObjectKey: "5f0588f2423b92b722636abf5314b6a8",
            totalSize: 3805599,
            uploadedSize: 3805599,
            state: "DONE",
            audit: {
              createdBy: "bob",
              createdAt: "2022-10-02T21:32:39.125430Z",
              updatedBy: "bob",
              updatedAt: "2022-10-02T21:32:39.125685Z",
            },
          },
        ],
      })
    );
  })
);

describe("listAllUploads", () => {
  it("validates the returned response", async () => {
    expect(await listAllUploads()).toEqual([
      {
        id: "upload1",
        fileName: "pexels-nick-mark-mayer-10448623.jpg",
        fileMimeType: "image/jpeg",
        fileLastModified: "2022-09-02T13:13:01.732Z",
        totalSize: 3589243,
        uploadedSize: 3589243,
        state: "RUNNING",
        audit: {
          createdBy: "bob",
          createdAt: "2022-10-02T21:32:38.749634Z",
          updatedBy: "bob",
          updatedAt: "2022-10-02T21:32:39.286755Z",
        },
        activeOnClient: false,
      },
      {
        id: "upload2",
        fileName: "pexels-maria-loznevaya-13445202.jpg",
        fileMimeType: "image/jpeg",
        fileLastModified: "2022-09-02T13:12:33.432Z",
        totalSize: 3805599,
        uploadedSize: 3805599,
        state: "DONE",
        audit: {
          createdBy: "bob",
          createdAt: "2022-10-02T21:32:39.125430Z",
          updatedBy: "bob",
          updatedAt: "2022-10-02T21:32:39.125685Z",
        },
        activeOnClient: false,
      },
    ]);
  });
});
