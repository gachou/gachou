import { usersMe } from "src/backend/__generated__/openapi-client";
import { Role } from "src/model/role";
import { handleErrors } from "src/backend/errors";

interface FetchUserInfo {
  username: string;
  roles: Role[];
}

export function fetchUserInfo(): Promise<FetchUserInfo> {
  return handleErrors(usersMe());
}
