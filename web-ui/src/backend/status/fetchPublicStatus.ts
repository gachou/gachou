import { getPublicStatus } from "src/backend/__generated__/openapi-client";
import { handleErrors } from "src/backend/errors";

interface PublicStatus {
  adminUserExists: boolean;
}

export function fetchPublicStatus(): Promise<PublicStatus> {
  return handleErrors(getPublicStatus());
}
