import { CrudService, crudService } from "src/backend/tools/crudService";

interface Request {
  id: string;
  writeOnlyField: number;
}

interface Response {
  id: string;
  readonlyField: number;
}

const createResponse = { id: "create", readonlyField: 1 };
const updateResponse = { id: "update", readonlyField: 2 };
const fetchResponse = { id: "fetch", readonlyField: 3 };
const removeResponse = { id: "remove", readonlyField: 4 };
const listResponse = { items: [{ id: "list", readonlyField: 5 }] };

function methods(): CrudService<string, Request, Response> {
  return {
    create: vi.fn().mockResolvedValue(createResponse),
    update: vi.fn().mockResolvedValue(updateResponse),
    fetch: vi.fn().mockResolvedValue(fetchResponse),
    remove: vi.fn().mockResolvedValue(removeResponse),
    list: vi.fn().mockResolvedValue(listResponse),
  };
}

describe("CrudService", () => {
  it("delegates to the create method", async () => {
    const { create, update, fetch, remove, list } = methods();
    const service = crudService({ create, update, fetch, remove, list });
    const createRequest = { id: "new", writeOnlyField: 1 };

    const result = await service.create(createRequest);

    expect(result).toEqual(createResponse);
    expect(create).toHaveBeenCalledWith(createRequest);
  });

  it("delegates to the update method", async () => {
    const { create, update, fetch, remove, list } = methods();
    const service = crudService({ create, update, fetch, remove, list });
    const updateRequest = { id: "idToUpdate", writeOnlyField: 2 };

    const result = await service.update(updateRequest);

    expect(result).toEqual(updateResponse);
    expect(update).toHaveBeenCalledWith(updateRequest);
  });

  it("delegates to the fetch method", async () => {
    const { create, update, fetch, remove, list } = methods();
    const service = crudService({ create, update, fetch, remove, list });

    const result = await service.fetch("idToFetch");

    expect(result).toEqual(fetchResponse);
    expect(fetch).toHaveBeenCalledWith("idToFetch");
  });

  it("delegates to the remove method", async () => {
    const { create, update, fetch, remove, list } = methods();
    const service = crudService({ create, update, fetch, remove, list });

    const result = await service.remove("idToFetch");

    expect(result).toEqual(removeResponse);
    expect(remove).toHaveBeenCalledWith("idToFetch");
  });

  it("delegates to the list method", async () => {
    const { create, update, fetch, remove, list } = methods();
    const service = crudService({ create, update, fetch, remove, list });

    const result = await service.list();

    expect(result).toEqual(listResponse);
    expect(list).toHaveBeenCalled();
  });
});
