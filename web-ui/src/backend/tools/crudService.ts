import { List } from "src/model/list";

export interface CrudService<Id extends string | number, Request, Response> {
  create(item: Request): Promise<Response>;
  update(item: Request): Promise<Response>;
  list(): Promise<List<Response>>;
  fetch(id: Id): Promise<Response>;
  remove(id: Id): Promise<Response>;
}

export function crudService<Id extends string | number, Request, Response>(
  crudMethods: CrudService<Id, Request, Response>
): CrudService<Id, Request, Response> {
  return crudMethods;
}
