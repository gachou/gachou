/* eslint-disable */
/**
 * Gachou Backend
 * 0.0.1
 * DO NOT MODIFY - This file has been generated using oazapfts.
 * See https://www.npmjs.com/package/oazapfts
 */
import * as Oazapfts from "oazapfts/lib/runtime";
import * as QS from "oazapfts/lib/runtime/query";
export const defaults: Oazapfts.RequestOpts = {
  baseUrl: "/",
};
const oazapfts = Oazapfts.runtime(defaults);
export const servers = {};
export type ConfigPropertyDto = {
  key: string;
  envVar: string;
};
export type ConfigurationSchemaResponse = {
  allowedProperties?: ConfigPropertyDto[];
};
export type PasswordEncryptionRequest = {
  password: string;
};
export type EncryptedPasswordResponse = {
  encryptedPassword: string;
  adminPasswordConfigKey: string;
};
export type LoginRequest = {
  username: string;
  password: string;
};
export type JwtResponse = {
  token: string;
};
export type RolesDto = "ADMIN" | "USER";
export type UserResponse = {
  username: string;
  roles: RolesDto[];
};
export type PublicStatusResponse = {
  adminUserExists: boolean;
};
export type Instant = string;
export type TusdMetadataDto = {
  filename?: string;
  filetype?: string;
  lastModified: Instant;
};
export type AbstractTusdStorageDto = object;
export type TusdUploadDto = {
  ID: string;
  Size: number;
  Offset: number;
  IsFinal: boolean;
  IsPartial: boolean;
  MetaData: TusdMetadataDto;
  Storage?: AbstractTusdStorageDto;
};
export type TusdHttpReqDto = {
  Method: string;
  URI: string;
  RemoteAddr: string;
  Header: {
    [key: string]: string[];
  };
};
export type TusdHookRequest = {
  Upload: TusdUploadDto;
  HTTPRequest: TusdHttpReqDto;
};
export type UploadStateDto = "RUNNING" | "DONE";
export type AuditDto = {
  createdBy: string;
  createdAt: Instant;
  updatedBy: string;
  updatedAt: Instant;
};
export type UploadResponse = {
  id: string;
  fileName: string;
  fileMimeType: string;
  fileLastModified: Instant;
  s3Bucket: string;
  s3ObjectKey: string;
  totalSize: number;
  uploadedSize: number;
  state: UploadStateDto;
  audit: AuditDto;
};
export type UploadListResponse = {
  uploads: UploadResponse[];
};
export type ListUsersResponse = {
  users: UserResponse[];
};
export type CreateUserRequest = {
  username: string;
  password: string;
};
export type UpdateUserRequest = {
  password: string;
};
export type TestUserDto = {
  username: string;
  password: string;
  passwordIsModularCryptFormat?: boolean;
};
export type TestSetupRequest = {
  testUsers: TestUserDto[];
};
/**
 * Example for restricted resource
 */
export function adminExample(opts?: Oazapfts.RequestOpts) {
  return oazapfts.fetchJson<
    | {
        status: 200;
        data: {
          [key: string]: string;
        };
      }
    | {
        status: 401;
      }
    | {
        status: 403;
      }
  >("/api/admin", {
    ...opts,
  });
}
/**
 * Returns the property names of all configuration props
 */
export function getConfigurationSchema(opts?: Oazapfts.RequestOpts) {
  return oazapfts.fetchJson<{
    status: 200;
    data: ConfigurationSchemaResponse;
  }>("/api/config/schema", {
    ...opts,
  });
}
/**
 * Returns the password, encrypted using the Modular Crypt Format
 */
export function encryptPassword(
  passwordEncryptionRequest: PasswordEncryptionRequest,
  opts?: Oazapfts.RequestOpts
) {
  return oazapfts.fetchJson<{
    status: 200;
    data: EncryptedPasswordResponse;
  }>(
    "/api/encryptPassword",
    oazapfts.json({
      ...opts,
      method: "POST",
      body: passwordEncryptionRequest,
    })
  );
}
/**
 * Returns a JWT for username and password
 */
export function login(loginRequest: LoginRequest, opts?: Oazapfts.RequestOpts) {
  return oazapfts.fetchJson<
    | {
        status: 200;
        data: JwtResponse;
      }
    | {
        status: 401;
      }
  >(
    "/api/login",
    oazapfts.json({
      ...opts,
      method: "POST",
      body: loginRequest,
    })
  );
}
/**
 * Returns data about the currently logged in user
 */
export function usersMe(opts?: Oazapfts.RequestOpts) {
  return oazapfts.fetchJson<
    | {
        status: 200;
        data: UserResponse;
      }
    | {
        status: 401;
      }
    | {
        status: 403;
      }
  >("/api/me/user", {
    ...opts,
  });
}
/**
 * Returns publicly available configuration values
 */
export function getPublicStatus(opts?: Oazapfts.RequestOpts) {
  return oazapfts.fetchJson<{
    status: 200;
    data: PublicStatusResponse;
  }>("/api/status/public", {
    ...opts,
  });
}
/**
 * Endpoint for tusd http-hooks
 */
export function tusdHook(
  hookName: string,
  tusdHookRequest: TusdHookRequest,
  opts?: Oazapfts.RequestOpts
) {
  return oazapfts.fetchText(
    "/api/tusd-hook",
    oazapfts.json({
      ...opts,
      method: "POST",
      body: tusdHookRequest,
      headers: {
        ...(opts && opts.headers),
        "Hook-Name": hookName,
      },
    })
  );
}
export function listAllCurrentUploads(opts?: Oazapfts.RequestOpts) {
  return oazapfts.fetchJson<
    | {
        status: 200;
        data: UploadListResponse;
      }
    | {
        status: 401;
      }
    | {
        status: 403;
      }
  >("/api/uploads", {
    ...opts,
  });
}
/**
 * Cancel a pending upload
 */
export function cancelUpload(id: string, opts?: Oazapfts.RequestOpts) {
  return oazapfts.fetchJson<
    | {
        status: 200;
        data: UploadResponse;
      }
    | {
        status: 401;
      }
    | {
        status: 403;
      }
  >(`/api/uploads/${encodeURIComponent(id)}`, {
    ...opts,
    method: "DELETE",
  });
}
/**
 * Returns a list of users and their roles
 */
export function listUsers(opts?: Oazapfts.RequestOpts) {
  return oazapfts.fetchJson<
    | {
        status: 200;
        data: ListUsersResponse;
      }
    | {
        status: 401;
      }
    | {
        status: 403;
      }
  >("/api/users", {
    ...opts,
  });
}
/**
 * Create a new user
 */
export function createUser(
  createUserRequest: CreateUserRequest,
  opts?: Oazapfts.RequestOpts
) {
  return oazapfts.fetchJson<
    | {
        status: 200;
        data: UserResponse;
      }
    | {
        status: 401;
      }
    | {
        status: 403;
      }
  >(
    "/api/users",
    oazapfts.json({
      ...opts,
      method: "POST",
      body: createUserRequest,
    })
  );
}
/**
 * Finds a user by its username
 */
export function findUser(username: string, opts?: Oazapfts.RequestOpts) {
  return oazapfts.fetchJson<
    | {
        status: 200;
        data: UserResponse;
      }
    | {
        status: 401;
      }
    | {
        status: 403;
      }
  >(`/api/users/${encodeURIComponent(username)}`, {
    ...opts,
  });
}
/**
 * Updates a user
 */
export function updateUser(
  username: string,
  updateUserRequest: UpdateUserRequest,
  opts?: Oazapfts.RequestOpts
) {
  return oazapfts.fetchJson<
    | {
        status: 200;
        data: UserResponse;
      }
    | {
        status: 401;
      }
    | {
        status: 403;
      }
  >(
    `/api/users/${encodeURIComponent(username)}`,
    oazapfts.json({
      ...opts,
      method: "PUT",
      body: updateUserRequest,
    })
  );
}
/**
 * Deletes a user
 */
export function deleteUser(username: string, opts?: Oazapfts.RequestOpts) {
  return oazapfts.fetchJson<
    | {
        status: 200;
        data: UserResponse;
      }
    | {
        status: 401;
      }
    | {
        status: 403;
      }
  >(`/api/users/${encodeURIComponent(username)}`, {
    ...opts,
    method: "DELETE",
  });
}
/**
 * Setup test data for integration tests
 */
export function setupTestData(
  testSetupRequest: TestSetupRequest,
  {
    xGachouTestSetupToken,
  }: {
    xGachouTestSetupToken?: string;
  } = {},
  opts?: Oazapfts.RequestOpts
) {
  return oazapfts.fetchText(
    "/devtest/reset-gachou-for-tests",
    oazapfts.json({
      ...opts,
      method: "POST",
      body: testSetupRequest,
      headers: {
        ...(opts && opts.headers),
        "X-Gachou-Test-Setup-Token": xGachouTestSetupToken,
      },
    })
  );
}
