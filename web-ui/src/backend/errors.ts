import { ApiResponse } from "oazapfts/lib/runtime";
import { HttpError, ok } from "oazapfts";
import { auth } from "src/adapter/authentication";

export class ApiError extends Error {
  readonly status: number;

  constructor(status: number, message?: string) {
    super(message != null ? message : `Error: ${status}`);
    this.status = status;
  }
}

export class UnauthorizedError extends ApiError {
  constructor() {
    super(401);
  }
}

export async function handleErrors<T extends ApiResponse>(promise: Promise<T>) {
  try {
    return await ok(promise);
  } catch (error: unknown) {
    if (error instanceof HttpError) {
      throw mapHttpError(error);
    }
    throw error;
  }
}

function mapHttpError(error: HttpError): ApiError {
  if (error.status === 401) {
    auth.logout();
    return new UnauthorizedError();
  }
  return new ApiError(error.status, error.data.message);
}
