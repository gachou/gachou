/* eslint-disable no-restricted-properties */
import "deploy-config.js";
import { Static, VString } from "src/utils/validation";

if (!window.TUS_UPLOAD_URL) {
  alert(`
window.TUS_UPLOAD_URL is not set.
---------------------------------
This means that there is something
wrong with the deploy-config of Gachou.
----------------------------------
Contact your administrator, please.
  `);
  throw new Error("window.TUS_UPLOAD_URL is not set");
}

const TusUploadUrl = VString;
type UrlSchema = Static<typeof TusUploadUrl>;

export const tusUploadUrl: UrlSchema = TusUploadUrl.check(
  window.TUS_UPLOAD_URL
);
