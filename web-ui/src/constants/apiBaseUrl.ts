/* eslint-disable no-restricted-properties */
import "deploy-config.js";

if (!window.API_BASE_URL) {
  alert(`
window.API_BASE_URL is not set.
---------------------------------
This means that there is something
wrong with the deploy-config of Gachou.
----------------------------------
Contact your administrator, please.
  `);
  throw new Error("window.API_BASE_URL is not set");
}

type BaseUrlSchema = "mock-api://" | string;

export const apiBaseUrl: BaseUrlSchema = window.API_BASE_URL;
