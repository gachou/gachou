import React from "react";

import * as Icons from "./index";
import { Story } from "@storybook/react";
import { Button } from "src/utils/bootstrap";
import { ButtonVariant } from "react-bootstrap/types";

export default {
  title: "icons",
};

const variants: ButtonVariant[] = [
  "secondary",
  "outline-secondary",
  "danger",
  "outline-danger",
];

const Template: Story<{ Icon: React.FC }> = ({ Icon }) => (
  <div>
    <div className={"m-1 p-1 border"}>
      <Icon />
    </div>
    <Button className={"m-1"}>
      <Icon />
    </Button>
    <Button className={"m-1"}>
      <Icon /> Button with icon
    </Button>
    <Button className={"m-1"}>No icon</Button>
    <br />
    <Button className={"m-1"} size={"sm"}>
      <Icon />
    </Button>
    <Button className={"m-1"} size={"sm"}>
      <Icon /> Small Button
    </Button>
    <Button className={"m-1"} size={"sm"}>
      No icon
    </Button>
    <br />
    <Button className={"m-1"} size={"lg"}>
      <Icon />
    </Button>
    <Button className={"m-1"} size={"lg"}>
      <Icon /> Large Button
    </Button>
    <Button className={"m-1"} size={"lg"}>
      No icon
    </Button>
    <br />
    {variants.map((variant) => (
      <Button className={"m-1"} variant={variant}>
        <Icon /> {variant}
      </Button>
    ))}
  </div>
);

export const Clipboard = Template.bind({});
Clipboard.args = {
  Icon: Icons.IconClipboard,
};

export const Check = Template.bind({});
Check.args = {
  Icon: Icons.IconCheck,
};

export const Key = Template.bind({});
Key.args = {
  Icon: Icons.IconKey,
};

export const Plus = Template.bind({});
Plus.args = {
  Icon: Icons.IconPlus,
};
