import "./icons.scss";

export { default as IconClipboard } from "bootstrap-icons/icons/clipboard.svg?component";
export { default as IconCheck } from "bootstrap-icons/icons/check.svg?component";
export { default as IconKey } from "bootstrap-icons/icons/key.svg?component";
export { default as IconPlus } from "bootstrap-icons/icons/plus.svg?component";
export { default as IconTrash } from "bootstrap-icons/icons/trash.svg?component";
export { default as IconEmojiFrown } from "bootstrap-icons/icons/emoji-frown.svg?component";
export { default as IconWarning } from "bootstrap-icons/icons/exclamation-diamond-fill.svg?component";
