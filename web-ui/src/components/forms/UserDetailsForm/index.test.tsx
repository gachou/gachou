import { screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import {
  UserCredentialsForm,
  UserDetailsFormProps,
} from "src/components/forms/UserDetailsForm/index";
import { customRender } from "src/tests/utils/custom-render";
import { asyncNoop, noop } from "src/utils/noop";
import { enterUserDetails } from "src/tests/ui-helpers/UserCredentialsForm";

describe("UserCredentialsForm", () => {
  it("shows the input form", async () => {
    render();
    expect(screen.getByLabelText("Username:")).toBeInTheDocument();
    expect(screen.getByLabelText("Password:")).toBeInTheDocument();
    expect(screen.getByLabelText("Password confirmation:")).toBeInTheDocument();
    expect(
      screen.getByText("Create user", { selector: "button" })
    ).toBeInTheDocument();
    expect(
      screen.getByText("Cancel", { selector: "button" })
    ).toBeInTheDocument();
  });

  it("calls the 'onSubmit'-handler when the form is submitted", async () => {
    const onSubmit = vi.fn();
    render({ onSubmit });
    await enterUserDetails("bob", "pw-bob", "pw-bob");
    await submit();
    expect(onSubmit).toHaveBeenCalledWith(
      { username: "bob", password: "pw-bob" },
      expect.any(Function)
    );
  });

  it("shows an error if the passwords do not match", async () => {
    render();
    await enterUserDetails("bob", "pw-bob", "pw-wrong");
    await submit();
    expect(
      screen.getByTestId("user-detail--password-confirm")
    ).toHaveTextContent("Passwords do not match");
  });

  it("shows errors provided by the submit function", async () => {
    render({
      onSubmit: async (credentials, setError) =>
        setError("username", { message: "Some submit error" }),
    });
    await enterUserDetails("bob", "pw-bob", "pw-bob");
    await submit();
    await waitFor(() => {
      expect(screen.getByTestId("user-detail--username")).toHaveTextContent(
        "Some submit error"
      );
    });
  });

  it("calls the 'onCancel' callback when the user clicks 'Cancel'", async () => {
    const onCancel = vi.fn();
    render({ onCancel });
    await userEvent.click(screen.getByText("Cancel", { selector: "button" }));
    await waitFor(() => {
      expect(onCancel).toHaveBeenCalled();
    });
  });
});

function render({
  onSubmit = asyncNoop,
  onCancel = noop,
}: Partial<UserDetailsFormProps> = {}): void {
  customRender(
    <UserCredentialsForm
      onSubmit={onSubmit}
      onCancel={onCancel}
      submitButtonText={"Create user"}
    />
  );
}

async function submit() {
  await userEvent.click(
    screen.getByText("Create user", { selector: "button" })
  );
}
