import React, { ReactNode } from "react";
import { Button, Form } from "src/utils/bootstrap";
import { SubmitHandler, useForm, UseFormSetError } from "react-hook-form";

interface Inputs {
  username: string;
  password: string;
  passwordConfirm: string;
}

export interface UserDetailsFormProps {
  disableUsername?: boolean;
  submitButtonText: ReactNode;
  defaultValues?: Omit<Inputs, "password" | "passwordConfirm">;
  onSubmit: (
    value: Omit<Inputs, "passwordConfirm">,
    setError: UseFormSetError<Inputs>
  ) => Promise<void>;
  onCancel: () => void;
}

export const UserCredentialsForm: React.FC<UserDetailsFormProps> = ({
  disableUsername,
  submitButtonText,
  onSubmit,
  onCancel,
  defaultValues,
}) => {
  const { register, handleSubmit, setError, formState } = useForm<Inputs>({
    defaultValues,
  });

  const submit: SubmitHandler<Inputs> = async ({
    username,
    password,
    passwordConfirm,
  }) => {
    if (passwordConfirm !== password) {
      setError("passwordConfirm", { message: "Passwords do not match" });
      return;
    }
    await onSubmit({ username, password }, setError);
  };
  return (
    <Form onSubmit={handleSubmit(submit)} onReset={onCancel} noValidate>
      <Form.Group className="mb-3" data-testid={"user-detail--username"}>
        <Form.Label htmlFor={"user-detail--username"}>Username:</Form.Label>
        <Form.Control
          id={"user-detail--username"}
          type="text"
          {...register("username")}
          placeholder="Username"
          isInvalid={formState.errors.username != null}
          disabled={disableUsername}
        />
        {formState.errors.username && (
          <Form.Control.Feedback type={"invalid"}>
            {formState.errors.username.message}
          </Form.Control.Feedback>
        )}
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label htmlFor={"user-detail--password"}>Password:</Form.Label>
        <Form.Control
          id={"user-detail--password"}
          type="password"
          {...register("password")}
          placeholder="Password"
        />
      </Form.Group>
      <Form.Group
        className="mb-3"
        data-testid={"user-detail--password-confirm"}
      >
        <Form.Label htmlFor={"user-detail--password-confirm"}>
          Password confirmation:
        </Form.Label>
        <Form.Control
          id={"user-detail--password-confirm"}
          type="password"
          {...register("passwordConfirm")}
          placeholder="Password Confirmation"
          isInvalid={formState.errors.passwordConfirm != null}
        />
        {formState.errors.passwordConfirm && (
          <Form.Control.Feedback type={"invalid"}>
            {formState.errors.passwordConfirm.message}
          </Form.Control.Feedback>
        )}
      </Form.Group>
      <Button className="me-2" variant="primary" type="submit">
        {submitButtonText}
      </Button>
      <Button variant="secondary" type="reset">
        Cancel
      </Button>
    </Form>
  );
};
