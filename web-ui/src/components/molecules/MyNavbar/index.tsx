import React from "react";
import { Container, Nav, Navbar, NavDropdown } from "src/utils/bootstrap";
import { MyLogo } from "src/components/atoms/MyLogo";
import { Link, useLocation } from "wouter";
import { MyNavLink } from "src/components/atoms/MyNavLink";
import css from "./MyNavbar.module.scss";
import { auth } from "src/adapter/authentication";

const UserNavLinks: React.FC = () => {
  const [location] = useLocation();
  return (
    <Nav activeKey={location} className={css.myNavbarNav}>
      <MyNavLink to={"/uploads"} text={"Uploads"} />
    </Nav>
  );
};

const AdminNavLinks: React.FC = () => {
  const [location] = useLocation();
  return (
    <Nav activeKey={location} className={css.myNavbarNav}>
      <MyNavLink to={"/users"} text={"Users"} />
    </Nav>
  );
};

export const GachouNavbar: React.FC<{ className?: string }> = ({
  className,
}) => {
  const { currentUser, logout } = auth.useAuthentication();

  return (
    <Navbar bg="light" expand="lg" className={className}>
      <Container>
        <Navbar.Brand className={css.myNavbarBrand}>
          <Link href={"/"}>
            <a>
              <MyLogo size={"2rem"} className={"me-2"} />
            </a>
          </Link>
        </Navbar.Brand>
        {currentUser?.roles.includes("ADMIN") && <AdminNavLinks />}
        {currentUser?.roles.includes("USER") && <UserNavLinks />}
        <Navbar.Toggle aria-controls="gachou-navbar__collapse" />
        {currentUser != null && (
          <Navbar.Collapse id="gachou-navbar__collapse">
            <Nav className="ms-auto">
              <NavDropdown
                title={"User: " + currentUser.username}
                data-testid={"user-menu"}
              >
                <NavDropdown.ItemText>
                  Roles: {currentUser.roles}
                </NavDropdown.ItemText>
                <NavDropdown.Item onClick={logout}>Logout</NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        )}
      </Container>
    </Navbar>
  );
};
