import {
  Button,
  FormControl,
  InputGroup,
  Overlay,
  Tooltip,
} from "src/utils/bootstrap";
import React, { useRef, useState } from "react";
import css from "./index.module.scss";
import { IconCheck, IconClipboard } from "src/components/icons";

export const CopyToClipboard: React.FC<{
  className?: string;
  text: string;
  title: string;
}> = ({ text, className, title }) => {
  const [done, setDone] = useState(false);
  const button = useRef<HTMLButtonElement>(null);

  async function copyToClipboard() {
    await navigator.clipboard.writeText(text);
    setDone(true);
    setTimeout(() => setDone(false), 10000);
  }

  return (
    <div className={className + " " + css.clipboardText}>
      <InputGroup onClick={copyToClipboard} title={title}>
        <FormControl value={text} readOnly />
        <Button variant={done ? "success" : "secondary"} ref={button}>
          {done ? <IconCheck /> : <IconClipboard />}
        </Button>
        <Overlay target={button} show={done} placement={"right"}>
          {(props) => (
            <Tooltip id={"copy-success"} {...props}>
              Copied to clipboard
            </Tooltip>
          )}
        </Overlay>
      </InputGroup>
    </div>
  );
};
