import React from "react";

import { CopyToClipboard } from "./index";
import { ComponentStory } from "@storybook/react";

export default {
  component: CopyToClipboard,
  layout: "padded",
};

const Template: ComponentStory<typeof CopyToClipboard> = (args) => (
  <div style={{ marginRight: "20rem" }}>
    <CopyToClipboard {...args} />
  </div>
);

export const ShortText = Template.bind({});
ShortText.args = {
  text: "lorem ipsum solor sit",
};
