import { ComponentStory } from "@storybook/react";
import React from "react";
import { MyFileButton } from "src/components/atoms/MyFileButton";
import { action } from "@storybook/addon-actions";

export default {
  component: MyFileButton,
  layout: "padded",
  decorators: [
    (Story: React.FC) => (
      <div style={{ margin: "3em" }}>
        <Story />
      </div>
    ),
  ],
};

const Template: ComponentStory<typeof MyFileButton> = (args) => {
  return <MyFileButton {...args}>Choose file</MyFileButton>;
};

export const Default = Template.bind({});
Default.args = {
  onChange: action("change"),
};
