import React, { ChangeEvent, ReactNode, useRef } from "react";
import { generateId } from "src/utils/generateId";

export interface MyFileButtonProps {
  children?: ReactNode;
  multiple?: boolean;
  onChange?: (files: FileList) => void;
}

export const MyFileButton: React.FC<MyFileButtonProps> = ({
  children,
  multiple = false,
  onChange,
}) => {
  const htmlId = useRef("filebutton-" + generateId());

  function handleFileSelect(event: ChangeEvent<HTMLInputElement>) {
    if (onChange) {
      onChange(event.target.files ?? new FileList());
    }
  }

  return (
    <label htmlFor={htmlId.current} className={"btn btn-primary"}>
      {children}
      <input
        multiple={multiple}
        id={htmlId.current}
        style={{ display: "none" }}
        type={"file"}
        onInput={handleFileSelect}
      />
    </label>
  );
};
