import { RoutePattern } from "src/routes/routes";
import React from "react";
import { Nav } from "src/utils/bootstrap";
import css from "./MyNavLink.module.scss";
import { Link } from "wouter";

interface MyNavLinkProps {
  to: RoutePattern;
  text: string;
}

export const MyNavLink: React.FC<MyNavLinkProps> = ({ to, text }) => {
  return (
    <Nav.Link as={Link} className={css.myNavLink} href={to}>
      {text}
    </Nav.Link>
  );
};
