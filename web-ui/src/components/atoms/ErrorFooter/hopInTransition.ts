import { TransitionSpec } from "src/hooks/useWebTransition";

export const hopInTransition = new TransitionSpec(
  (el) =>
    new Animation(
      new KeyframeEffect(
        el,
        [
          {
            transform: "translateY(100%) rotate(0) scaleY(1)",
          },
          {
            transform: "translateY(-100%) rotate(0) scaleY(1)",
          },
          {
            transform: "translateY(0) rotate(2deg) scaleY(0.7)",
          },
          {
            transform: "translateY(0) rotate(0) scaleY(1)",
          },
        ],
        { duration: 300, easing: "ease-in-out" }
      )
    ),
  (el) =>
    new Animation(
      new KeyframeEffect(
        el,
        [
          {
            transform: "translateY(0) rotate(0) scaleY(1)",
            easing: "ease-out",
          },
          {
            transform: "translateY(0) rotate(0) scaleY(1.5)",
            easing: "ease-out",
          },
          {
            transform: "translateY(-100%) rotate(0) scaleY(0.7)",
            easing: "ease-out",
          },
          {
            transform: "translateY(0) rotate(2deg) scaleY(1)",
          },
          {
            transform: "translateY(100%) rotate(0) scaleY(1)",
          },
        ],
        { duration: 300, easing: "ease-in-out" }
      )
    )
);
