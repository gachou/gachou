import React from "react";
import { ComponentStory } from "@storybook/react";
import { ErrorFooter } from "src/components/atoms/ErrorFooter/ErrorFooter";

export default {
  component: ErrorFooter,
  layout: "padded",
};

const Template: ComponentStory<typeof ErrorFooter> = (args) => {
  return <ErrorFooter {...args} />;
};

export const Default = Template.bind({});
Default.args = {
  text: "Some error text",
  show: true,
  spin: false,
};

export const LongText = Template.bind({});
LongText.args = {
  text: "Some very long error text that certainly has multiple lines on mobile.",
  show: true,
  spin: false,
};

export const LongTextSpinner = Template.bind({});
LongTextSpinner.args = {
  text: "Some very long error text that certainly has multiple lines on mobile.",
  show: true,
  spin: true,
};
