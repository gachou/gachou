import React, { ReactNode, useRef } from "react";
import css from "./ErrorFooter.module.scss";
import { Spinner } from "src/utils/bootstrap";
import { useWebTransition } from "src/hooks/useWebTransition";
import { hopInTransition } from "./hopInTransition";

export const ErrorFooter: React.FC<{
  text: ReactNode;
  show: boolean;
  spin: boolean;
}> = ({ text, show, spin }) => {
  const ref = useRef<HTMLDivElement | null>(null);

  const { visible } = useWebTransition({ show, spec: hopInTransition, ref });
  if (!visible) {
    return null;
  }
  return (
    <div ref={ref} className={css.errorFooter} data-testid={"error-footer"}>
      <div className={css.errorText}>{text}</div>
      {spin && (
        <div>
          <Spinner animation={"border"} className={css.errorSpinner} />
        </div>
      )}
    </div>
  );
};
