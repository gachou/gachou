import React from "react";

export const MyLogo: React.FC<{
  size: string;
  scale?: number;
  withText?: boolean;
  className?: string;
}> = ({ size, scale = 1, className }) => {
  return (
    <svg
      data-testid={"gachou-logo"}
      width={size}
      height={size}
      viewBox="0 0 16.9 16.9"
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      style={{
        transform: `scale(${scale})`,
        transformOrigin: "50% 50%",
      }}
    >
      <use
        xlinkHref="#a"
        transform="rotate(-30 8.5 8.5)"
        width="100%"
        height="100%"
      />
      <path
        id="a"
        fill="#ececec"
        stroke="currentColor"
        d="M2.5 2.5h12v12h-12Z"
        strokeWidth=".4"
      />
      <path
        d="M9.5 4.8a2 2 0 0 0-.6 0 2 2 0 0 0-.9.6c-.1.2-.3.5-.3.8v.7l.4.7.4.4.1.2-.3.2h-.6a36.4 36.4 0 0 1-.4-.2 2.7 2.7 0 0 1-.8-.3L6 7.4l-.4-.3c-.1 0-.3-.1-.4.1a4 4 0 0 0-.6 1.1l-.2 1.2c0 .4 0 .9.2 1.3s.7.8 1 1c.6.3.8.3 1.5.4h2.6l1.6-.1a2.9 2.9 0 0 0 1-1c.3-.3.4-.7.4-1 0-.4-.2-.8-.4-1.1l-.7-.6-.3-.6c0-.2 0-.5.3-.8l.2.1h.6v-.3a1.2 1.2 0 0 0-.2-.6c.3-.4.4-.7.3-.9l-1 .5a1 1 0 0 0-.1-.2l-.4-.4a3.5 3.5 0 0 0-1-.4 2.4 2.4 0 0 0-.4 0Z"
        fill="currentcolor"
      />
    </svg>
  );
};
