import React, { ReactNode } from "react";
import { Modal } from "src/utils/bootstrap";

export interface ModalProps {
  show: boolean;
  title: string;
  children: ReactNode;
}

export const GModal: React.FC<ModalProps> = ({ title, children, show }) => {
  return (
    <Modal show={show} title={title} data-testid={"modal"}>
      <Modal.Header>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>

      <Modal.Body>{children}</Modal.Body>
    </Modal>
  );
};
