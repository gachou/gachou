import { ShowApiValue } from "src/components/atoms/ShowApiValue/ShowApiValue";
import { ComponentStory } from "@storybook/react";
import React from "react";
import { action } from "@storybook/addon-actions";

export default {
  component: ShowApiValue,
  layout: "padded",
};

const Template: ComponentStory<typeof ShowApiValue<string>> = (args) => (
  <div>
    Text before
    <ShowApiValue {...args}>{(value) => <b>{value}</b>}</ShowApiValue>
    Text after
  </div>
);

export const ShowError = Template.bind({});
ShowError.args = {
  apiValue: {
    value: null,
    error: new Error("User 'bob' could not be loaded"),
    reload: promiseAction("reload clicked"),
  },
};

export const Loading = Template.bind({});
Loading.args = {
  apiValue: {
    value: null,
    error: null,
    reload: promiseAction("reload clicked"),
  },
};

function promiseAction(msg: string): (...args: never[]) => Promise<void> {
  return (...args) => {
    action(msg)(...args);
    return Promise.resolve();
  };
}
