import React, { ReactNode } from "react";
import { ApiValue } from "src/hooks/useApi";
import { IconEmojiFrown } from "src/components/icons";
import { Button, Card, Spinner } from "src/utils/bootstrap";
import css from "./ShowApiValue.module.scss";

type Options<V> = {
  apiValue: ApiValue<V>;
  children: (result: V, reload: () => void) => ReactNode;
  className?: string;
};

export function ShowApiValue<V>({
  apiValue,
  children,
  className,
}: Options<V>): JSX.Element {
  if (apiValue.error != null) {
    return (
      <Card className={className}>
        <Card.Body>
          <Card.Title>
            <IconEmojiFrown /> I am so sorry
          </Card.Title>
          <Card.Text>{apiValue.error.message}</Card.Text>
          <Button variant={"info"} onClick={() => apiValue.reload}>
            Retry
          </Button>
        </Card.Body>
      </Card>
    );
  }
  if (apiValue.value != null) {
    return <div>{children(apiValue.value, apiValue.reload)}</div>;
  }
  return (
    <div className={css.loading}>
      <Spinner animation={"border"} variant={"secondary"} /> Loading...
    </div>
  );
}
