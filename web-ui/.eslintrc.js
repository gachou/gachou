module.exports = {
  root: true,
  parserOptions: {
    sourceType: "script",
    ecmaVersion: 2021,
  },
  env: {
    node: true,
  },
  extends: ["eslint:recommended", "prettier"],
  rules: {
    eqeqeq: ["error", "always", { null: "ignore" }],
    "no-console": "error",
  },
  overrides: [
    {
      files: ["src/**/*.+(ts|tsx)", ".storybook/preview.ts"],
      ...require("./eslint-configs/source-files"),
    },
    {
      files: "src/**/*.test.+(ts|tsx)",
      ...require("./eslint-configs/source-files-unit-test"),
    },
    {
      files: "src/**/*.stories.tsx",
      ...require("./eslint-configs/source-files-stories"),
    },
    {
      files: ["scripts/**/*.mjs"],
      ...require("./eslint-configs/scripts"),
    },
    {
      // Configuration files using NodeJS + TypeScript
      files: ["vite.config.ts", "vite-plugins/*.ts"],
      ...require("./eslint-configs/typescript-node"),
    },
  ],
};
