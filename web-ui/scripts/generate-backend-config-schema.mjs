import axios from "axios";
import fs from "fs";
import { resolve } from "./utils/resolve.mjs";
import prettier from "prettier";
import { failWithError } from "./utils/fail-with-error.mjs";

const outputFile = resolve("src/config/__generated__/backend-config-schema.ts");

async function generateBackendConfigSchema() {
  const response = await axios.get("http://localhost:8080/api/config/schema");
  const validKeys = [];
  const validEnvVars = [];
  for (const { key, envVar } of response.data.allowedProperties) {
    validKeys.push(key);
    validEnvVars.push(envVar);
  }
  const typeScriptCode = `
    export type ValidKeys = ${toLiteralUnionType(validKeys)}
    export type ValidEnvVars = ${toLiteralUnionType(validEnvVars)}
    `;
  const prettyCode = prettier.format(typeScriptCode, { parser: "typescript" });

  fs.writeFileSync(outputFile, prettyCode);
  console.log("Backend configuration schema written to " + outputFile);
}

function toLiteralUnionType(validKeys) {
  return validKeys.map(JSON.stringify).join(" | ");
}

generateBackendConfigSchema().catch(failWithError);
