/* eslint-env node */

import axios from "axios";
import { writeSpec } from "./utils/write-spec.mjs";
import { writeMockServiceWorkerTypes } from "./utils/write-mock-service-worker-types.mjs";
import { resolve } from "./utils/resolve.mjs";
import { failWithError } from "./utils/fail-with-error.mjs";
import { writeOpenapiClient } from "./utils/write-openapi-client.mjs";

const specFile = resolve("src/backend/__generated__/openapi.json");
const mockServiceWorkerTypesFile = resolve(
  "src/tests/openapi-msw/__generated__/openapi-msw-types.ts"
);
const openapiClientFile = resolve(
  "src/backend/__generated__/openapi-client.ts"
);

async function buildFromOpenAPI() {
  const spec = await downloadSpec();
  await writeSpec(spec, specFile);
  await writeMockServiceWorkerTypes(spec, mockServiceWorkerTypesFile);
  await writeOpenapiClient(specFile, openapiClientFile);
}

async function downloadSpec() {
  const response = await axios.get("http://localhost:8080/q/openapi");
  return response.data;
}

buildFromOpenAPI().catch(failWithError);
