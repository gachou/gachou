// https://github.com/cosee/techtalk-typescript-2021-02/blob/master/scripts/utils/prepare-spec.js
import fs from "fs";
import prettier from "prettier";

export async function writeSpec(spec, specFile) {
  const jsonData = JSON.stringify(spec, removeExtensionProps);
  const prettySpec = prettier.format(jsonData, { parser: "json" });
  fs.writeFileSync(specFile, prettySpec);
}

const removeExtensionProps = (key, value) =>
  key.match(/^x-/) ? undefined : value;
