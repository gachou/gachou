import cp from "child_process";
import prettier from "prettier";
import fs from "fs";

export async function writeOpenapiClient(specFile, typesFile) {
  const result = cp.execFileSync("oazapfts", [specFile]);
  const dtsContents = "/* eslint-disable */\n" + result;
  const formattedDts = prettier.format(dtsContents, { parser: "typescript" });
  fs.writeFileSync(typesFile, formattedDts);
  console.log("OpenAPI-spec generated!");
}
