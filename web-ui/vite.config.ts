/// <reference types="vitest" />

import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import path from "path";
import { copyMockServiceWorker } from "./vite-plugins/copyMockServiceWorker";
import { injectDeployConfig } from "./vite-plugins/injectDeployConfig";
import { injectVersion } from "./vite-plugins/injectVersion";
import svgr from "@honkhonk/vite-plugin-svgr";
import { visualizer } from "rollup-plugin-visualizer";

const outDir = path.resolve(__dirname, "dist");
const srcDir = path.resolve(__dirname, "src");

export default defineConfig({
  plugins: [
    react(),
    svgr(),
    injectDeployConfig(process.env.DEPLOY_PROFILE),
    copyMockServiceWorker(),
    injectVersion(),
    visualizer(),
  ],
  resolve: {
    alias: {
      src: srcDir,
    },
  },
  root: "src",
  build: {
    outDir,
  },
  server: {
    port: 3000,
  },
  test: {
    globals: true,
    environment: "jsdom",
    setupFiles: [path.resolve(srcDir, "setupTests.ts")],
  },
});
