package org.knappi.gachou.rest_api.endpoints.uploads.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.rest_api.endpoints.tusdhook.dto.TusdFileStorageDto;
import org.knappi.gachou.rest_api.endpoints.tusdhook.dto.TusdHookRequest;
import org.knappi.gachou.rest_api.endpoints.tusdhook.dto.TusdS3StorageDto;

import javax.inject.Inject;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class TusdHookRequestTest {

    @Inject
    ObjectMapper objectMapper;

    @Test
    void deserialize_withS3Storage() throws Exception {
        TusdHookRequest tusdHookRequest = objectMapper.readValue(
                TusdHookRequest.class.getResource("/fixtures/tusdtest/hookWithS3Storage.json"),
                TusdHookRequest.class
        );
        assertThat(tusdHookRequest.getUpload().getStorage()).isInstanceOf(TusdS3StorageDto.class);
    }

    @Test
    void deserializees_lastModifiedDate() throws Exception {
        TusdHookRequest tusdHookRequest = objectMapper.readValue(
                TusdHookRequest.class.getResource("/fixtures/tusdtest/hookWithS3Storage.json"),
                TusdHookRequest.class
        );

        assertThat(tusdHookRequest
                .getUpload()
                .getMetadata()
                .getLastModified()).isEqualTo(Instant.parse("2022-09-03T16:46:00Z"));
    }

    @Test
    void deserialize_withFileStorage() throws Exception {
        TusdHookRequest tusdHookRequest = objectMapper.readValue(
                TusdHookRequest.class.getResource("/fixtures/tusdtest/hookWithFileStorage.json"),
                TusdHookRequest.class
        );
        assertThat(tusdHookRequest.getUpload().getStorage()).isInstanceOf(TusdFileStorageDto.class);

    }
}