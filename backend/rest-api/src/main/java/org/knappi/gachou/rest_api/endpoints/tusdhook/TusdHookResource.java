package org.knappi.gachou.rest_api.endpoints.tusdhook;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.knappi.gachou.core.model.authentication.Role;
import org.knappi.gachou.core.model.upload.Upload;
import org.knappi.gachou.core.model.upload.UploadState;
import org.knappi.gachou.core.usecases.upload.UploadService;
import org.knappi.gachou.rest_api.endpoints.tusdhook.dto.TusdHookRequest;
import org.knappi.gachou.rest_api.endpoints.tusdhook.dto.TusdS3StorageDto;
import org.knappi.gachou.rest_api.endpoints.tusdhook.dto.TusdUploadDto;
import org.knappi.gachou.rest_api.endpoints.tusdhook.mapper.TusdHookRequestMapper;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;
import java.util.Optional;

@Path("/api/tusd-hook")
@RolesAllowed(Role.Constants.USER)
@Slf4j
public class TusdHookResource {

    @Inject
    TusdHookRequestMapper tusdMapper;

    @Inject
    UploadService uploadService;

    @Operation(operationId = "tusdHook",
               description = "Endpoint for tusd http-hooks")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RequestBody(required = true)
    public Response tusdHook(@Valid TusdHookRequest tusdHookRequest,
                             @NotNull @HeaderParam("Hook-Name") String hookName) {

        log.info("tusd '" + hookName + "'-hook called: " + tusdHookRequest.toString());
        switch (hookName) {
            case "post-create":
                log.info("Creating upload: " + tusdHookRequest.getUpload().getId());
                createUpload(tusdHookRequest);
                return Response.accepted().build();
            case "post-receive":
                log.info("Progress for " +
                        tusdHookRequest.getUpload().getId() +
                        " is " +
                        tusdHookRequest.getUpload().getOffset());
                updateUpload(tusdHookRequest, UploadState.RUNNING);
                return Response.accepted().build();
            case "post-finish":
                log.info("Finished: " + tusdHookRequest.getUpload().getId());
                updateUpload(tusdHookRequest, UploadState.DONE);
                return Response.accepted().build();
            case "post-terminate":
                log.info("Canceled: " + tusdHookRequest.getUpload().getId());
                uploadService.cancelUpload(tusdHookRequest.getUpload().getId());
                return Response.accepted().build();
            case "pre-create":
            case "pre-finish":
                return Response.accepted().build();
            default:
                return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    private void updateUpload(TusdHookRequest tusdHookRequest, UploadState state) {
        TusdUploadDto upload = tusdHookRequest.getUpload();
        TusdS3StorageDto storage = getS3Storage(tusdHookRequest);
        uploadService.updateUploadProgress(tusdMapper.toCore(upload, storage, state));
    }

    private void createUpload(TusdHookRequest tusdHookRequest) {
        TusdUploadDto upload = tusdHookRequest.getUpload();
        TusdS3StorageDto storage = getS3Storage(tusdHookRequest);
        uploadService.createUpload(tusdMapper.toCore(upload,
                storage,
                UploadState.RUNNING
        ));
    }

    private static TusdS3StorageDto getS3Storage(TusdHookRequest tusdHookRequest) {
        if (!(tusdHookRequest.getUpload().getStorage() instanceof TusdS3StorageDto)) {
            throw new BadRequestException("Storage needs to be of type 's3'");
        }
        return (TusdS3StorageDto) tusdHookRequest.getUpload().getStorage();
    }
}
