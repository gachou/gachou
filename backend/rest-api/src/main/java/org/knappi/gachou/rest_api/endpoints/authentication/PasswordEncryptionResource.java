package org.knappi.gachou.rest_api.endpoints.authentication;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.knappi.gachou.core.usecases.authentication.PasswordEncryptionService;
import org.knappi.gachou.rest_api.endpoints.authentication.dto.PasswordEncryptionRequest;
import org.knappi.gachou.rest_api.endpoints.authentication.dto.EncryptedPasswordResponse;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Produces("application/json")
@Consumes("application/json")
@Path("/api/encryptPassword")
@PermitAll
public class PasswordEncryptionResource {

    @Inject
    PasswordEncryptionService passwordEncryptionService;

    @Operation(operationId = "encryptPassword",
               description = "Returns the password, encrypted using the Modular Crypt Format")
    @APIResponse(responseCode = "200",
                 description = "On success")
    @RequestBody(required = true)
    @POST
    public EncryptedPasswordResponse encryptPassword(@Valid  PasswordEncryptionRequest body) {
        String password = passwordEncryptionService.encryptPasswordToString(body.getPassword());
        return EncryptedPasswordResponse.builder().encryptedPassword(password).build();
    }
}
