package org.knappi.gachou.rest_api.exceptionmapping;

import org.knappi.gachou.core.model.authentication.UserAlreadyExistsException;
import org.knappi.gachou.rest_api.exceptionmapping.dto.BasicExceptionResponse;

import javax.naming.AuthenticationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class UserAlreadyExistsExceptionMapping implements ExceptionMapper<UserAlreadyExistsException> {

    @Override
    public Response toResponse(UserAlreadyExistsException exception) {
        return ErrorResponses.basicError(Response.Status.CONFLICT, exception.getMessage());
    }
}
