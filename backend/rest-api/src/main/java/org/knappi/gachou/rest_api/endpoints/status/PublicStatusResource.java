package org.knappi.gachou.rest_api.endpoints.status;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.knappi.gachou.core.usecases.authentication.UserService;
import org.knappi.gachou.rest_api.endpoints.status.dto.PublicStatusResponse;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/api/status/public")
@Produces(MediaType.APPLICATION_JSON)
@PermitAll
public class PublicStatusResource {

    @Inject
    UserService userService;

    @GET
    @Operation(operationId = "getPublicStatus",
               description = "Returns publicly available configuration values")
    @APIResponse(responseCode = "200", description = "On success")
    public PublicStatusResponse getPublicConfiguration() {
        return PublicStatusResponse.builder()
                .adminUserExists(userService.doesAdminUserExist())
                .build();
    }
}
