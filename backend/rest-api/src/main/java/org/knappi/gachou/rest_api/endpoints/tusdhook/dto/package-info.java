/**
 * The model of these classes is derived from the documentation at
 * <br>
 * <code>https://github.com/tus/tusd/blob/master/docs/hooks.md</code>
 * <br>
 * and from empirical tests and observation of the format the tusd-hooks
 * are called with.
 */
package org.knappi.gachou.rest_api.endpoints.tusdhook.dto;