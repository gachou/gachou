package org.knappi.gachou.rest_api.endpoints.tusdhook.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class TusdHookRequest {

    @JsonProperty("Upload")
    @NotNull
    private TusdUploadDto upload;

    @JsonProperty("HTTPRequest")
    @NotNull
    private TusdHttpReqDto httpRequest;
}
