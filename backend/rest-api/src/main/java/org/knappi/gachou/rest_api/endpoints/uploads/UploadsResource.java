package org.knappi.gachou.rest_api.endpoints.uploads;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.knappi.gachou.core.model.authentication.Role;
import org.knappi.gachou.core.model.upload.Upload;
import org.knappi.gachou.core.usecases.upload.UploadService;
import org.knappi.gachou.rest_api.endpoints.uploads.dto.UploadListResponse;
import org.knappi.gachou.rest_api.endpoints.uploads.dto.UploadResponse;
import org.knappi.gachou.rest_api.endpoints.uploads.mapper.UploadResponseMapper;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/api/uploads")
@RolesAllowed(Role.Constants.USER)
@Slf4j
public class UploadsResource {

    @Inject
    UploadResponseMapper mapper;

    @Inject
    UploadService uploadService;


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(operationId = "listAllCurrentUploads")
    public UploadListResponse listAllUploadMetadata() {
        return mapper.toDto(uploadService.listAllUploads());
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(operationId = "cancelUpload",
               summary = "Cancel a pending upload",
               description = "Removes stored upload-metadata from the database and uploaded files" +
                       " from the 'upload' s3-bucket")
    @Path("{id}")
    public UploadResponse cancelUpload(@PathParam("id") String uploadId) {
        Upload deletedMetadata = uploadService.cancelUpload(uploadId);
        return mapper.toDto(deletedMetadata);
    }


}
