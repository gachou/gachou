package org.knappi.gachou.rest_api.exceptionmapping.dto;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotNull;

@Builder
@RegisterForReflection
public class BasicExceptionResponse {
    @NotNull
    public final int status;
    @NotNull
    public final String message;
}
