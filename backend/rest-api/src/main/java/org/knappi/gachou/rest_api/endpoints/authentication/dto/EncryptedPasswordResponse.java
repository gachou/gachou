package org.knappi.gachou.rest_api.endpoints.authentication.dto;

import lombok.Builder;
import org.knappi.gachou.core.usecases.authentication.config.AdminUserConfig;
import org.knappi.gachou.core.usecases.config.ConfigHelper;

import javax.validation.constraints.NotNull;

@Builder
public class EncryptedPasswordResponse {
    @NotNull
    public final String encryptedPassword;

    @NotNull
    public final String adminPasswordConfigKey = ConfigHelper.getConfigKey(AdminUserConfig.class,
            "encryptedPassword");
}
