package org.knappi.gachou.rest_api;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.ws.rs.core.Application;

@OpenAPIDefinition(tags = {
        @Tag(name = "authentication",
             description = "Operations related to user accounts and passwords"),
},
                   info = @Info(title = "Gachou Backend",
                                version = "0.0.1"))
public class OpenApiMetadata extends Application {
}
