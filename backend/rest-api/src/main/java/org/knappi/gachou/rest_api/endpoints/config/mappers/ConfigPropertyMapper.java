package org.knappi.gachou.rest_api.endpoints.config.mappers;

import org.knappi.gachou.core.model.config.ConfigProperty;
import org.knappi.gachou.core.model.config.GachouConfiguration;
import org.knappi.gachou.rest_api.endpoints.config.dto.ConfigPropertyDto;
import org.knappi.gachou.rest_api.endpoints.config.dto.ConfigurationSchemaResponse;
import org.knappi.gachou.utils.mapstruct.MapStructConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapStructConfig.class)
public interface ConfigPropertyMapper {
    @Mapping(source = "key",
             target = "key")
    @Mapping(source = "envVar",
             target = "envVar")
    ConfigPropertyDto toDto(ConfigProperty configProperty);

    @Mapping(source = "properties",
             target = "allowedProperties")
    ConfigurationSchemaResponse toResponse(GachouConfiguration gachouConfiguration);
}
