package org.knappi.gachou.rest_api.endpoints.uploads.dto;

import lombok.Builder;

import javax.validation.constraints.NotNull;
import java.util.List;

@Builder
public class UploadListResponse {
    @NotNull
    public List<UploadResponse> uploads;
}
