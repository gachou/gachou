package org.knappi.gachou.rest_api.endpoints.tusdhook.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Data
public class TusdHttpReqDto {
    @NotNull
    @JsonProperty("Method")
    private String method;

    @NotNull
    @JsonProperty("URI")
    private String uri;

    @NotNull
    @JsonProperty("RemoteAddr")
    private String remoteAddress;

    @NotNull
    @JsonProperty("Header")
    private Map<String, List<String>> headers;

}
