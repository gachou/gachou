package org.knappi.gachou.rest_api.exceptionmapping;

import org.knappi.gachou.core.exceptions.AbstractNotFoundException;
import org.knappi.gachou.core.model.upload.UploadNotFoundException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NotFoundExceptionMapping implements ExceptionMapper<AbstractNotFoundException> {

    @Override
    public Response toResponse(AbstractNotFoundException exception) {
        return ErrorResponses.basicError(Response.Status.NOT_FOUND, exception.getMessage());
    }
}
