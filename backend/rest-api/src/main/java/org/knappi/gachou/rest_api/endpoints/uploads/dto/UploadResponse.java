package org.knappi.gachou.rest_api.endpoints.uploads.dto;


import lombok.Builder;

import javax.validation.constraints.NotNull;
import java.time.Instant;

@Builder
public class UploadResponse {

    @NotNull
    public final String id;
    @NotNull
    public final String fileName;
    @NotNull
    public final String fileMimeType;
    @NotNull
    public final Instant fileLastModified;
    @NotNull
    public final String s3Bucket;
    @NotNull
    public final String s3ObjectKey;
    @NotNull
    public final long totalSize;
    @NotNull
    public final long uploadedSize;
    @NotNull
    public final UploadStateDto state;
    @NotNull
    public final AuditDto audit;
}
