package org.knappi.gachou.rest_api.endpoints.users.dto;

public enum RolesDto {
    ADMIN,
    USER
}
