package org.knappi.gachou.rest_api.endpoints.tusdhook.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "Type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = TusdS3StorageDto.class, name="s3store" ),
        @JsonSubTypes.Type(value = TusdFileStorageDto.class, name="filestore")
})
@Data
public abstract class AbstractTusdStorageDto {

}
