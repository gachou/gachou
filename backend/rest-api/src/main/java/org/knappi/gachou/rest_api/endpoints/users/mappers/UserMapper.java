package org.knappi.gachou.rest_api.endpoints.users.mappers;

import org.knappi.gachou.core.model.authentication.User;
import org.knappi.gachou.core.model.authentication.UserList;
import org.knappi.gachou.rest_api.endpoints.users.dto.ListUsersResponse;
import org.knappi.gachou.rest_api.endpoints.users.dto.UserResponse;
import org.knappi.gachou.utils.mapstruct.MapStructConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapStructConfig.class)
public interface UserMapper {

    @Mapping(source = "username",
             target = "username")
    @Mapping(source = "roles",
             target = "roles")
    UserResponse toDto(User user);

    @Mapping(source = "users", target="users")
    ListUsersResponse toDto(UserList users);
}
