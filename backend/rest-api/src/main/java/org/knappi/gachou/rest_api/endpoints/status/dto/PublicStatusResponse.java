package org.knappi.gachou.rest_api.endpoints.status.dto;

import lombok.Builder;

import javax.validation.constraints.NotNull;

@Builder
public class PublicStatusResponse {
    @NotNull
    public final boolean adminUserExists;
}
