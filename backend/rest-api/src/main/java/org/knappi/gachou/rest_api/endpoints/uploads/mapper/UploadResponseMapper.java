package org.knappi.gachou.rest_api.endpoints.uploads.mapper;

import org.knappi.gachou.core.model.upload.Upload;
import org.knappi.gachou.core.model.upload.UploadList;
import org.knappi.gachou.rest_api.endpoints.uploads.dto.UploadListResponse;
import org.knappi.gachou.rest_api.endpoints.uploads.dto.UploadResponse;
import org.knappi.gachou.utils.mapstruct.MapStructConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapStructConfig.class)
public interface UploadResponseMapper {

    @Mapping(source = "uploadId", target = "id")
    @Mapping(source = "fileName",
             target = "fileName")
    @Mapping(source = "fileMimeType",
             target = "fileMimeType")
    @Mapping(source = "fileLastModified",
             target = "fileLastModified")
    @Mapping(source = "s3Bucket",
             target = "s3Bucket")
    @Mapping(source = "s3ObjectKey",
             target = "s3ObjectKey")
    @Mapping(source = "totalSize",
             target = "totalSize")
    @Mapping(source = "uploadedSize",
             target = "uploadedSize")
    @Mapping(source = "state",
             target = "state")
    @Mapping(source = "audit", target = "audit")
    UploadResponse toDto(Upload metadata);

    @Mapping(source="uploads", target = "uploads")
    UploadListResponse toDto(UploadList uploadMetadataList);
}
