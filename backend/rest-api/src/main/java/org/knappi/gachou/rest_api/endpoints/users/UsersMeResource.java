package org.knappi.gachou.rest_api.endpoints.users;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.jboss.resteasy.reactive.NoCache;
import org.knappi.gachou.core.model.authentication.Role;
import org.knappi.gachou.core.usecases.authentication.CurrentUserService;
import org.knappi.gachou.rest_api.endpoints.users.dto.UserResponse;
import org.knappi.gachou.rest_api.endpoints.users.mappers.UserMapper;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/api/me/user")
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed(Role.Constants.ADMIN)
public class UsersMeResource {

    @Inject
    UserMapper userMapper;

    @Inject
    CurrentUserService currentUserService;

    @Operation(operationId = "usersMe",
               description = "Returns data about the currently logged in user")
    @GET
    @PermitAll
    @NoCache
    public UserResponse me() {
        return userMapper.toDto(currentUserService.findCurrentUser());
    }
}