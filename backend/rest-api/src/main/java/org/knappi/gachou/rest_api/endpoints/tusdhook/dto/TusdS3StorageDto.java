package org.knappi.gachou.rest_api.endpoints.tusdhook.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
public class TusdS3StorageDto extends AbstractTusdStorageDto {
    @JsonProperty("Bucket")
    @NotNull
    private String bucket;

    @JsonProperty("Key")
    @NotNull
    private String key;
}
