package org.knappi.gachou.rest_api.endpoints.devtest;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.knappi.gachou.core.usecases.devtest.TestSetupService;
import org.knappi.gachou.core.usecases.devtest.config.DangerousTestSetupConfig;
import org.knappi.gachou.core.usecases.devtest.exception.TestDataSetupForbiddenException;
import org.knappi.gachou.rest_api.endpoints.devtest.mapper.TestSetupMapper;
import org.knappi.gachou.rest_api.endpoints.devtest.model.TestSetupRequest;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * This endpoint can be used to set up test-data for integration tests.
 * <p>
 * This is not possible directly from the test, because the test does not run
 * in a container and the application has been compiled before.
 * <p>
 * The endpoint requires a special header with an authentication token
 * ({@link DangerousTestSetupConfig}, which define via a config option.
 * If the token is not specified in the config, this endpoint is not
 * <p>
 * Because it would be harmful to have this feature on a production environment, as an
 * additional security measure, it requires the correct token value to be passed to the
 * test-setup endpoint in the request-headers.
 */
@Produces(MediaType.TEXT_PLAIN)
@Consumes(MediaType.APPLICATION_JSON)
@Path(TestSetupResource.PATH)
@PermitAll
@Slf4j
public class TestSetupResource {

    public static final String PATH = "/devtest/reset-gachou-for-tests";
    public static final String TOKEN_HEADER_NAME = "X-Gachou-Test-Setup-Token";


    @Inject
    TestSetupService testSetupService;

    @Inject
    TestSetupMapper testSetupMapper;

    @Operation(operationId = "setupTestData",
               description = "Setup test data for integration tests")
    @POST
    @RequestBody(required = true)
    public Response resetGachou(@HeaderParam(TOKEN_HEADER_NAME) String accessToken,
                                @RequestBody TestSetupRequest testSetupRequest) {
        try {
            testSetupService.setupForTest(testSetupMapper.toCore(testSetupRequest), accessToken);
        } catch (TestDataSetupForbiddenException e) {
            log.info("test setup forbidden: " + testSetupRequest.toString());
            return Response.status(403).build();
        }
        log.info("test setup successful: " + testSetupRequest.toString());
        return Response.accepted().build();
    }
}