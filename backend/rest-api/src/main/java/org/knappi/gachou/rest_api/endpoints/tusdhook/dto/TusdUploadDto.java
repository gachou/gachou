package org.knappi.gachou.rest_api.endpoints.tusdhook.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

@Data
@JsonIgnoreProperties(value = {"PartialUploads"})
public class TusdUploadDto {

    @NotNull
    @JsonProperty("ID")
    private String id;

    @NotNull
    @JsonProperty("Size")
    private long size;

    @NotNull
    @JsonProperty("Offset")
    private long offset;

    @NotNull
    @JsonProperty("IsFinal")
    private boolean isFinal;

    @NotNull
    @JsonProperty("IsPartial")
    private boolean isPartial;

    @NotNull
    @JsonProperty("MetaData")
    private TusdMetadataDto metadata;

    @Nullable
    @JsonProperty("Storage")
    private AbstractTusdStorageDto storage;

}
