package org.knappi.gachou.rest_api.endpoints.config;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.knappi.gachou.core.usecases.config.GachouConfigService;
import org.knappi.gachou.rest_api.endpoints.config.dto.ConfigurationSchemaResponse;
import org.knappi.gachou.rest_api.endpoints.config.mappers.ConfigPropertyMapper;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/api/config/schema")
@Produces(MediaType.APPLICATION_JSON)
@PermitAll
public class ConfigurationSchemaResource {

    @Inject
    GachouConfigService gachouConfigService;

    @Inject
    ConfigPropertyMapper configPropertyMapper;

    @GET
    @Operation(operationId = "getConfigurationSchema",
               description = "Returns the property names of all configuration props")
    @APIResponse(responseCode = "200", description = "On success")
    public ConfigurationSchemaResponse getConfigurationSchema() {
        return configPropertyMapper.toResponse(gachouConfigService.buildConfig());
    }

}
