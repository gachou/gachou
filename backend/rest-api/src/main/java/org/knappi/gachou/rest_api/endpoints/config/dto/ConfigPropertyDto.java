package org.knappi.gachou.rest_api.endpoints.config.dto;

import lombok.Builder;

import javax.validation.constraints.NotNull;

@Builder
public class ConfigPropertyDto {
    @NotNull
    public final String key;
    @NotNull
    public final String envVar;
}
