package org.knappi.gachou.rest_api.endpoints.users.dto;

import lombok.Builder;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Builder
public class UserResponse {
    @NotNull
    public final String username;
    @NotNull
    public final Set<RolesDto> roles;
}
