package org.knappi.gachou.rest_api.endpoints.tusdhook.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class TusdFileStorageDto extends AbstractTusdStorageDto {
    @JsonProperty("Path")
    private String path;
}
