package org.knappi.gachou.rest_api.endpoints.uploads.dto;

public enum UploadStateDto {
    RUNNING,
    DONE
}
