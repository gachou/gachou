package org.knappi.gachou.rest_api.endpoints.tusdhook.mapper;


import org.knappi.gachou.core.model.upload.Upload;
import org.knappi.gachou.core.model.upload.UploadState;
import org.knappi.gachou.rest_api.endpoints.tusdhook.dto.TusdS3StorageDto;
import org.knappi.gachou.rest_api.endpoints.tusdhook.dto.TusdUploadDto;
import org.knappi.gachou.utils.mapstruct.MapStructConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapStructConfig.class)
public interface TusdHookRequestMapper {

    @Mapping(source = "upload.id",
             target = "uploadId")
    @Mapping(source = "upload.size",
             target = "totalSize")
    @Mapping(source = "upload.offset",
             target = "uploadedSize")
    @Mapping(source = "upload.metadata.filename",
             target = "fileName")
    @Mapping(source = "upload.metadata.lastModified",
             target = "fileLastModified")
    @Mapping(source = "upload.metadata.filetype",
             target = "fileMimeType")
    @Mapping(source = "storage.bucket",
             target = "s3Bucket")
    @Mapping(source = "storage.key",
             target = "s3ObjectKey")
    @Mapping(source = "state",
             target = "state")
    @Mapping(target = "audit",
             ignore = true)
    Upload toCore(TusdUploadDto upload,
                  TusdS3StorageDto storage,
                  UploadState state);

}
