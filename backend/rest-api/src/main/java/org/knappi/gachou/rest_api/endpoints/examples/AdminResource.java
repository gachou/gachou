package org.knappi.gachou.rest_api.endpoints.examples;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.knappi.gachou.core.model.authentication.Role;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Map;

// Will be removed when there are other admin resources
@Path("/api/admin")
@RolesAllowed(Role.Constants.ADMIN)
public class AdminResource {

    @Operation(operationId = "adminExample",
               description = "Example for restricted resource")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String,String> admin() {
        return Map.of("access", "granted");
    }
}