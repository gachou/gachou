package org.knappi.gachou.rest_api.endpoints.authentication.dto;

import lombok.Data;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotNull;

@Data
public class LoginRequest {
    @NotNull
    private String username;
    @Schema(type = SchemaType.STRING)
    @NotNull
    private char[] password;
}
