package org.knappi.gachou.rest_api.endpoints.tusdhook.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.Instant;

@Data
public class TusdMetadataDto {
    private String filename;
    private String filetype;
    /**
     * Last-modified date of the file. This can be used as fallback for the creation-date
     * if the metadata stored in EXIF or XMP tags does not contain such a date.
     */
    @NotNull
    private Instant lastModified;
}
