package org.knappi.gachou.rest_api.endpoints.config.dto;


import lombok.Builder;

import java.util.List;

@Builder
public class ConfigurationSchemaResponse {
    public final List<ConfigPropertyDto> allowedProperties;
}
