package org.knappi.gachou.rest_api.exceptionmapping;

import org.knappi.gachou.rest_api.exceptionmapping.dto.BasicExceptionResponse;
import org.knappi.gachou.utils.NotNullByDefault;

import javax.ws.rs.core.Response;

@NotNullByDefault
class ErrorResponses {
    static Response basicError(Response.Status status, String message) {
        return Response
                .status(status)
                .entity(BasicExceptionResponse
                        .builder()
                        .status(status.getStatusCode())
                        .message(message)
                        .build())
                .build();
    }
}
