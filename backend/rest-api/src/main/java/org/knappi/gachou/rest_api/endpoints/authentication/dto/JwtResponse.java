package org.knappi.gachou.rest_api.endpoints.authentication.dto;

import lombok.Builder;

import javax.validation.constraints.NotNull;

@Builder
public class JwtResponse {
    @NotNull
    public final String token;
}
