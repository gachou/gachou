package org.knappi.gachou.rest_api.endpoints.authentication;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.knappi.gachou.core.usecases.authentication.LoginService;
import org.knappi.gachou.rest_api.endpoints.authentication.dto.JwtResponse;
import org.knappi.gachou.rest_api.endpoints.authentication.dto.LoginRequest;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.naming.AuthenticationException;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Produces("application/json")
@Consumes("application/json")
@Path("/api/login")
@PermitAll
public class LoginResource {

    @Inject
    LoginService loginService;


    @Operation(operationId = "login",
               description = "Returns a JWT for username and password")
    @APIResponse(responseCode = "401", description = "If the login failed.")
    @APIResponse(responseCode = "200", description = "On success")
    @RequestBody(required = true)
    @POST
    public JwtResponse login(@Valid LoginRequest loginRequest) throws AuthenticationException {
        String token = loginService.tryCreateJwt(loginRequest.getUsername(),
                loginRequest.getPassword()
        );
        return JwtResponse.builder().token(token).build();
    }
}
