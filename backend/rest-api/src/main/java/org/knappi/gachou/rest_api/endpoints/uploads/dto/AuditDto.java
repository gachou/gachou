package org.knappi.gachou.rest_api.endpoints.uploads.dto;

import lombok.Builder;

import javax.validation.constraints.NotNull;
import java.time.Instant;

@Builder
public class AuditDto {
    @NotNull
    public final String createdBy;
    @NotNull
    public final Instant createdAt;
    @NotNull
    public final String updatedBy;
    @NotNull
    public final Instant updatedAt;

}
