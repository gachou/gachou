package org.knappi.gachou.rest_api.endpoints.users.dto;

import lombok.Builder;

import javax.validation.constraints.NotNull;
import java.util.List;

@Builder
public class ListUsersResponse {
    @NotNull
    public final List<UserResponse> users;
}
