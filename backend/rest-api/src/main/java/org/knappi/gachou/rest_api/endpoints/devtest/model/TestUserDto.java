package org.knappi.gachou.rest_api.endpoints.devtest.model;

import lombok.Data;
import org.knappi.gachou.utils.NotNullByDefault;

import javax.validation.constraints.NotNull;

@Data
public class TestUserDto {
    @NotNull
    private String username;
    @NotNull
    private String password;

    private boolean passwordIsModularCryptFormat;
}
