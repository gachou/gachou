package org.knappi.gachou.rest_api.endpoints.devtest.mapper;

import org.knappi.gachou.core.model.devtest.TestSetup;
import org.knappi.gachou.rest_api.endpoints.devtest.model.TestSetupRequest;
import org.knappi.gachou.utils.mapstruct.MapStructConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapStructConfig.class)
public interface TestSetupMapper {
    @Mapping(target = "testUsers", source = "testUsers")
    TestSetup toCore(TestSetupRequest request);
}
