package org.knappi.gachou.rest_api.endpoints.users;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.knappi.gachou.core.model.authentication.Role;
import org.knappi.gachou.core.model.authentication.User;
import org.knappi.gachou.core.usecases.authentication.UserService;
import org.knappi.gachou.rest_api.endpoints.users.dto.ListUsersResponse;
import org.knappi.gachou.rest_api.endpoints.users.dto.CreateUserRequest;
import org.knappi.gachou.rest_api.endpoints.users.dto.UpdateUserRequest;
import org.knappi.gachou.rest_api.endpoints.users.dto.UserResponse;
import org.knappi.gachou.rest_api.endpoints.users.mappers.UserMapper;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/api/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RolesAllowed(Role.Constants.ADMIN)
public class UsersResource {

    @Inject
    UserService userService;

    @Inject
    UserMapper userMapper;


    @GET
    @Operation(operationId = "listUsers",
               description = "Returns a list of users and their roles")
    public ListUsersResponse listUsers() {
        return userMapper.toDto(userService.listUserAccounts());
    }

    @Operation(operationId = "createUser",
               description = "Create a new user")
    @RequestBody(required = true)
    @POST
    public UserResponse createUser(@RequestBody CreateUserRequest createUserRequest) {
        userService.addUser(createUserRequest.getUsername(), createUserRequest.getPassword());
        return userMapper.toDto(userService.findUser(createUserRequest.getUsername()));
    }

    @GET
    @Operation(operationId = "findUser",
               description = "Finds a user by its username")
    @Path("{username}")
    public UserResponse findUser(@PathParam("username") String username) {
        return userMapper.toDto(userService.findUser(username));
    }

    @PUT
    @Operation(operationId = "updateUser",
               description = "Updates a user")
    @Path("{username}")
    @RequestBody(required = true)
    public UserResponse updateUser(@PathParam("username") String username,
                                   @RequestBody UpdateUserRequest updateUserRequest) {
        User user = userService.updateUser(username, updateUserRequest.getPassword());
        return userMapper.toDto(user);
    }

    @DELETE
    @Operation(operationId = "deleteUser",
               description = "Deletes a user")
    @Path("{username}")
    public UserResponse deleteUser(@PathParam("username") String username) {
        return userMapper.toDto(userService.deleteUser(username));
    }
}
