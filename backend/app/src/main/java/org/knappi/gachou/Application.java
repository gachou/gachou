package org.knappi.gachou;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.annotations.QuarkusMain;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Singleton;

@QuarkusMain
@Slf4j
public class Application {

    public static void main(String ... args) {
        System.out.println("Running main method");
        Quarkus.run(args);
    }
}
