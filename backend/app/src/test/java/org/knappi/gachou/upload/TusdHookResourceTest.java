package org.knappi.gachou.upload;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.common.http.TestHTTPResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.rest_api.endpoints.uploads.UploadsResource;
import org.knappi.gachou.rest_api.endpoints.tusdhook.TusdHookResource;
import org.knappi.gachou.testutils.GachouTestData;
import org.knappi.gachou.testutils.GachouTestFile;
import org.knappi.gachou.testutils.Requests;
import org.knappi.gachou.testutils.Resources;
import org.knappi.gachou.testutils.TestDataBuilder;
import org.knappi.gachou.testutils.TestProfileForTestDataSetup;
import org.knappi.gachou.testutils.upload.FakeTusdUploader;
import org.knappi.gachou.testutils.upload.FakeUploadDetails;

import javax.inject.Inject;
import java.io.IOException;
import java.nio.file.Path;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static org.knappi.gachou.testutils.Requests.withToken;

@QuarkusTest
@TestProfile(TestProfileForTestDataSetup.class)
class TusdHookResourceTest {

    @TestHTTPEndpoint(TusdHookResource.class)
    @TestHTTPResource
    String tusdHookUrl;

    @TestHTTPEndpoint(UploadsResource.class)
    @TestHTTPResource
    String currentUploadsUrl;

    private String adminToken;
    private String userToken;

    @Inject
    FakeTusdUploader fakeTusdUploader;
    private FakeTusdUploader.FakeTusdUpload userUpload;

    @BeforeEach
    public void setup() throws IOException {
        TestDataBuilder
                .startWithWipedData()
                .addUser("testuser", "abc")
                .addUser("admin", "admin")
                .apply();
        adminToken = Requests.login("admin", "admin");
        userToken = Requests.login("testuser", "abc");
        Path path = GachouTestData.fetch(GachouTestFile.BASIC__WITH_XMP_IDENTIFIER_JPG);
        userUpload = fakeTusdUploader.create(path, userToken);
    }

    @Test
    public void preCreate_hook_answers_401_ifNotLoggedIn() throws IOException {

        Path path = GachouTestData.fetch(GachouTestFile.BASIC__WITH_XMP_IDENTIFIER_JPG);
        FakeTusdUploader.FakeTusdUpload fakeTusdUpload = fakeTusdUploader.create(path, null);
        fakeTusdUpload.callPreCreate().then().assertThat().statusCode(401);
    }

    @Test
    public void preCreate_hook_answers_202_ifUser() throws IOException {
        Path path = GachouTestData.fetch(GachouTestFile.BASIC__WITH_XMP_IDENTIFIER_JPG);
        FakeTusdUploader.FakeTusdUpload fakeTusdUpload = fakeTusdUploader.create(path, userToken);
        fakeTusdUpload.callPreCreate().then().assertThat().statusCode(202);
    }

    @Test
    public void postCreate_hook_creates_running_upload() throws IOException {
        userUpload.callPreCreate();
        userUpload.callPostCreate().then().assertThat().statusCode(202);
        FakeUploadDetails fakeUploadDetails = userUpload.getFakeUploadDetails();

        withToken(userToken)
                .when()
                .get(currentUploadsUrl)
                .then()
                .body("uploads[0].id", is(fakeUploadDetails.getUploadId()))
                .body("uploads[0].totalSize", is(22632))
                .body("uploads[0].uploadedSize", is(0))
                .body("uploads[0].state", is("RUNNING"));
    }

    @Test
    public void postFinish_hook_marks_upload_as_DONE() throws IOException {
        userUpload.callPreCreate().then().assertThat().statusCode(202);
        userUpload.callPostCreate().then().assertThat().statusCode(202);
        userUpload.callPostReceive().then().assertThat().statusCode(202);
        userUpload.callPostFinish().then().assertThat().statusCode(202);
        FakeUploadDetails fakeUploadDetails = userUpload.getFakeUploadDetails();

        withToken(userToken)
                .when()
                .get(currentUploadsUrl)
                .then()
                .body("uploads[0].id", is(fakeUploadDetails.getUploadId()))
                .body("uploads[0].totalSize", is(22632))
                .body("uploads[0].uploadedSize", is(22632))
                .body("uploads[0].state", is("DONE"));
    }

    @Test
    public void adminIsNotAllowedToUpload() throws IOException {
        Path path = GachouTestData.fetch(GachouTestFile.BASIC__WITH_XMP_IDENTIFIER_JPG);
        var fakeTusdUpload = fakeTusdUploader.create(path, adminToken);
        fakeTusdUpload.callPreCreate().then().assertThat().statusCode(403);
    }

    @Test
    public void return400_ifRequestBody_isIncomplete() {
        withToken(userToken)
                .body(Resources.asStream("/fixtures/tusd/incompleteData.json"))
                .contentType(ContentType.JSON)
                .header("Hook-Name", "post-receive")
                .when()
                .post(tusdHookUrl)
                .then()
                .assertThat()
                .statusCode(400);
    }

    @Test
    public void return400_ifRequestBody_isInvalid() {
        withToken(userToken)
                .body(Resources.asStream("/fixtures/tusd/invalidSize.json"))
                .contentType(ContentType.JSON)
                .header("Hook-Name", "post-receive")
                .when()
                .post(tusdHookUrl)
                .then()
                .assertThat()
                .statusCode(400);
    }

    @Test
    public void return400_ifHookNameHeader_isMissing() {
        withToken(userToken)
                .body(Resources.asStream("/fixtures/tusd/validS3-running.json"))
                .contentType(ContentType.JSON)
                .when()
                .post(tusdHookUrl)
                .then()
                .assertThat()
                .statusCode(400);
    }

    @Test
    public void return400_ifHookNameHeader_isInvalid() {
        withToken(userToken)
                .body(Resources.asStream("/fixtures/tusd/validS3-running.json"))
                .header("Hook-Name", "a-nonexisting-header")
                .contentType(ContentType.JSON)
                .when()
                .post(tusdHookUrl)
                .then()
                .assertThat()
                .statusCode(400);
    }

    @Test
    public void return400_ifHookPointsToFileStore() {
        withToken(userToken)
                .body(Resources.asStream("/fixtures/tusd/valid-filestore-running.json"))
                .contentType(ContentType.JSON)
                .when()
                .post(tusdHookUrl)
                .then()
                .assertThat()
                .statusCode(400);
    }
}