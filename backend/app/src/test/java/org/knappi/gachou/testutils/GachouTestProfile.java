package org.knappi.gachou.testutils;

import io.quarkus.test.junit.QuarkusTestProfile;

import java.util.HashMap;
import java.util.Map;

/**
 * Inherit this test-profile and add {@link GachouConfigOverride} annotations to the subclass, in
 * order to specify config overrides.
 *
 * Note that you MUST use the {@link TestDataBuilder} to reset the test-data in each test, because
 * that also runs the {@link io.quarkus.runtime.StartupEvent}.
 *
 */
public abstract class GachouTestProfile implements QuarkusTestProfile {

    @Override
    public final Map<String, String> getConfigOverrides() {
        Map<String, String> configOverrides = new HashMap<>();
        Class<?> checkedClass = this.getClass();
        while (GachouTestProfile.class.isAssignableFrom(checkedClass)) {
            for (GachouConfigOverride configOverride : checkedClass.getAnnotationsByType(
                    GachouConfigOverride.class)) {
                configOverrides.put(configOverride.key(), configOverride.value());
            }
            checkedClass = checkedClass.getSuperclass();
        }
        return Map.copyOf(configOverrides);
    }
}
