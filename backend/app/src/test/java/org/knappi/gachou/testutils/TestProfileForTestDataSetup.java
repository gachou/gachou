package org.knappi.gachou.testutils;

import org.knappi.gachou.core.usecases.devtest.config.DangerousTestSetupConfig;

@GachouConfigOverride(key = DangerousTestSetupConfig.TOKEN,
                      value = TestProfileForTestDataSetup.TEST_SETUP_ACCESS_TOKEN)
public class TestProfileForTestDataSetup extends GachouTestProfile {
    public final static String TEST_SETUP_ACCESS_TOKEN = "integration-test-token";
}
