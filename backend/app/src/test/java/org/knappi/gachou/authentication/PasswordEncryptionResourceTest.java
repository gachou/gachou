package org.knappi.gachou.authentication;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.testutils.TestDataBuilder;
import org.knappi.gachou.testutils.TestProfileForTestDataSetup;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.knappi.gachou.testutils.Requests.login;

@QuarkusTest
@TestProfile(TestProfileForTestDataSetup.class)
public class PasswordEncryptionResourceTest {

    @Test
    public void canLogin_afterSettingEncryptedPassword() {
        String modularCryptPassword = fetchEncryptedPassword("abc");
        TestDataBuilder
                .startWithWipedData()
                .addUserWithEncryptedPassword("testuser", modularCryptPassword)
                .apply();
        String token = login("testuser", "abc");
        assertUserInfoResponse(token)
                .statusCode(200)
                .body("username", is("testuser"));
    }

    @Test
    public void canNotLogin_afterSettingDifferentEncryptedPassword() {
        String modularCryptPassword = fetchEncryptedPassword("yyy");
        TestDataBuilder
                .startWithWipedData()
                .addUserWithEncryptedPassword("testuser", modularCryptPassword)
                .apply();
        String token = login("testuser", "abc");
        assertUserInfoResponse(token)
                .statusCode(401);
    }

    private String fetchEncryptedPassword(String password) {
        return given()
                .body(Map.of("password", password))
                .contentType(ContentType.JSON)
                .when()
                .post("/api/encryptPassword")
                .body()
                .jsonPath()
                .getString("encryptedPassword");
    }

    private ValidatableResponse assertUserInfoResponse(String token) {
        return given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/api/me/user")
                .then()
                .assertThat();
    }

    @Test
    public void loginEndpoint_answers401error_ifPasswordWrong() {

        given()
                .body(Map.of("username", "testuser", "password", "wrong-password"))
                .contentType(ContentType.JSON)
                .when()
                .post("/api/login")
                .then()
                .assertThat()
                .statusCode(401)
                .body("status", is(401))
                .body("message", equalTo("Wrong username or password"));
    }
}
