package org.knappi.gachou.authentication;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.common.http.TestHTTPResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.rest_api.endpoints.users.UsersMeResource;
import org.knappi.gachou.testutils.TestDataBuilder;
import org.knappi.gachou.testutils.TestProfileForTestDataSetup;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.knappi.gachou.testutils.Requests.login;
import static org.knappi.gachou.testutils.Requests.withToken;

@QuarkusTest
@TestProfile(TestProfileForTestDataSetup.class)
public class UsersMeResourceTest {

    @TestHTTPEndpoint(UsersMeResource.class)
    @TestHTTPResource
    String url;

    private String adminToken;
    private String userToken;

    @BeforeEach
    public void initialize() {
        TestDataBuilder
                .startWithWipedData()
                .addUser("testuser", "abc")
                .addUser("admin", "admin")
                .apply();
        adminToken = login("admin", "admin");
        userToken = login("testuser", "abc");
    }

    @Test
    public void returnsAdminUser_forAdminToken() {
        withToken(adminToken)
                .get(url)
                .then()
                .assertThat()
                .statusCode(200)
                .body("username", is("admin"))
                .body("roles", is(List.of("ADMIN")));
    }

    @Test
    public void returnsUser_forUserToken() {
        withToken(userToken)
                .get(url)
                .then()
                .assertThat()
                .statusCode(200)
                .body("username", is("testuser"))
                .body("roles", is(List.of("USER")));
    }
}