package org.knappi.gachou.testutils;

import lombok.Getter;

@Getter
public enum GachouTestFile {
    BASIC__WITH_XMP_IDENTIFIER_JPG(
            "basic/with-xmp-identifier.jpg",
            "2ef9ed4ab94ce85fa0dae21ac584295b5459878881bd54ac03463fc135b69040"
    );
    private final String hash;
    private final String path;

    GachouTestFile(String path, String hash) {
        this.path = path;
        this.hash = hash;
    }

}
