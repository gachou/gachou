package org.knappi.gachou.testutils.upload;

import lombok.RequiredArgsConstructor;
import org.knappi.gachou.aws.s3.config.S3Config;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ListObjectsRequest;
import software.amazon.awssdk.services.s3.model.S3Object;

import javax.inject.Singleton;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
@RequiredArgsConstructor
public class TestUploads {


    public final FakeTusdUploader fakeTusdUploader;
    public final S3Client s3Client;
    public final S3Config s3Config;

    public FakeUploadDetails fakeFullTusdUpload(String accessToken, Path file) throws IOException {
        var fakeTusdUpload = fakeTusdUploader.create(file, accessToken);
        return fakeTusdUpload.runUpload();
    }

    public String storeMetadataInBackend(String accessToken, Path file) throws IOException {
        var fakeTusdUpload = fakeTusdUploader.create(file, accessToken);
        fakeTusdUpload.callPostCreate();
        fakeTusdUpload.callPostFinish();
        return fakeTusdUpload.id;
    }

    public Set<String> listFiles() {
        return s3Client
                .listObjects(ListObjectsRequest.builder().bucket(s3Config.uploadBucket()).build())
                .contents()
                .stream()
                .map(S3Object::key)
                .collect(Collectors.toSet());
    }
}
