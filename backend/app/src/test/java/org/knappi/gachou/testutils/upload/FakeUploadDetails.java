package org.knappi.gachou.testutils.upload;

import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotNull;

@Value
@Builder
public class FakeUploadDetails {
    @NotNull
    String uploadId;

    @NotNull
    String s3ObjectKey;
    @NotNull
    String s3Bucket;
}
