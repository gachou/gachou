package org.knappi.gachou.testutils.upload;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.common.http.TestHTTPResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.rest_api.endpoints.uploads.UploadsResource;
import org.knappi.gachou.testutils.*;

import javax.inject.Inject;
import java.nio.file.Path;

import static org.hamcrest.Matchers.contains;
import static org.knappi.gachou.testutils.Requests.withToken;

@QuarkusTest
@Slf4j
@TestProfile(TestProfileForTestDataSetup.class)
public class TestUploadsTest {

    @TestHTTPEndpoint(UploadsResource.class)
    @TestHTTPResource
    String currentUploadsUrl;

    @Inject
    TestUploads testUploads;

    @BeforeEach
    public void initialize() {
        TestDataBuilder
                .startWithWipedData()
                .addUser("bob", "pw-bob")
                .addUser("admin", "admin")
                .apply();
    }

    @Test
    public void testData() throws Exception {
        Path path = GachouTestData.fetch(GachouTestFile.BASIC__WITH_XMP_IDENTIFIER_JPG);
        String userToken = Requests.login("bob", "pw-bob");
        FakeUploadDetails result = testUploads.fakeFullTusdUpload(userToken, path);
        Assertions
                .assertThat(testUploads.listFiles())
                .contains(result.getS3ObjectKey(), result.getS3ObjectKey() + ".info");
        withToken(userToken)
                .when()
                .get(currentUploadsUrl)
                .then()
                .body("uploads.id", contains(result.getUploadId()));
    }
}
