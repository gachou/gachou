package org.knappi.gachou.testutils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Slf4j
public class GachouTestData {

    public static final String URL_PREFIX = "https://gachou.gitlab.io/testdata/files/";
    public static final String CACHE_DIRECTORY = ".gachou-testdata";

    public static Path fetch(GachouTestFile testFile) throws IOException {
        return new FileHandle(testFile).get();
    }

    public static class FileHandle {
        private final String url;
        private final Path targetFile;
        private final String hash;

        public FileHandle(GachouTestFile testFile) {
            this.targetFile = Path.of(CACHE_DIRECTORY, testFile.getPath());
            this.url = URL_PREFIX + testFile.getPath();
            this.hash = testFile.getHash();
        }


        public Path get() throws IOException {
            if (fileIsValid()) {
                log.info("Using existing " + targetFile);
                return targetFile;
            }
            log.info("Downloading " + url + " to " + targetFile);
            ensureParentDirectory();
            download();
            if (!fileIsValid()) {
                throw new RuntimeException("Testfile not valid after download");
            }
            return targetFile;
        }

        public boolean fileIsValid() throws IOException {
            if (Files.notExists(targetFile)) {
                return false;
            }
            return this.hash.equals(computeHash());
        }

        private String computeHash() throws IOException {
            try {
                byte[] bytes = Files.readAllBytes(targetFile);
                return Hex.encodeHexString(MessageDigest.getInstance("SHA-256").digest(bytes));
            } catch (NoSuchAlgorithmException e) {
                throw new IllegalStateException("Error while validating file", e);

            }
        }

        private void download() throws IOException {
            ReadableByteChannel rbc = Channels.newChannel(new URL(url).openStream());
            try (FileOutputStream fos = new FileOutputStream(targetFile.toFile())) {
                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            }
        }


        private void ensureParentDirectory() throws IOException {
            Files.createDirectories(targetFile.getParent());
        }


    }


}
