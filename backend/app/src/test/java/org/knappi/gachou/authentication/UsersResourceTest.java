package org.knappi.gachou.authentication;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.common.http.TestHTTPResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.rest_api.endpoints.users.UsersResource;
import org.knappi.gachou.testutils.TestDataBuilder;
import org.knappi.gachou.testutils.TestProfileForTestDataSetup;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.knappi.gachou.testutils.Requests.login;
import static org.knappi.gachou.testutils.Requests.tryLogin;
import static org.knappi.gachou.testutils.Requests.userForToken;
import static org.knappi.gachou.testutils.Requests.withToken;

@QuarkusTest
@TestProfile(TestProfileForTestDataSetup.class)
public class UsersResourceTest {

    @TestHTTPEndpoint(UsersResource.class)
    @TestHTTPResource
    String url;

    private String adminToken;
    private String userToken;

    @BeforeEach
    public void initialize() {
        TestDataBuilder
                .startWithWipedData()
                .addUser("testuser1", "abc")
                .addUser("testuser2", "abc")
                .addUser("admin", "admin")
                .apply();
        adminToken = login("admin", "admin");
        userToken = login("testuser1", "abc");
    }

    @Test
    public void listUsers_returns_listOfUsers() {
        withToken(adminToken)
                .get(url)
                .then()
                .assertThat()
                .statusCode(200)
                .body("users[0].username", is("admin"))
                .body("users[0].roles", is(List.of("ADMIN")))
                .body("users[1].username", is("testuser1"))
                .body("users[1].roles", is(List.of("USER")))
                .body("users[2].username", is("testuser2"))
                .body("users[2].roles", is(List.of("USER")));
    }

    @Test
    public void addUser_createsAUser_thatCanLogin() {
        withToken(adminToken)
                .body(Map.of("username", "newuser", "password", "newpass"))
                .contentType(ContentType.JSON)
                .when()
                .post(url)
                .then()
                .assertThat()
                .statusCode(200)
                .body("username", is("newuser"))
                .body("roles", is(List.of("USER")));

        String token = login("newuser", "newpass");
        assertThat(userForToken(token)).isEqualTo("newuser");
    }

    @Test
    public void addUser_return409_andCreatesNoUser_ifUserAlreadyExists() {
        withToken(adminToken)
                .body(Map.of("username", "testuser1", "password", "newpass"))
                .contentType(ContentType.JSON)
                .when()
                .post(url)
                .then()
                .assertThat()
                .body("status", is(409))
                .statusCode(409);

        tryLogin("testuser1", "newpass").then().statusCode(401);
    }

    @Test
    public void findUser_return404_ifUserIsNotFound() {
        withToken(adminToken)
                .contentType(ContentType.JSON)
                .when()
                .get(url + "/missinguser")
                .then()
                .assertThat()
                .body("status", is(404))
                .statusCode(404);
    }

    @Test
    public void findUser_returnsTheMatchingUser() {
        withToken(adminToken)
                .contentType(ContentType.JSON)
                .when()
                .get(url + "/testuser1")
                .then()
                .assertThat()
                .statusCode(200)
                .body("username", is("testuser1"))
                .body("roles", is(List.of("USER")));
    }

    @Test
    public void updateUser_updatesThePasswordOfAnExistingUser() {
        withToken(adminToken)
                .body(Map.of("password", "newpass"))
                .contentType(ContentType.JSON)
                .when()
                .put(url + "/testuser1")
                .prettyPeek()
                .then()
                .assertThat()
                .statusCode(200)
                .body("username", is("testuser1"))
                .body("roles", is(List.of("USER")));

        String token = login("testuser1", "newpass");
        assertThat(userForToken(token)).isEqualTo("testuser1");
    }

    @Test
    public void updateUser_return404_ifUserIsNotFound() {
        withToken(adminToken)
                .body(Map.of("password", "newpass"))
                .contentType(ContentType.JSON)
                .when()
                .put(url + "/missinguser")
                .then()
                .assertThat()
                .body("status", is(404))
                .statusCode(404);

        tryLogin("missinguser", "newpass").then().statusCode(401);
    }

    @Test
    public void deleteUser_return404_ifUserIsNotFound() {
        withToken(adminToken)
                .when()
                .delete(url + "/missinguser")
                .then()
                .assertThat()
                .statusCode(404)
                .body("status", is(404));

    }

    @Test
    public void deleteUser_returnsAndThen_deletesUser() {
        withToken(adminToken)
                .when()
                .delete(url + "/testuser1")
                .then()
                .assertThat()
                .statusCode(200)
                .body("username", is("testuser1"))
                .body("roles", is(List.of("USER")));

        tryLogin("testuser1", "abc").then().statusCode(401);
    }



    @Test
    public void isForbidden_forNonAdminUsers() {
        withToken(userToken).when().get(url).then().assertThat().statusCode(403);
        withToken(userToken)
                .when()
                .body(Map.of("username", "newuser", "password", "newpass"))
                .contentType(ContentType.JSON)
                .post(url)
                .then()
                .assertThat()
                .statusCode(403);

        withToken(userToken).when().get(url + "/testuser1").then().assertThat().statusCode(403);

        withToken(userToken)
                .when()
                .body(Map.of("password", "newpass"))
                .contentType(ContentType.JSON)
                .put(url + "/testuser1")
                .then()
                .assertThat()
                .statusCode(403);

        withToken(userToken)
                .when()
                .delete(url + "/testuser1")
                .then()
                .assertThat()
                .statusCode(403);
    }
}
