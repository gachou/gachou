package org.knappi.gachou.authentication.newadminpassword;

import io.quarkus.runtime.StartupEvent;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.restassured.http.ContentType;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.database.gatewyimpl.devtest.DangerousDatabaseResetGatewayImpl;
import org.knappi.gachou.testutils.GachouConfigOverride;
import org.knappi.gachou.testutils.GachouTestProfile;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Map;

import static io.restassured.RestAssured.given;

@QuarkusTest
@Slf4j
@TestProfile(NewAdminPasswordDisabledTest.NoAdminPasswordTestProfile.class)
public class NewAdminPasswordDisabledTest {

    @GachouConfigOverride(key = "gachou.auth.admin-user.encrypted-password",
                          value = "")
    public static class NoAdminPasswordTestProfile extends GachouTestProfile {
    }

    public static final String ADMIN_PASSWORD_PLAIN = "admin-password";
    @Inject
    DangerousDatabaseResetGatewayImpl databaseReset;

    @Inject
    Event<StartupEvent> startupEvent;


    @BeforeEach
    @Transactional
    public void setup() {
        databaseReset.wipeData();
    }

    @Test
    public void denysAdminLogin_ifAdminPasswordKey_isNotSet() {
        startupEvent.fire(new StartupEvent());

        given()
                .body(Map.of("username", "admin", "password", ADMIN_PASSWORD_PLAIN))
                .contentType(ContentType.JSON)
                .when()
                .post("/api/login")
                .then()
                .assertThat()
                .statusCode(401);
    }
}
