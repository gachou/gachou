package org.knappi.gachou.testutils.upload;

import org.jetbrains.annotations.NotNull;
import org.knappi.gachou.aws.s3.config.S3Config;
import org.knappi.gachou.rest_api.endpoints.tusdhook.dto.TusdHookRequest;
import org.knappi.gachou.rest_api.endpoints.tusdhook.dto.TusdHttpReqDto;
import org.knappi.gachou.rest_api.endpoints.tusdhook.dto.TusdMetadataDto;
import org.knappi.gachou.rest_api.endpoints.tusdhook.dto.TusdS3StorageDto;
import org.knappi.gachou.rest_api.endpoints.tusdhook.dto.TusdUploadDto;

import javax.enterprise.inject.spi.CDI;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Map;
import java.util.UUID;

class TusdHookRequestFactory {
    private final String id;
    private final String s3Bucket;
    private final Path path;
    private final String filetype;
    private final long size;

    public static TusdHookRequestFactory from(Path file, String id) throws IOException {
        S3Config s3Config = CDI.current().select(S3Config.class).get();
        return new TusdHookRequestFactory(file, id, s3Config.uploadBucket());
    }

    private TusdHookRequestFactory(Path path, String id, String s3Bucket) throws IOException {
        this.s3Bucket = s3Bucket;
        this.id = id;
        this.path = path;
        this.filetype = Files.probeContentType(path);
        this.size = Files.size(path);

    }

    @NotNull
    public TusdHookRequest create() throws IOException {
        TusdHookRequest tusdHookRequest = new TusdHookRequest();
        tusdHookRequest.setUpload(createUploadDto());
        tusdHookRequest.setHttpRequest(createHttpReqDto());
        return tusdHookRequest;
    }

    @NotNull
    private TusdUploadDto createUploadDto() throws IOException {
        TusdUploadDto upload = new TusdUploadDto();
        upload.setMetadata(createMetadataDto());
        upload.setStorage(createStorageDto());
        upload.setId(id);
        upload.setOffset(size);
        upload.setSize(size);
        upload.setFinal(true);
        upload.setPartial(false);
        return upload;
    }

    @NotNull
    public TusdMetadataDto createMetadataDto() throws IOException {
        TusdMetadataDto metadata = new TusdMetadataDto();
        metadata.setLastModified(Instant.ofEpochMilli(4000));
        metadata.setFilename(path.getFileName().toString());
        metadata.setFiletype(filetype);
        return metadata;
    }

    @NotNull
    public TusdS3StorageDto createStorageDto() {
        TusdS3StorageDto storage = new TusdS3StorageDto();
        storage.setBucket(s3Bucket);
        storage.setKey(id + "-key");
        return storage;
    }

    @NotNull
    private static TusdHttpReqDto createHttpReqDto() {
        TusdHttpReqDto httpRequest = new TusdHttpReqDto();
        httpRequest.setHeaders(Map.of());
        httpRequest.setMethod("PATCH");
        httpRequest.setRemoteAddress("192.168.1.0");
        httpRequest.setUri("http://example.com/irrelevant");
        return httpRequest;
    }

}
