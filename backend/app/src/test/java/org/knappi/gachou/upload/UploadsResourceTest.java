package org.knappi.gachou.upload;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.common.http.TestHTTPResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.rest_api.endpoints.uploads.UploadsResource;
import org.knappi.gachou.testutils.GachouTestData;
import org.knappi.gachou.testutils.GachouTestFile;
import org.knappi.gachou.testutils.Requests;
import org.knappi.gachou.testutils.TestDataBuilder;
import org.knappi.gachou.testutils.TestProfileForTestDataSetup;
import org.knappi.gachou.testutils.upload.FakeUploadDetails;
import org.knappi.gachou.testutils.upload.TestUploads;

import javax.inject.Inject;
import java.nio.file.Path;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.knappi.gachou.testutils.Requests.withToken;

@QuarkusTest
@Slf4j
@TestProfile(TestProfileForTestDataSetup.class)
public class UploadsResourceTest {

    @TestHTTPEndpoint(UploadsResource.class)
    @TestHTTPResource
    String currentUploadsUrl;

    @Inject
    TestUploads testUploads;

    @BeforeEach
    public void initialize() {
        TestDataBuilder
                .startWithWipedData()
                .addUser("bob", "pw-bob")
                .addUser("admin", "admin")
                .apply();
    }

    @Test
    public void cancelingUploads_removes_metadata() throws Exception {
        Path path = GachouTestData.fetch(GachouTestFile.BASIC__WITH_XMP_IDENTIFIER_JPG);
        String userToken = Requests.login("bob", "pw-bob");
        FakeUploadDetails result = testUploads.fakeFullTusdUpload(userToken, path);

        withToken(userToken)
                .when()
                .delete(currentUploadsUrl + "/" + result.getUploadId())
                .then()
                .body("id", equalTo(result.getUploadId()));

        Assertions.assertThat(testUploads.listFiles()).doesNotContain(result.getS3ObjectKey());
        Assertions
                .assertThat(testUploads.listFiles())
                .doesNotContain(result.getS3ObjectKey() + ".info");
        withToken(userToken)
                .when()
                .get(currentUploadsUrl)
                .then()
                .body("uploads.id", not(contains(result.getUploadId())));
    }

    @Test
    public void cancelingUploads_removes_metadata_even_if_file_is_not_available() throws Exception {
        Path path = GachouTestData.fetch(GachouTestFile.BASIC__WITH_XMP_IDENTIFIER_JPG);
        String userToken = Requests.login("bob", "pw-bob");
        String uploadId = testUploads.storeMetadataInBackend(userToken, path);

        withToken(userToken)
                .when()
                .delete(currentUploadsUrl + "/" + uploadId)
                .then()
                .body("id", equalTo(uploadId));

        withToken(userToken)
                .when()
                .get(currentUploadsUrl)
                .then()
                .body("uploads.id", not(contains(uploadId)));
    }

    @Test
    public void cancelingUploads_returns_404_if_upload_does_not_exist() {
        String userToken = Requests.login("bob", "pw-bob");
        withToken(userToken)
                .when()
                .delete(currentUploadsUrl + "/non-existing-upload" )
                .then()
                .statusCode(404);

    }
}
