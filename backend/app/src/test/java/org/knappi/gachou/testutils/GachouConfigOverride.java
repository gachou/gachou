package org.knappi.gachou.testutils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotate subclasses of {@link TestProfileForTestDataSetup} to add config properties
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface GachouConfigOverride {
    String key();
    String value();
}
