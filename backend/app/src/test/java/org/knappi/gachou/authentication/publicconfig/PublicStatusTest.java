package org.knappi.gachou.authentication.publicconfig;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.common.http.TestHTTPResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.rest_api.endpoints.status.PublicStatusResource;
import org.knappi.gachou.rest_api.endpoints.users.UsersResource;
import org.knappi.gachou.testutils.TestDataBuilder;
import org.knappi.gachou.testutils.TestProfileForTestDataSetup;

import java.net.URL;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

@QuarkusTest
@TestProfile(TestProfileForTestDataSetup.class)
public class PublicStatusTest {

    @TestHTTPEndpoint(PublicStatusResource.class)
    @TestHTTPResource
    URL url;
    @Test
    public void returns_adminUserExists_false_ifAdminDoestNotExist() {
        TestDataBuilder.startWithWipedData().apply();
        given()
                .when()
                .get(url)
                .then()
                .assertThat()
                .statusCode(200)
                .body("adminUserExists", is(false));
    }

    @Test
    public void returns_adminUserExists_true_ifAdminExists() {
        TestDataBuilder.startWithWipedData().addUser("admin","abc").apply();
        given()
                .when()
                .get(url)
                .then()
                .assertThat()
                .statusCode(200)
                .body("adminUserExists", is(true));
    }

}
