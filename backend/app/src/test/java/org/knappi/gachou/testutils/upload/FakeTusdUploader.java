package org.knappi.gachou.testutils.upload;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.knappi.gachou.aws.s3.utils.S3BucketAdapter;
import org.knappi.gachou.rest_api.endpoints.tusdhook.dto.TusdMetadataDto;
import org.knappi.gachou.rest_api.endpoints.tusdhook.dto.TusdS3StorageDto;
import software.amazon.awssdk.services.s3.S3Client;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Singleton;
import java.io.IOException;
import java.nio.file.Path;
import java.util.UUID;

import static org.knappi.gachou.testutils.Requests.withToken;

@RequiredArgsConstructor
@Singleton
public class FakeTusdUploader {

    private final S3Client s3Client;

    private final ObjectMapper objectMapper;

    public FakeTusdUpload create(@Nonnull Path file, @Nullable String accessToken) throws IOException {
        return new FakeTusdUpload(file, accessToken);
    }

    public class FakeTusdUpload {
        public final String id = UUID.randomUUID().toString();
        private final Path file;
        @Nullable
        private final String accessToken;

        private final TusdHookRequestFactory requestFactory;

        public FakeTusdUpload(@Nonnull Path file, @Nullable String accessToken) throws IOException {
            this.file = file;
            this.accessToken = accessToken;
            this.requestFactory = TusdHookRequestFactory.from(file, id);
        }

        public FakeUploadDetails runUpload() throws IOException {
            callPreCreate();
            uploadToS3();
            callPostCreate();
            callPostReceive();
            callPreFinish();
            callPostFinish();
            return getFakeUploadDetails();
        }

        public FakeUploadDetails getFakeUploadDetails() throws IOException {
            TusdS3StorageDto storage = requestFactory.createStorageDto();
            TusdMetadataDto metadata = requestFactory.createMetadataDto();
            return FakeUploadDetails
                    .builder()
                    .s3ObjectKey(storage.getKey())
                    .s3Bucket(storage.getBucket())
                    .uploadId(this.id)
                    .build();
        }

        @SneakyThrows
        public void uploadToS3() {
            var requestBody = requestFactory.create();
            TusdS3StorageDto storage = requestFactory.createStorageDto();
            S3BucketAdapter s3BucketAdapter = new S3BucketAdapter(storage.getBucket(), s3Client);
            s3BucketAdapter.createObject(storage.getKey(), file);
            s3BucketAdapter.createObject(
                    storage.getKey() + ".info",
                    objectMapper.writeValueAsBytes(requestBody)
            );
        }

        @SneakyThrows
        public Response callPreCreate() {
            var requestBody = requestFactory.create();
            requestBody.getUpload().setStorage(null);
            return withToken(accessToken)
                    .body(objectMapper.writeValueAsBytes(requestBody))
                    .header("Hook-Name", "pre-create")
                    .contentType(ContentType.JSON)
                    .post("/api/tusd-hook");
        }

        @SneakyThrows
        public Response callPostCreate() {
            var requestBody = requestFactory.create();
            requestBody.getUpload().setOffset(0);
            return withToken(accessToken)
                    .body(objectMapper.writeValueAsBytes(requestBody))
                    .header("Hook-Name", "post-create")
                    .contentType(ContentType.JSON)
                    .post("/api/tusd-hook");
        }

        @SneakyThrows
        public Response callPostReceive() {
            var requestBody = requestFactory.create();
            return withToken(accessToken)
                    .body(objectMapper.writeValueAsBytes(requestBody))
                    .header("Hook-Name", "post-receive")
                    .contentType(ContentType.JSON)
                    .post("/api/tusd-hook");
        }

        @SneakyThrows
        public Response callPreFinish() {
            var requestBody = requestFactory.create();
            return withToken(accessToken)
                    .body(objectMapper.writeValueAsBytes(requestBody))
                    .header("Hook-Name", "pre-finish")
                    .contentType(ContentType.JSON)
                    .post("/api/tusd-hook");
        }

        @SneakyThrows
        public Response callPostFinish() {
            var requestBody = requestFactory.create();
            return withToken(accessToken)
                    .body(objectMapper.writeValueAsBytes(requestBody))
                    .header("Hook-Name", "post-finish")
                    .contentType(ContentType.JSON)
                    .post("/api/tusd-hook");
        }

        @SneakyThrows
        public Response callPostTerminate() {
            var requestBody = requestFactory.create();
            return withToken(accessToken)
                    .body(objectMapper.writeValueAsBytes(requestBody))
                    .header("Hook-Name", "post-terminate")
                    .contentType(ContentType.JSON)
                    .post("/api/tusd-hook");
        }
    }
}

