package org.knappi.gachou.authentication;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.testutils.Requests;
import org.knappi.gachou.testutils.TestDataBuilder;
import org.knappi.gachou.testutils.TestProfileForTestDataSetup;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.knappi.gachou.testutils.Requests.login;
import static org.knappi.gachou.testutils.Requests.userForToken;

@QuarkusTest
@TestProfile(TestProfileForTestDataSetup.class)
public class LoginResourceTest {

    @BeforeEach
    public void initialize() {
        TestDataBuilder
                .startWithWipedData()
                .addUser("testuser", "abc")
                .addUser("admin", "admin")
                .apply();
    }

    @Test
    public void loginEndpoint_returnsJsonWebToken_forAuth() {
        String token = login("testuser", "abc");
        assertThat(userForToken(token)).isEqualTo("testuser");
    }

    @Test
    public void loginEndpoint_answers401error_ifPasswordWrong() {

        Requests
                .tryLogin("testuser", "wrong-password")
                .then()
                .assertThat()
                .statusCode(401)
                .body("status", is(401))
                .body("message", equalTo("Wrong username or password"));
    }
}
