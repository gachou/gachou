package org.knappi.gachou.authentication.newadminpassword;

import io.quarkus.runtime.StartupEvent;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.restassured.http.ContentType;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.database.gatewyimpl.devtest.DangerousDatabaseResetGatewayImpl;
import org.knappi.gachou.testutils.GachouConfigOverride;
import org.knappi.gachou.testutils.GachouTestProfile;

import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

@QuarkusTest
@Slf4j
@TestProfile(NewAdminPasswordEnabledTest.AdminPasswordTestProfile.class)
public class NewAdminPasswordEnabledTest {

    public static final String ADMIN_PASSWORD_ENCRYPTED =
            "JDJhJDEwJE44Tnk1QjFKWkxIak50NjFaQnNwR2VQQ3R4aGs1UnM4Sy9uMjQzdjJZMGExZC9wc2FTa20u";
    public static final String ADMIN_PASSWORD_PLAIN = "admin-password";

    @GachouConfigOverride(key = "gachou.auth.admin-user.encrypted-password",
                          value = ADMIN_PASSWORD_ENCRYPTED)
    public static class AdminPasswordTestProfile extends GachouTestProfile {
    }

    @Inject
    DangerousDatabaseResetGatewayImpl databaseReset;

    @Inject
    Event<StartupEvent> startupEvent;


    @BeforeEach
    @Transactional
    public void setup() {
        databaseReset.wipeData();
        startupEvent.fire(new StartupEvent());
    }

    @Test
    public void allowsAdminLogin_ifAdminPasswordKey_isSet() {
        String token = given()
                .body(Map.of("username", "admin", "password", ADMIN_PASSWORD_PLAIN))
                .contentType(ContentType.JSON)
                .when()
                .post("/api/login")
                .body()
                .jsonPath()
                .getString("token");
        given()
                .header("Authorization", "Bearer " + token)
                .when()
                .get("/api/me/user")
                .then()
                .assertThat()
                .statusCode(200)
                .body("username", is("admin"));
    }
}
