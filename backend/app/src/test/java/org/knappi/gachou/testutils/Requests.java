package org.knappi.gachou.testutils;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import javax.annotation.Nullable;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class Requests {
    public static String login(String username, String password) {
        return tryLogin(username, password)
                .body()
                .jsonPath()
                .getString("token");
    }

    public static Response tryLogin(String username, String password) {
        return given()
                .body(Map.of("username", username, "password", password))
                .contentType(ContentType.JSON)
                .when()
                .post("/api/login");
    }

    public static String userForToken(String token) {
        return withToken(token)
                .when()
                .get("/api/me/user")
                .then()
                .assertThat()
                .statusCode(200)
                .extract()
                .body()
                .jsonPath()
                .getString("username");
    }

    public static RequestSpecification withToken(@Nullable String token) {
        if (token == null) {
            return given();
        }
        return given().header("Authorization", "Bearer " + token);
    }
}
