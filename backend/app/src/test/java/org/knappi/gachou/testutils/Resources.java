package org.knappi.gachou.testutils;

import java.io.InputStream;

public class Resources {
    public static InputStream asStream(String name) {
        return Resources.class.getResourceAsStream(name);
    }
}
