package org.knappi.gachou.aws.s3.utils;

import io.quarkus.test.junit.QuarkusTest;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.Bucket;
import software.amazon.awssdk.services.s3.model.S3Object;

import javax.inject.Inject;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.TestInstance.Lifecycle;

@QuarkusTest
@TestInstance(Lifecycle.PER_CLASS)
class S3BucketAdapterTest {

    public static final String BUCKET_NAME_PREFIX = S3BucketAdapterTest.class
            .getSimpleName()
            .toLowerCase();
    public static final String FILE_1_TXT = "file1.txt";
    public static final String FILE_2_TXT = "file2.txt";
    public static final String FILE_3_TXT = "file3.txt";
    @Inject
    S3Client s3Client;

    @BeforeAll
    public void removeTestBuckets() {
        s3Client.listBuckets().buckets().stream().map(Bucket::name).forEach(bucketName -> {
            if (bucketName.startsWith(BUCKET_NAME_PREFIX)) {
                S3BucketAdapter s3BucketAdapter = new S3BucketAdapter(bucketName, s3Client);
                s3BucketAdapter.deleteObjects(s3BucketAdapter.streamObjects().map(S3Object::key));
                s3BucketAdapter.delete();
            }
        });
    }

    @Test
    public void testCreateExistsDelete_bucket() {
        S3BucketAdapter s3BucketAdapter = createAdapter("bucket");
        assertThat(s3BucketAdapter.exists()).isFalse();
        s3BucketAdapter.create();
        assertThat(s3BucketAdapter.exists()).isTrue();
        s3BucketAdapter.delete();
        assertThat(s3BucketAdapter.exists()).isFalse();
    }

    @Test
    public void testPutListDeleteObject_single() {
        S3BucketAdapter s3BucketAdapter = createAdapter("single-object");
        s3BucketAdapter.create();
        s3BucketAdapter.createObject(FILE_1_TXT, "abc".getBytes());
        s3BucketAdapter.createObject(FILE_2_TXT, "abc".getBytes());
        assertThat(s3BucketAdapter.streamObjects())
                .extracting(S3Object::key)
                .containsExactlyInAnyOrder(FILE_1_TXT, FILE_2_TXT);
        s3BucketAdapter.deleteObject(FILE_1_TXT);
        assertThat(s3BucketAdapter.streamObjects())
                .extracting(S3Object::key)
                .containsExactly(FILE_2_TXT);
        s3BucketAdapter.deleteObject(FILE_2_TXT);
        assertThat(s3BucketAdapter.streamObjects()).hasSize(0);
    }

    @Test
    public void testPutListBulkDeleteObjects_multiple() {
        S3BucketAdapter s3BucketAdapter = createAdapter("multi-object");
        s3BucketAdapter.create();
        s3BucketAdapter.createObject(FILE_1_TXT, "abc".getBytes());
        s3BucketAdapter.createObject(FILE_2_TXT, "abc".getBytes());
        s3BucketAdapter.createObject(FILE_3_TXT, "abc".getBytes());
        assertThat(s3BucketAdapter.streamObjects())
                .extracting(S3Object::key)
                .containsExactly(FILE_1_TXT, FILE_2_TXT, FILE_3_TXT);
        s3BucketAdapter.deleteObjects(Stream.of(FILE_1_TXT, FILE_2_TXT));
        assertThat(s3BucketAdapter.streamObjects())
                .extracting(S3Object::key)
                .containsExactly(FILE_3_TXT);
    }

    @NotNull
    private S3BucketAdapter createAdapter(String testName) {
        String bucketName = BUCKET_NAME_PREFIX + "--" + testName;
        S3BucketAdapter bucket = new S3BucketAdapter(bucketName, s3Client);
        assertThat(bucket.exists()).isFalse();
        return bucket;
    }

}