package org.knappi.gachou.aws.s3;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.CreateBucketRequest;
import software.amazon.awssdk.services.s3.model.DeleteBucketRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.ListObjectsRequest;
import software.amazon.awssdk.services.s3.model.ListObjectsResponse;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import javax.inject.Inject;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class S3ClientTest {

    public static final String TEST_BUCKET_NAME = "test-bucket";
    public static final String TEST_OBJECT_KEY = "test-file";
    public static final String FILE_CONTENTS = "new file";
    @Inject
    S3Client s3Client;

    @BeforeEach
    void setup() {
        try {
            s3Client.deleteObject(DeleteObjectRequest
                    .builder()
                    .bucket(TEST_BUCKET_NAME)
                    .key(TEST_OBJECT_KEY)
                    .build());
        } catch (Exception e) {
            // ignore
        }
        try {
            s3Client.deleteBucket(DeleteBucketRequest.builder().bucket(TEST_BUCKET_NAME).build());
        } catch (Exception e) {
            // ignore
        }
    }


    @Test
    void createBucketAndFile() {
        s3Client.createBucket(CreateBucketRequest
                .builder()
                .bucket(TEST_BUCKET_NAME)
                .build());

        s3Client.putObject(
                PutObjectRequest.builder().bucket(TEST_BUCKET_NAME).key(TEST_OBJECT_KEY).build(),
                RequestBody.fromString(FILE_CONTENTS)
        );

        ListObjectsResponse objects = s3Client.listObjects(ListObjectsRequest
                .builder()
                .bucket(TEST_BUCKET_NAME)
                .build());

        assertThat(objects.contents().size()).isEqualTo(1);
        assertThat(objects.contents().get(0).key()).isEqualTo(TEST_OBJECT_KEY);

        ResponseInputStream<GetObjectResponse> file = s3Client.getObject(GetObjectRequest
                .builder()
                .bucket(TEST_BUCKET_NAME)
                .key(TEST_OBJECT_KEY)
                .build());

        assertThat(file).asString(StandardCharsets.UTF_8).isEqualTo(FILE_CONTENTS);
    }
}
