package org.knappi.gachou.aws.s3.config;

import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix = "gachou.s3")
public interface S3Config {
    String uploadBucket();
}
