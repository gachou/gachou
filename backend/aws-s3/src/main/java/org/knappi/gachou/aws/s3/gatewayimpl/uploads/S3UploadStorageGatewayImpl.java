package org.knappi.gachou.aws.s3.gatewayimpl.uploads;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.knappi.gachou.aws.s3.config.S3Config;
import org.knappi.gachou.aws.s3.utils.S3BucketAdapter;
import org.knappi.gachou.core.model.upload.Upload;
import org.knappi.gachou.core.model.upload.UploadState;
import org.knappi.gachou.core.usecases.upload.gateway.UploadStorage;
import software.amazon.awssdk.services.s3.S3Client;

import javax.inject.Singleton;
import java.util.stream.Stream;

@Slf4j
@Singleton
public class S3UploadStorageGatewayImpl implements UploadStorage {

    private final S3BucketAdapter bucket;

    @ConfigProperty(name = "quarkus.s3.endpoint-override")
    String endpointOverride;

    public S3UploadStorageGatewayImpl(S3Config s3Config, S3Client s3Client) {
        this.bucket = new S3BucketAdapter(s3Config.uploadBucket(), s3Client);
    }


    @Override
    public void initUploadStorage() {
        log.info(String.format("Initializing upload-bucket '%s' at %s",
                bucket.getName(),
                endpointOverride
        ));

        if (bucket.exists()) {
            log.info("Bucket '" + bucket.getName() + "' already exists");
            return;
        }
        try {
            bucket.create();
        } catch (Exception e) {
            log.error("Error while creating upload buckets ", e);
        }
    }

    @Override
    public void deleteUpload(Upload upload) {
        bucket.deleteObjects(Stream.of(upload.getS3ObjectKey(), upload.getS3ObjectKey() + ".info"));
    }
}
