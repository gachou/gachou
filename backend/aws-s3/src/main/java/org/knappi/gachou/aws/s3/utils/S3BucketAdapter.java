package org.knappi.gachou.aws.s3.utils;

import lombok.extern.slf4j.Slf4j;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.CreateBucketRequest;
import software.amazon.awssdk.services.s3.model.DeleteBucketRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectResponse;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.ObjectIdentifier;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.S3Object;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class S3BucketAdapter {
    private final S3Client s3Client;
    private final String name;

    public S3BucketAdapter(String name, S3Client s3Client) {
        this.name = name;
        this.s3Client = s3Client;
    }

    public String getName() {
        return name;
    }

    public boolean exists() {
        return s3Client
                .listBuckets()
                .buckets()
                .stream()
                .anyMatch(bucket -> bucket.name().equals(this.name));
    }

    public void create() {
        s3Client.createBucket(CreateBucketRequest.builder().bucket(this.name).build());
    }

    public void delete() {
        s3Client.deleteBucket(DeleteBucketRequest.builder().bucket(this.name).build());
    }

    public Stream<S3Object> streamObjects() {
        return s3Client
                .listObjectsV2Paginator(ListObjectsV2Request.builder().bucket(this.name).build())
                .stream()
                .flatMap(page -> page.contents().stream());
    }

    public void createObject(String key, byte[] contents) {
        s3Client.putObject(
                PutObjectRequest.builder().bucket(this.name).key(key).build(),
                RequestBody.fromBytes(contents)
        );
    }

    public void createObject(String key, Path file) {
        s3Client.putObject(
                PutObjectRequest.builder().bucket(this.name).key(key).build(),
                RequestBody.fromFile(file)
        );
    }

    public void deleteObject(String key) {
        log.info("Deleting S3 object " + key);
        DeleteObjectResponse deleteObjectResponse = s3Client.deleteObject(DeleteObjectRequest
                .builder()
                .bucket(this.name)
                .key(key)
                .build());
        log.info("S3 object deleted  " + key + ": " + deleteObjectResponse.toString());
    }

    public void deleteObjects(Stream<String> keys) {
        List<ObjectIdentifier> objectIds = keys
                .map(S3BucketAdapter::asObjectIdentifiers)
                .collect(Collectors.toList());

        for (ObjectIdentifier objectId : objectIds) {
            String key = objectId.key();
            deleteObject(key);
        }
    }

    private static ObjectIdentifier asObjectIdentifiers(String key) {
        return ObjectIdentifier.builder().key(key).build();
    }
}
