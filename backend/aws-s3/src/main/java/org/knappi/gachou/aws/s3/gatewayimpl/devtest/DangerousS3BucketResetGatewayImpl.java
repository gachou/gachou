package org.knappi.gachou.aws.s3.gatewayimpl.devtest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.knappi.gachou.aws.s3.config.S3Config;
import org.knappi.gachou.aws.s3.utils.S3BucketAdapter;
import org.knappi.gachou.core.usecases.devtest.gateway.DangerousS3BucketReset;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.S3Object;

import javax.inject.Singleton;

@Slf4j
@Singleton
@RequiredArgsConstructor
public class DangerousS3BucketResetGatewayImpl implements DangerousS3BucketReset {

    private final S3Client client;
    private final S3Config config;

    @Override
    public void wipeUploadsFromS3Bucket() {
        log.info("Wiping uploaded files from s3-bucket '" + config.uploadBucket() + "'");
        S3BucketAdapter bucket = new S3BucketAdapter(config.uploadBucket(), client);
        bucket.deleteObjects(bucket.streamObjects().map(S3Object::key));
    }
}
