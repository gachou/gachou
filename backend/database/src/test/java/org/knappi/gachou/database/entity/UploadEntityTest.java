package org.knappi.gachou.database.entity;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import lombok.RequiredArgsConstructor;
import org.assertj.core.api.Assertions;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.database.entity.valuegenerators.InstantNowGenerator;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@QuarkusTest
class UploadEntityTest {

    @BeforeEach
    @Transactional
    public void cleanup() {
        UploadEntity.deleteAll();
    }

    @AfterEach
    public void resetInstantNow() {
        InstantNowGenerator.mockInstant = null;
    }

    @Test
    @TestSecurity(user = "testuser",
                  roles = {"USER"})
    @Transactional
    public void create() {

        UploadEntity entity = new UploadEntity();
        entity.uploadId = "123456";
        entity.fileName = "testfile1.png";
        entity.fileMimeType = "image/png";
        entity.fileLastModified = Instant.ofEpochMilli(4000);
        entity.s3Bucket = "upload";
        entity.s3ObjectKey = "abc";
        entity.totalSize = 2400000000L;
        entity.uploadedSize = 1200000000L;
        entity.state = UploadEntity.UploadState.RUNNING;
        entity.persistAndFlush();


        UploadEntity loadedEntity = findSingleEntity();
        Assertions.assertThat(loadedEntity.uploadId).isEqualTo("123456");
        Assertions.assertThat(loadedEntity.createdBy).isEqualTo("testuser");
        Assertions.assertThat(loadedEntity.updatedBy).isEqualTo("testuser");
        Assertions.assertThat(loadedEntity.fileName).isEqualTo("testfile1.png");
        Assertions.assertThat(loadedEntity.fileMimeType).isEqualTo("image/png");
        Assertions.assertThat(loadedEntity.fileLastModified).isEqualTo(Instant.ofEpochMilli(4000));
        Assertions.assertThat(loadedEntity.s3Bucket).isEqualTo("upload");
        Assertions.assertThat(loadedEntity.s3ObjectKey).isEqualTo("abc");
        Assertions.assertThat(loadedEntity.totalSize).isEqualTo(2400000000L);
        Assertions.assertThat(loadedEntity.uploadedSize).isEqualTo(1200000000L);
        Assertions.assertThat(loadedEntity.state).isEqualTo(UploadEntity.UploadState.RUNNING);
    }

    @NotNull
    private static UploadEntity findSingleEntity() {
        List<UploadEntity> entities = UploadEntity
                .<UploadEntity>findAll()
                .stream()
                .collect(Collectors.toList());
        Assertions.assertThat(entities).hasSize(1);

        return entities.get(0);
    }
}