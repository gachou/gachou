package org.knappi.gachou.database.gatewyimpl.uploadmetadata;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.core.model.upload.Upload;
import org.knappi.gachou.core.model.upload.UploadState;
import org.knappi.gachou.database.base.AbstractGatewayImplTest;
import org.knappi.gachou.database.entity.valuegenerators.InstantNowGenerator;

import javax.inject.Inject;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class UploadsGatewayImplTest extends AbstractGatewayImplTest {


    @Inject
    UploadsGatewayImpl uploads;

    public static final Upload.UploadBuilder baseUpload = Upload
            .builder()
            .uploadedSize(2L)
            .totalSize(4L)
            .fileName("abc.jpg")
            .fileLastModified(Instant.ofEpochMilli(2000))
            .fileMimeType("image/jpeg")
            .state(UploadState.RUNNING)
            .s3Bucket("bucket")
            .s3ObjectKey("objectKey")
            .uploadId("tusdId");

    @AfterEach
    public void resetInstantNow() {
        InstantNowGenerator.mockInstant = null;
    }

    @Test
    void auditMappedCorrectly() {
        InstantNowGenerator.mockInstant = Instant.ofEpochMilli(2000L);
        uploads.createUpload(baseUpload.build());

        List<Upload> uploads = this.uploads.streamUploads().collect(Collectors.toList());

        assertThat(uploads).hasSize(1);
        assertThat(uploads.get(0).getAudit().createdAt).isEqualTo(Instant.ofEpochMilli(2000L));
    }
}