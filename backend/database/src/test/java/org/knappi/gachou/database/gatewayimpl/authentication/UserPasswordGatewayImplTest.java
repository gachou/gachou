package org.knappi.gachou.database.gatewayimpl.authentication;

import io.quarkus.test.junit.QuarkusTest;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.core.model.authentication.UserAlreadyExistsException;
import org.knappi.gachou.core.usecases.authentication.gateway.UserAccounts;
import org.knappi.gachou.core.usecases.authentication.model.EncryptedPassword;
import org.knappi.gachou.database.base.AbstractGatewayImplTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@RequiredArgsConstructor
@QuarkusTest
class UserPasswordGatewayImplTest extends AbstractGatewayImplTest {

    public static final EncryptedPassword PASSWORD = EncryptedPassword.builder()
            .hash("somehash")
            .iterationCount(10)
            .salt("somesalt")
            .build();

    private final UserAccounts passwordService;

    @Test
    void findEncryptedPassword_findsPassword_ofAddedUser() {
        passwordService.addUser("testuser", PASSWORD);
        assertThat(passwordService.findEncryptedPassword("testuser")).hasValue(PASSWORD);
    }

    @Test
    void findEncryptedPassword_returnsEmpty_ifNoUserFound() {
        assertThat(passwordService.findEncryptedPassword("non-existing-user")).isEmpty();
    }

    @Test
    void addedUser_cannotBeAddedAgain() {
        passwordService.addUser("testuser", PASSWORD);
        assertThatThrownBy(() -> passwordService.addUser("testuser",
                PASSWORD
        )).isInstanceOf(UserAlreadyExistsException.class);
    }
}