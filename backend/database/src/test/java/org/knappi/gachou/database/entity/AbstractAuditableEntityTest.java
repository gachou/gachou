package org.knappi.gachou.database.entity;

import io.quarkus.narayana.jta.QuarkusTransaction;
import io.quarkus.security.identity.SecurityIdentity;
import io.quarkus.security.runtime.QuarkusPrincipal;
import io.quarkus.security.runtime.QuarkusSecurityIdentity;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestIdentityAssociation;
import lombok.RequiredArgsConstructor;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.database.entity.valuegenerators.InstantNowGenerator;

import javax.transaction.Transactional;
import java.time.Instant;

@QuarkusTest
@RequiredArgsConstructor
class AbstractAuditableEntityTest {

    private final TestIdentityAssociation testIdentityAssociation;

    private final SecurityIdentity creator = QuarkusSecurityIdentity
            .builder()
            .setPrincipal(new QuarkusPrincipal("creator"))
            .build();

    private final SecurityIdentity updater = QuarkusSecurityIdentity
            .builder()
            .setPrincipal(new QuarkusPrincipal("updater"))
            .build();


    @BeforeEach
    @Transactional
    public void cleanup() {
        UploadEntity.deleteAll();
    }

    @AfterEach
    public void resetInstantNow() {
        InstantNowGenerator.mockInstant = null;
    }

    @Test
    public void testAuditingValues() {
        testIdentityAssociation.setTestIdentity(creator);
        InstantNowGenerator.mockInstant = Instant.ofEpochMilli(2000);

        long id = QuarkusTransaction.call(() -> {
            UploadEntity uploadEntity = new UploadEntity();

            uploadEntity.uploadId = "";
            uploadEntity.fileName = "";
            uploadEntity.fileMimeType = "";
            uploadEntity.fileLastModified = Instant.ofEpochMilli(4000);

            uploadEntity.s3Bucket = "";
            uploadEntity.s3ObjectKey = "";

            uploadEntity.totalSize = 0L;
            uploadEntity.uploadedSize = 0L;
            uploadEntity.state = UploadEntity.UploadState.RUNNING;
            uploadEntity.persistAndFlush();
            return uploadEntity.id;
        });

        testIdentityAssociation.setTestIdentity(updater);
        InstantNowGenerator.mockInstant = Instant.ofEpochMilli(3000);


        QuarkusTransaction.run(() -> {
            UploadEntity loadedEntity = UploadEntity.findById(id);
            loadedEntity.setFileName("new filename");
            loadedEntity.persistAndFlush();
        });


        UploadEntity loadedEntity = UploadEntity.findById(id);

        Assertions.assertThat(loadedEntity.createdAt).isEqualTo(Instant.ofEpochMilli(2000));
        Assertions.assertThat(loadedEntity.createdBy).isEqualTo("creator");
        Assertions.assertThat(loadedEntity.updatedAt).isEqualTo(Instant.ofEpochMilli(3000));
        Assertions.assertThat(loadedEntity.updatedBy).isEqualTo("updater");

    }
}