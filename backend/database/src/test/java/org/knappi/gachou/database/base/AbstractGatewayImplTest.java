package org.knappi.gachou.database.base;


import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.knappi.gachou.core.usecases.devtest.gateway.DangerousDatabaseReset;

import javax.inject.Inject;
import javax.transaction.Transactional;

@QuarkusTest
public abstract class AbstractGatewayImplTest {

    @Inject
    DangerousDatabaseReset dangerousDatabaseReset;

    @BeforeEach
    @Transactional
    public void wipeDataBase() {
        dangerousDatabaseReset.wipeData();
    }
}
