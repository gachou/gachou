create sequence hibernate_sequence start 1 increment 1;


create table "user_password" (
    id serial8 primary key,
    username varchar(255) not null,
    password_salt varchar(255) not null,
    password_iteration_count int not null,
    password_hash varchar(255) not null
);