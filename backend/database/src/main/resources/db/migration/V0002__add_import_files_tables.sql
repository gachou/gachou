create table "upload_file"
(
    id                 serial8 primary key,
    upload_id          varchar(255) not null unique,
    created_by         varchar(255) not null,
    created_at         timestamp default now(),
    updated_by         varchar(255) not null,
    updated_at         timestamp,
    file_name          varchar(255) not null,
    file_mime_type     varchar(255) not null,
    file_last_modified timestamp,
    s3_bucket          varchar(255) not null,
    s3_object_key      varchar(255) not null,
    total_size         bigint       not null,
    uploaded_size      bigint       not null,
    state              varchar(20)
);

