package org.knappi.gachou.database.gatewyimpl.authentication.mapper;

import org.knappi.gachou.core.usecases.authentication.model.EncryptedPassword;
import org.knappi.gachou.core.usecases.authentication.model.Username;
import org.knappi.gachou.database.entity.UserPasswordEntity;
import org.knappi.gachou.utils.mapstruct.MapStructConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(config = MapStructConfig.class)
public interface UserPasswordEntityMapper {
    @Mapping(source = "hash",
             target = "hash")
    @Mapping(source = "iterationCount",
             target = "iterationCount")
    @Mapping(source = "salt",
             target = "salt")
    @Mapping(target = "id",
             ignore = true)
    @Mapping(target = "username",
             ignore = true)
    void updateEntity(@MappingTarget UserPasswordEntity userPasswordEntity,
                      EncryptedPassword encryptedPassword);

    @Mapping(source = "iterationCount",
             target = "iterationCount")
    @Mapping(source = "salt",
             target = "salt")
    @Mapping(source = "hash",
             target = "hash")
    EncryptedPassword toEncryptedPassword(UserPasswordEntity userPasswordEntity);

    @Mapping(source = "username",
             target = "username")
    @Mapping(source = "encryptedPassword.hash",
             target = "hash")
    @Mapping(source = "encryptedPassword.iterationCount",
             target = "iterationCount")
    @Mapping(source = "encryptedPassword.salt",
             target = "salt")
    @Mapping(target = "id",
             ignore = true)
    UserPasswordEntity toEntity(String username, EncryptedPassword encryptedPassword);

    @Mapping(source = "username",
             target = "username")
    Username toUsername(UserPasswordEntity userPasswordEntity);
}
