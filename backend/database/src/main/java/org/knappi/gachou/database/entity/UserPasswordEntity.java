package org.knappi.gachou.database.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.panache.common.Sort;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Optional;
import java.util.stream.Stream;

@Entity(name = "user_password")
@Getter
@Setter
public class UserPasswordEntity extends PanacheEntity {

    public static Optional<UserPasswordEntity> findByUsername(String username) {
        return find("username",username).firstResultOptional();
    }

    public static Stream<UserPasswordEntity> streamUsers() {
        return streamAll(Sort.ascending("username"));
    }

    private String username;

    @Column(name = "password_salt")
    public String salt;

    @Column(name = "password_hash")
    public String hash;

    @Column(name = "password_iteration_count")
    public int iterationCount;

}
