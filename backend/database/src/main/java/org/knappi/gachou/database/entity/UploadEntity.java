package org.knappi.gachou.database.entity;

import io.quarkus.panache.common.Sort;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.Instant;
import java.util.Optional;
import java.util.stream.Stream;

@Entity(name = "upload_file")
@Getter
@Setter
public class UploadEntity extends AbstractAuditableEntity {

    public static Optional<UploadEntity> findByUploadId(String uploadId) {
        return find("uploadId", uploadId).firstResultOptional();
    }

    public static Stream<UploadEntity> streamMetadata() {
        return streamAll(Sort.ascending("createdBy"));
    }

    @Column(name = "upload_id")
    public String uploadId;

    @Column(name = "file_name")
    public String fileName;

    @Column(name = "file_mime_type")
    public String fileMimeType;

    @Column(name = "file_last_modified")
    public Instant fileLastModified;

    @Column(name = "s3_bucket")
    public String s3Bucket;

    @Column(name = "s3_object_key")
    public String s3ObjectKey;

    @Column(name = "total_size")
    public long totalSize;

    @Column(name = "uploaded_size")
    public long uploadedSize;

    public enum UploadState {
        RUNNING,
        DONE
    }

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    public UploadState state;
}

