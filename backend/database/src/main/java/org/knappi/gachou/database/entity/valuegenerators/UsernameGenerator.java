package org.knappi.gachou.database.entity.valuegenerators;

import io.quarkus.security.identity.SecurityIdentity;
import org.hibernate.Session;
import org.hibernate.tuple.ValueGenerator;

import javax.enterprise.inject.spi.CDI;
import javax.inject.Singleton;

@Singleton
public class UsernameGenerator implements ValueGenerator<String> {

    @Override
    public String generateValue(Session session, Object owner) {
        SecurityIdentity identity = CDI.current().select(SecurityIdentity.class).get();
        if (identity == null) {
            throw new RuntimeException("Security identity not found in CDI");
        }
        return identity.getPrincipal().getName();
    }
}
