package org.knappi.gachou.database.gatewyimpl.authentication;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.knappi.gachou.core.model.authentication.UserAlreadyExistsException;
import org.knappi.gachou.core.model.authentication.UserNotFoundException;
import org.knappi.gachou.core.usecases.authentication.gateway.UserAccounts;
import org.knappi.gachou.core.usecases.authentication.model.EncryptedPassword;
import org.knappi.gachou.core.usecases.authentication.model.Username;
import org.knappi.gachou.database.entity.UserPasswordEntity;
import org.knappi.gachou.database.gatewyimpl.authentication.mapper.UserPasswordEntityMapper;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Singleton
@Slf4j
public class UserPasswordGatewayImpl implements UserAccounts {

    private final UserPasswordEntityMapper mapper;

    @Override
    @Transactional
    public void addUser(String username, EncryptedPassword encryptedPassword) {
        if (UserPasswordEntity.findByUsername(username).isPresent()) {
            throw new UserAlreadyExistsException(username);
        }
        storeNewUser(username, encryptedPassword);
    }

    @Override
    @Transactional
    public Optional<Username> updateUser(String username, EncryptedPassword encryptedPassword) {
        return UserPasswordEntity.findByUsername(username).map(entity -> {
            updateExistingUser(encryptedPassword, entity);
            return mapper.toUsername(entity);
        });
    }

    private void storeNewUser(String username, EncryptedPassword encryptedPassword) {
        mapper.toEntity(username, encryptedPassword).persist();
    }

    @Override
    public Stream<Username> streamUsernames() {
        return UserPasswordEntity.streamUsers().map(mapper::toUsername);
    }

    @Override
    @Transactional
    public Optional<Username> deleteUser(String username) throws UserNotFoundException {
        return UserPasswordEntity.findByUsername(username).map(userPasswordEntity -> {
            userPasswordEntity.delete();
            return mapper.toUsername(userPasswordEntity);
        });
    }

    private void updateExistingUser(EncryptedPassword encryptedPassword,
                                    UserPasswordEntity userPasswordEntity) {
        mapper.updateEntity(userPasswordEntity, encryptedPassword);
        userPasswordEntity.persist();
    }

    @Override
    public Optional<EncryptedPassword> findEncryptedPassword(String username) {
        return UserPasswordEntity.findByUsername(username).map(mapper::toEncryptedPassword);
    }

    @Override
    public Optional<Username> findUser(String username) {
        return UserPasswordEntity.findByUsername(username).map(mapper::toUsername);
    }
}
