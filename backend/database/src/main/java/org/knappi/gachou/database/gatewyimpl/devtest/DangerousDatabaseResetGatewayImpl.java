package org.knappi.gachou.database.gatewyimpl.devtest;

import org.knappi.gachou.core.usecases.devtest.gateway.DangerousDatabaseReset;
import org.knappi.gachou.database.entity.UploadEntity;
import org.knappi.gachou.database.entity.UserPasswordEntity;

import javax.inject.Singleton;

@Singleton
public class DangerousDatabaseResetGatewayImpl implements DangerousDatabaseReset {

    /**
     * For testing purposes only
     */
    public void wipeData() {
        UserPasswordEntity.deleteAll();
        UploadEntity.deleteAll();
    }
}
