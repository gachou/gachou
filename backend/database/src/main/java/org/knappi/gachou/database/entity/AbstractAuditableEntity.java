package org.knappi.gachou.database.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GeneratorType;
import org.knappi.gachou.database.entity.valuegenerators.InstantNowGenerator;
import org.knappi.gachou.database.entity.valuegenerators.UsernameGenerator;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.Instant;

@MappedSuperclass
@Getter
@Setter
public abstract class AbstractAuditableEntity extends PanacheEntity {

    @Column(name = "created_by", updatable = false)
    @GeneratorType(type = UsernameGenerator.class, when = GenerationTime.INSERT)
    public String createdBy;

    @Column(name = "created_at", updatable = false)
    @GeneratorType(type = InstantNowGenerator.class, when = GenerationTime.INSERT)
    public Instant createdAt;

    @Column(name = "updated_by")
    @GeneratorType(type = UsernameGenerator.class, when = GenerationTime.ALWAYS)
    public String updatedBy;

    @Column(name = "updated_at")
    @GeneratorType(type = InstantNowGenerator.class, when = GenerationTime.ALWAYS)
    public Instant updatedAt;

}
