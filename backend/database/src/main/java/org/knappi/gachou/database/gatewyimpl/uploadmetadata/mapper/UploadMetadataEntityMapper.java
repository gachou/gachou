package org.knappi.gachou.database.gatewyimpl.uploadmetadata.mapper;

import org.knappi.gachou.core.model.base.Audit;
import org.knappi.gachou.core.model.upload.Upload;
import org.knappi.gachou.database.entity.AbstractAuditableEntity;
import org.knappi.gachou.database.entity.UploadEntity;
import org.knappi.gachou.utils.mapstruct.MapStructConfig;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(config = MapStructConfig.class)
public interface UploadMetadataEntityMapper {

    @Mapping(source = "uploadId", target = "uploadId")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedBy", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(source = "fileName",
             target = "fileName")
    @Mapping(source = "fileMimeType",
             target = "fileMimeType")
    @Mapping(source = "fileLastModified",
             target = "fileLastModified")
    @Mapping(source = "s3Bucket",
             target = "s3Bucket")
    @Mapping(source = "s3ObjectKey",
             target = "s3ObjectKey")
    @Mapping(source = "totalSize",
             target = "totalSize")
    @Mapping(source = "uploadedSize",
             target = "uploadedSize")
    @Mapping(source = "state",
             target = "state")
    UploadEntity toEntity(Upload uploadMetadata);

    @Mapping(source = "uploadId", target = "uploadId")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdBy", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedBy", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(source = "fileName",
             target = "fileName")
    @Mapping(source = "fileMimeType",
             target = "fileMimeType")
    @Mapping(source = "fileLastModified",
             target = "fileLastModified")
    @Mapping(source = "s3Bucket",
             target = "s3Bucket")
    @Mapping(source = "s3ObjectKey",
             target = "s3ObjectKey")
    @Mapping(source = "totalSize",
             target = "totalSize")
    @Mapping(source = "uploadedSize",
             target = "uploadedSize")
    @Mapping(source = "state",
             target = "state")
    void updateEntity(@MappingTarget UploadEntity entity, Upload uploadMetadata);

    @Mapping(source = "uploadId", target = "uploadId")
    @Mapping(source = "fileName",
             target = "fileName")
    @Mapping(source = "fileMimeType",
             target = "fileMimeType")
    @Mapping(source = "fileLastModified",
             target = "fileLastModified")
    @Mapping(source = "s3Bucket",
             target = "s3Bucket")
    @Mapping(source = "s3ObjectKey",
             target = "s3ObjectKey")
    @Mapping(source = "totalSize",
             target = "totalSize")
    @Mapping(source = "uploadedSize",
             target = "uploadedSize")
    @Mapping(source = "state",
             target = "state")
    @Mapping(source = ".", target = "audit")
    Upload toCore(UploadEntity entity);


    @Mapping(target = "createdBy", source="createdBy")
    @Mapping(target = "createdAt", source="createdAt")
    @Mapping(target = "updatedBy", source="updatedBy")
    @Mapping(target = "updatedAt", source="updatedAt")
    Audit toAudit(AbstractAuditableEntity entity);
}
