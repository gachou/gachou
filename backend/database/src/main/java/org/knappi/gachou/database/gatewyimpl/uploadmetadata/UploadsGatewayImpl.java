package org.knappi.gachou.database.gatewyimpl.uploadmetadata;

import lombok.RequiredArgsConstructor;
import org.knappi.gachou.core.model.upload.Upload;
import org.knappi.gachou.core.usecases.upload.gateway.Uploads;
import org.knappi.gachou.database.entity.UploadEntity;
import org.knappi.gachou.database.gatewyimpl.uploadmetadata.mapper.UploadMetadataEntityMapper;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.stream.Stream;

@Singleton
@RequiredArgsConstructor
public class UploadsGatewayImpl implements Uploads {

    private final UploadMetadataEntityMapper mapper;


    @Override
    @Transactional
    public Upload createUpload(Upload upload) {
        UploadEntity uploadEntity = mapper.toEntity(upload);
        uploadEntity.persistAndFlush();
        return mapper.toCore(uploadEntity);
    }

    @Override
    @Transactional
    public Optional<Upload> updateUpload(Upload upload) {
        return UploadEntity.findByUploadId(upload.getUploadId()).map((entity) -> {
            mapper.updateEntity(entity, upload);
            return mapper.toCore(entity);
        });

    }

    @Override
    public Stream<Upload> streamUploads() {
        return UploadEntity.streamMetadata().map(mapper::toCore);
    }

    @Override
    public Optional<Upload> findUpload(String uploadId) {
        return UploadEntity.findByUploadId(uploadId).map(mapper::toCore);
    }

    @Override
    @Transactional
    public Optional<Upload> deleteUpload(String uploadId) {
        return UploadEntity.findByUploadId(uploadId).map(uploadEntity -> {
            uploadEntity.delete();
            return mapper.toCore(uploadEntity);
        });
    }
}
