package org.knappi.gachou.database.entity.valuegenerators;

import org.hibernate.Session;
import org.hibernate.tuple.ValueGenerator;

import javax.inject.Singleton;
import java.time.Instant;

@Singleton
public class InstantNowGenerator implements ValueGenerator<Instant> {

    public static Instant mockInstant = null;

    @Override
    public Instant generateValue(Session session, Object owner) {
        if (mockInstant != null) {
            return mockInstant;
        }
        return Instant.now();
    }
}
