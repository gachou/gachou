package org.knappi.gachou.mocks;

import io.quarkus.arc.DefaultBean;
import org.knappi.gachou.core.usecases.authentication.gateway.UserAccounts;
import org.knappi.gachou.core.usecases.devtest.gateway.DangerousDatabaseReset;
import org.knappi.gachou.core.usecases.devtest.gateway.DangerousS3BucketReset;
import org.knappi.gachou.core.usecases.upload.gateway.UploadStorage;
import org.knappi.gachou.core.usecases.upload.gateway.Uploads;
import org.mockito.Mockito;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;

/**
 * Injects mocks for beans that are defined as interface in the "core" module.
 * but may not be present for tests in submodules.
 * This class should be in the test-classpath of all module except "app"
 */
@Dependent
public class MockBeanFactory {

    @DefaultBean
    @Produces
    public UserAccounts userAccounts() {
        return Mockito.mock(UserAccounts.class);
    }

    @DefaultBean
    @Produces
    public DangerousDatabaseReset dangerousDatabaseReset() {
        return Mockito.mock(DangerousDatabaseReset.class);
    }

    @DefaultBean
    @Produces
    public Uploads uploads() {
        return Mockito.mock(Uploads.class);
    }

    @DefaultBean
    @Produces
    public UploadStorage uploadStorage() {
        return Mockito.mock(UploadStorage.class);
    }

    @DefaultBean
    @Produces
    public DangerousS3BucketReset dangerousS3BucketReset() { return Mockito.mock(DangerousS3BucketReset.class); }
}
