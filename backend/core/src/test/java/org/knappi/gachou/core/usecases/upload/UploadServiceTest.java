package org.knappi.gachou.core.usecases.upload;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.knappi.gachou.core.model.upload.Upload;
import org.knappi.gachou.core.model.upload.UploadList;
import org.knappi.gachou.core.model.upload.UploadNotFoundException;
import org.knappi.gachou.core.model.upload.UploadState;
import org.knappi.gachou.core.usecases.upload.gateway.UploadStorage;
import org.knappi.gachou.core.usecases.upload.gateway.Uploads;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UploadServiceTest {

    @InjectMocks
    UploadService uploadService;

    @Mock
    Uploads uploads;

    @Mock
    UploadStorage uploadStorage;


    @Test
    void updateUploadProgress_updates_running_uploads() {
        Upload upload = Upload.builder().uploadId("upload1").build();
        when(uploads.updateUpload(upload)).thenReturn(Optional.of(upload));
        uploadService.updateUploadProgress(upload);
        verify(uploads).updateUpload(upload);
    }

    @Test
    void updateUploadProgress_throws_on_missing_upload() {
        Upload upload = Upload.builder().uploadId("upload1").build();
        when(uploads.updateUpload(upload)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> uploadService.updateUploadProgress(upload)).isInstanceOf(
                UploadNotFoundException.class);

    }

    @Test
    void createUpload_adds_new_uploads() {
        Upload upload = Upload.builder().uploadId("upload1").build();
        uploadService.createUpload(upload);
        verify(uploads).createUpload(upload);
    }

    @Test
    void listAllUploads() {
        Upload upload1 = Upload.builder().uploadId("upload1").build();
        Upload upload2 = Upload.builder().uploadId("upload2").build();
        when(uploads.streamUploads()).thenReturn(Stream.of(upload1, upload2));
        assertThat(uploadService.listAllUploads()).isEqualTo(UploadList
                .builder()
                .uploads(List.of(upload1, upload2))
                .build());
    }

    @Test
    void cancelUpload_removes_RUNNING_uploads_from_storage() {
        Upload mockUpload = Upload.builder().uploadId("upload1").state(UploadState.RUNNING).build();
        when(uploads.deleteUpload("upload1")).thenReturn(Optional.of(mockUpload));
        assertThat(uploadService.cancelUpload("upload1")).isEqualTo(mockUpload);
        verify(uploads).deleteUpload(eq("upload1"));
        verify(uploadStorage, times(1)).deleteUpload(any());
    }

    @Test
    void cancelUpload_removes_DONE_uploads_from_storage() {
        Upload mockUpload = Upload.builder().uploadId("upload1").state(UploadState.DONE).build();
        when(uploads.deleteUpload("upload1")).thenReturn(Optional.of(mockUpload));
        assertThat(uploadService.cancelUpload("upload1")).isEqualTo(mockUpload);
        verify(uploads).deleteUpload(eq("upload1"));
        verify(uploadStorage, times(1)).deleteUpload(mockUpload);
    }
}