package org.knappi.gachou.core.usecases.config;

import org.junit.jupiter.api.Test;
import org.knappi.gachou.core.usecases.authentication.config.AdminUserConfig;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class ConfigHelperTest {

    @Test
    void retrievesConfigName_byClassAndMethod() {
        String username = ConfigHelper.getConfigKey(AdminUserConfig.class, "username");
        assertThat(username).isEqualTo("gachou.auth.admin-user.username");
    }

    @Test
    void throwsIfMethodDoesNotExist() {
        assertThatThrownBy(() -> ConfigHelper.getConfigKey(
                        AdminUserConfig.class,
                        "invalidName"
                ))
                .hasMessageContaining("invalidName")
                .hasMessageContaining(AdminUserConfig.class.getName());
    }


    @Test
    void throwsIfClass_isNotConfigMapping() {
        assertThatThrownBy(() -> ConfigHelper.getConfigKey(
                String.class,
                "invalidName"
        ))
                .hasMessageContaining(String.class.getName());
    }
}