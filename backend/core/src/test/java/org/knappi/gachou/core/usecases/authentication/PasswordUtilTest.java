package org.knappi.gachou.core.usecases.authentication;

import org.junit.jupiter.api.Test;
import org.knappi.gachou.core.usecases.authentication.model.EncryptedPassword;

import static org.assertj.core.api.Assertions.assertThat;

class PasswordUtilTest {

    public static final char[] PASSWORD = "my-password".toCharArray();
    public static final char[] OTHER_PASSWORD = "other-password".toCharArray();

    @Test
    void encryptedPassword_shouldBeVerified_ifPasswordCorrect() {

        EncryptedPassword encryptedPassword = PasswordUtil.encryptPassword(PASSWORD);
        assertThat(PasswordUtil.verify(encryptedPassword, PASSWORD)).isTrue();
        assertThat(PasswordUtil.verify(encryptedPassword, OTHER_PASSWORD)).isFalse();
    }

    @Test
    void modularCryptPassword_shouldBeVerified_ifPasswordCorrect() {
        String modularCrypt = PasswordUtil.encryptPasswordToModularCrypt(PASSWORD);
        EncryptedPassword encryptedPassword = PasswordUtil.parseModularCrypt(modularCrypt);
        assertThat(PasswordUtil.verify(encryptedPassword, PASSWORD)).isTrue();
        assertThat(PasswordUtil.verify(encryptedPassword, OTHER_PASSWORD)).isFalse();
    }
}