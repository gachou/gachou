package org.knappi.gachou.core.usecases.config;

import io.smallrye.config.ConfigMapping;
import org.assertj.core.api.Assertions;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.knappi.gachou.core.model.config.ConfigProperty;
import org.knappi.gachou.core.model.config.GachouConfiguration;

import javax.enterprise.inject.Instance;
import javax.enterprise.util.TypeLiteral;
import java.lang.annotation.Annotation;
import java.util.Iterator;
import java.util.List;


class GachouConfigServiceTest {
    @Test
    public void buildConfig() {
        GachouConfigService gachouConfigService =
                new GachouConfigService(asInstance(List.of(new TestMapping1.Provider(),
                new TestMapping2.Provider()
        )));

        Assertions
                .assertThat(gachouConfigService.buildConfig())
                .isEqualTo(GachouConfiguration
                        .builder()
                        .property(ConfigProperty
                                .builder()
                                .key("test.mapping1.test-prop1")
                                .envVar("TEST_MAPPING1_TEST_PROP1")
                                .methodName("testProp1")
                                .build())
                        .property(ConfigProperty
                                .builder()
                                .key("test.mapping1.test-prop2")
                                .envVar("TEST_MAPPING1_TEST_PROP2")
                                .methodName("testProp2")
                                .build())
                        .property(ConfigProperty
                                .builder()
                                .key("test.mapping2.test-prop1")
                                .envVar("TEST_MAPPING2_TEST_PROP1")
                                .methodName("testProp1")
                                .build())
                        .property(ConfigProperty
                                .builder()
                                .key("test.mapping2.test-prop2")
                                .envVar("TEST_MAPPING2_TEST_PROP2")
                                .methodName("testProp2")
                                .build())
                        .build());

    }

    @ConfigMapping(prefix = "test.mapping1")
    interface TestMapping1 {
        String testProp1();

        String testProp2();

        class Provider implements ConfigMappingProvider {

            @Override
            public Class<?> configMapping() {
                return TestMapping1.class;
            }
        }
    }

    @ConfigMapping(prefix = "test.mapping2")
    interface TestMapping2 {
        String testProp1();

        String testProp2();

        class Provider implements ConfigMappingProvider {

            @Override
            public Class<?> configMapping() {
                return TestMapping2.class;
            }
        }
    }

    public static <T> Instance<T> asInstance(Iterable<T> iterable) {
        return new Instance<>() {
            @Override
            public Instance<T> select(Annotation... qualifiers) {
                return null;
            }

            @Override
            public <U extends T> Instance<U> select(Class<U> subtype, Annotation... qualifiers) {
                return null;
            }

            @Override
            public <U extends T> Instance<U> select(TypeLiteral<U> subtype,
                                                    Annotation... qualifiers) {
                return null;
            }

            @Override
            public boolean isUnsatisfied() {
                return false;
            }

            @Override
            public boolean isAmbiguous() {
                return false;
            }

            @Override
            public void destroy(T instance) {

            }

            @NotNull
            @Override
            public Iterator<T> iterator() {
                return iterable.iterator();
            }

            @Override
            public T get() {
                return null;
            }
        };
    }
}