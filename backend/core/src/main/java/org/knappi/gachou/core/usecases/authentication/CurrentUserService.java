package org.knappi.gachou.core.usecases.authentication;

import io.quarkus.security.identity.SecurityIdentity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.knappi.gachou.core.model.authentication.User;

import javax.enterprise.context.RequestScoped;

@RequestScoped
@Getter
@RequiredArgsConstructor
public class CurrentUserService {

    private final SecurityIdentity identity;

    private final UserService userService;

    public User findCurrentUser() {
        return userService.findUser(identity.getPrincipal().getName());
    }
}
