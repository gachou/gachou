package org.knappi.gachou.core.usecases.devtest.gateway;

public interface DangerousS3BucketReset {
    void wipeUploadsFromS3Bucket();
}
