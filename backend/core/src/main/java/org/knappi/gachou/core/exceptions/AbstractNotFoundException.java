package org.knappi.gachou.core.exceptions;

public abstract class AbstractNotFoundException extends RuntimeException {
    public AbstractNotFoundException(String message) {
        super(message);
    }
}
