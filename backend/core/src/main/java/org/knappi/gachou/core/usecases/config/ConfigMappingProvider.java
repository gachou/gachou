package org.knappi.gachou.core.usecases.config;

/**
 * Base interface for config mappings, for the single purpose of
 * collecting them via CDI
 */
public interface ConfigMappingProvider {
    Class<?> configMapping();
}
