package org.knappi.gachou.core.model.upload;

import lombok.Getter;
import org.knappi.gachou.core.exceptions.AbstractNotFoundException;

@Getter
public class UploadNotFoundException extends AbstractNotFoundException {
    private final String uploadId;

    public UploadNotFoundException(String uploadId) {
        super("Upload not found: " + uploadId);
        this.uploadId = uploadId;
    }
}
