package org.knappi.gachou.core.model.devtest;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class TestSetup {
    List<TestUser> testUsers;
}
