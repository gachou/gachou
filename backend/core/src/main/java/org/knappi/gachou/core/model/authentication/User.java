package org.knappi.gachou.core.model.authentication;

import lombok.Builder;
import lombok.Value;

import java.util.Set;

@Value
@Builder
public class User {
    String username;
    Set<Role> roles;
}
