package org.knappi.gachou.core.usecases.authentication;

import lombok.RequiredArgsConstructor;
import org.knappi.gachou.core.usecases.authentication.model.EncryptedPassword;
import org.knappi.gachou.core.model.authentication.User;
import org.knappi.gachou.core.model.authentication.UserList;
import org.knappi.gachou.core.model.authentication.UserNotFoundException;
import org.knappi.gachou.core.usecases.authentication.config.AdminUserConfig;
import org.knappi.gachou.core.usecases.authentication.gateway.UserAccounts;
import org.knappi.gachou.core.usecases.authentication.model.Username;

import javax.inject.Singleton;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
@RequiredArgsConstructor
public class UserService {

    private final UserAccounts userAccounts;
    private final AdminUserConfig adminUserConfig;

    private final UserRolesHelper userRolesHelper;


    public void addUser(String username, char[] password) {
        userAccounts.addUser(username, PasswordUtil.encryptPassword(password));
    }

    public User updateUser(String username, char[] password) {
        Username result = userAccounts
                .updateUser(username, PasswordUtil.encryptPassword(password))
                .orElseThrow(() -> new UserNotFoundException(username));
        return asUser(result);
    }

    public void upsertUser(String username, char[] password) {
        upsert(username, PasswordUtil.encryptPassword(password));
    }

    public void upsertUserWithModularCryptPassword(String username, String modularCryptPassword) {
        upsert(username, PasswordUtil.parseModularCrypt(modularCryptPassword));
    }

    private void upsert(String username, EncryptedPassword encryptedPassword) {
        Optional<Username> updatedUser = userAccounts.updateUser(username, encryptedPassword);
        if (updatedUser.isEmpty()) {
            userAccounts.addUser(username, encryptedPassword);
        }
    }

    public boolean doesAdminUserExist() {
        String adminUser = adminUserConfig.username();
        return userAccounts.findUser(adminUser).isPresent();
    }

    public UserList listUserAccounts() {
        return UserList
                .builder()
                .users(userAccounts
                        .streamUsernames()
                        .map(this::asUser)
                        .collect(Collectors.toList()))
                .build();
    }

    public User findUser(String username) {
        Username result = userAccounts
                .findUser(username)
                .orElseThrow(() -> new UserNotFoundException(username));
        return asUser(result);
    }

    public User deleteUser(String username) {
        Username result = userAccounts.deleteUser(username)
                .orElseThrow(() -> new UserNotFoundException(username));
        return asUser(result);
    }

    private User asUser(Username username) {
        return User
                .builder()
                .username(username.getUsername())
                .roles(Set.of(userRolesHelper.groupOfUser(username.getUsername())))
                .build();
    }
}
