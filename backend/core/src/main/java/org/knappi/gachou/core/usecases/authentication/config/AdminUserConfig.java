package org.knappi.gachou.core.usecases.authentication.config;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;
import org.eclipse.microprofile.config.Config;
import org.knappi.gachou.core.usecases.config.ConfigMappingProvider;

import javax.inject.Singleton;
import java.util.Optional;

@ConfigMapping(prefix = "gachou.auth.admin-user")
public interface AdminUserConfig {

    Optional<String> encryptedPassword();

    @WithDefault("admin")
    String username();

    @Singleton
    class SchemaProvider implements ConfigMappingProvider {

        @Override
        public Class<?> configMapping() {
            return AdminUserConfig.class;
        }
    }
}
