package org.knappi.gachou.core.usecases.upload;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.knappi.gachou.core.model.upload.Upload;
import org.knappi.gachou.core.model.upload.UploadList;
import org.knappi.gachou.core.model.upload.UploadNotFoundException;
import org.knappi.gachou.core.model.upload.UploadState;
import org.knappi.gachou.core.usecases.upload.gateway.UploadStorage;
import org.knappi.gachou.core.usecases.upload.gateway.Uploads;

import javax.inject.Singleton;
import java.util.stream.Collectors;

@Singleton
@RequiredArgsConstructor
@Slf4j
public class UploadService {

    private final Uploads uploads;
    private final UploadStorage uploadStorage;


    public Upload createUpload(Upload upload) {
        return uploads.createUpload(upload);
    }

    public Upload updateUploadProgress(Upload upload) {
        return uploads
                .updateUpload(upload)
                .orElseThrow(() -> new UploadNotFoundException(upload.getUploadId()));
    }

    public UploadList listAllUploads() {
        return UploadList
                .builder()
                .uploads(uploads.streamUploads().collect(Collectors.toList()))
                .build();
    }

    public Upload cancelUpload(String uploadId) {
        return uploads.deleteUpload(uploadId).map(upload -> {
            uploadStorage.deleteUpload(upload);
            return upload;
        }).orElseThrow(() -> new UploadNotFoundException(uploadId));

    }
}
