package org.knappi.gachou.core.model.devtest;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class TestUser {
    String username;
    String password;
    boolean passwordIsModularCryptFormat;
}
