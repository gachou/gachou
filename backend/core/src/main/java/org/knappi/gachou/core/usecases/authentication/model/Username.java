package org.knappi.gachou.core.usecases.authentication.model;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class Username {
    String username;
}
