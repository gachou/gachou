package org.knappi.gachou.core.model.base;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Builder
@Data
public class Audit {
    public final String createdBy;
    public final Instant createdAt;
    public final String updatedBy;
    public final Instant updatedAt;
}
