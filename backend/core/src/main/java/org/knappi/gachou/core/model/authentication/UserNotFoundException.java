package org.knappi.gachou.core.model.authentication;

import lombok.Getter;
import org.knappi.gachou.core.exceptions.AbstractNotFoundException;

/**
 * This exception is meant to be thrown when a user is looked by for editing or
 * display, but NOT during log-in.
 */
@Getter
public class UserNotFoundException extends AbstractNotFoundException {
    private final String username;

    public UserNotFoundException(String username) {
        super("User not found: " + username);
        this.username = username;
    }
}
