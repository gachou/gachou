package org.knappi.gachou.core.model.config;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ConfigProperty {
    String key;
    String envVar;
    String methodName;
}
