package org.knappi.gachou.core.model.upload;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class UploadList {
    List<Upload> uploads;
}
