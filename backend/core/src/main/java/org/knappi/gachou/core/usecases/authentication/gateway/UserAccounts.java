package org.knappi.gachou.core.usecases.authentication.gateway;

import org.knappi.gachou.core.usecases.authentication.model.EncryptedPassword;
import org.knappi.gachou.core.model.authentication.UserNotFoundException;
import org.knappi.gachou.core.usecases.authentication.model.Username;

import java.util.Optional;
import java.util.stream.Stream;

public interface UserAccounts {

    void addUser(String username, EncryptedPassword password);

    Stream<Username> streamUsernames();

    Optional<Username> findUser(String username);

    Optional<Username> updateUser(String username, EncryptedPassword encryptedPassword);

    Optional<Username> deleteUser(String username) throws UserNotFoundException;

    Optional<EncryptedPassword> findEncryptedPassword(String username);
}
