package org.knappi.gachou.core.usecases.upload.gateway;

import org.knappi.gachou.core.model.upload.Upload;

import java.util.Optional;
import java.util.stream.Stream;

public interface Uploads {
    Upload createUpload(Upload upload);
    Optional<Upload> updateUpload(Upload upload);

    Stream<Upload> streamUploads();

    Optional<Upload> findUpload(String uploadId);

    Optional<Upload> deleteUpload(String uploadId);

}
