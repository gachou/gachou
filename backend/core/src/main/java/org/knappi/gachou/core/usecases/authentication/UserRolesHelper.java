package org.knappi.gachou.core.usecases.authentication;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.knappi.gachou.core.model.authentication.Role;
import org.knappi.gachou.core.usecases.authentication.config.AdminUserConfig;

import javax.inject.Singleton;

@Singleton
@RequiredArgsConstructor
class UserRolesHelper {
    private final AdminUserConfig adminUserConfig;

    @NotNull Role groupOfUser(String username) {
        if (username.equals(adminUserConfig.username())) {
            return Role.ADMIN;
        }
        return Role.USER;
    }
}
