package org.knappi.gachou.core.model.config;

import lombok.Builder;
import lombok.Singular;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class GachouConfiguration {
    @Singular
    List<ConfigProperty> properties;
}
