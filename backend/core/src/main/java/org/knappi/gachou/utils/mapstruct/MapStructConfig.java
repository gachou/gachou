package org.knappi.gachou.utils.mapstruct;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.MapperConfig;
import org.mapstruct.ReportingPolicy;

@MapperConfig(
        injectionStrategy = InjectionStrategy.CONSTRUCTOR,
        componentModel = "cdi",
        uses = CommonMapper.class,
        unmappedTargetPolicy = ReportingPolicy.ERROR
)
public interface MapStructConfig {
}
