package org.knappi.gachou.core.usecases.devtest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.knappi.gachou.core.usecases.authentication.UserService;
import org.knappi.gachou.core.usecases.devtest.config.DangerousTestSetupConfig;
import org.knappi.gachou.core.usecases.devtest.exception.TestDataSetupForbiddenException;
import org.knappi.gachou.core.usecases.devtest.gateway.DangerousDatabaseReset;
import org.knappi.gachou.core.model.devtest.TestSetup;
import org.knappi.gachou.core.model.devtest.TestUser;
import org.knappi.gachou.core.usecases.devtest.gateway.DangerousS3BucketReset;

import javax.inject.Singleton;
import javax.transaction.Transactional;

@RequiredArgsConstructor
@Singleton
@Slf4j
public class TestSetupService {
    private final DangerousDatabaseReset databaseReset;
    private final UserService userService;

    private final DangerousS3BucketReset s3Reset;

    private final DangerousTestSetupConfig config;

    public void setupForTest(TestSetup testSetup, String accessToken)
            throws TestDataSetupForbiddenException {
        if (config.getExpectedAccessToken().isEmpty()) {
            log.error("TestDataSetup disabled");
            throw new TestDataSetupForbiddenException();
        }
        if (!config.getExpectedAccessToken().get().equals(accessToken)) {
            log.error("TestDataSetup denied (wrong token)");
            throw new TestDataSetupForbiddenException();
        }
        setupDatabase(testSetup.getTestUsers());
        s3Reset.wipeUploadsFromS3Bucket();
    }


    @Transactional
    void setupDatabase(Iterable<TestUser> testUsers) {
        databaseReset.wipeData();
        for (TestUser testUser : testUsers) {
            addTestUser(
                    testUser.getUsername(),
                    testUser.getPassword(),
                    testUser.isPasswordIsModularCryptFormat()
            );
        }
    }

    private void addTestUser(String username, String password, boolean modularCryptFormat) {
        if (modularCryptFormat) {
            userService.upsertUserWithModularCryptPassword(username, password);
        } else {
            userService.addUser(username, password.toCharArray());
        }
    }
}
