package org.knappi.gachou.core.model.upload;

import lombok.Builder;
import lombok.Value;
import org.knappi.gachou.core.model.base.Audit;

import java.time.Instant;

@Builder
@Value
public class Upload {
    String uploadId;
    String fileName;
    String fileMimeType;
    Instant fileLastModified;
    String s3Bucket;
    String s3ObjectKey;
    long totalSize;
    long uploadedSize;
    UploadState state;
    Audit audit;
}
