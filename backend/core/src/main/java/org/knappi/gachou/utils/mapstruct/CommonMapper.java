package org.knappi.gachou.utils.mapstruct;

import org.mapstruct.Mapper;

@Mapper(componentModel = "cdi")
public interface CommonMapper {
    default char[] toCharArray(String string) {
        return string.toCharArray();
    }
}
