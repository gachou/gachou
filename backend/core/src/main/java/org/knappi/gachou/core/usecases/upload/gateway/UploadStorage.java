package org.knappi.gachou.core.usecases.upload.gateway;

import org.knappi.gachou.core.model.upload.Upload;

public interface UploadStorage {
    void initUploadStorage();

    void deleteUpload(Upload uploads);
}
