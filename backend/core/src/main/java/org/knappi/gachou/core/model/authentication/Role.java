package org.knappi.gachou.core.model.authentication;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum Role {
    ADMIN(Constants.ADMIN),
    USER(Constants.USER);

    private final String role;

    public static class Constants {
        public static final String ADMIN = "ADMIN";
        public static final String USER = "USER";
    }
}
