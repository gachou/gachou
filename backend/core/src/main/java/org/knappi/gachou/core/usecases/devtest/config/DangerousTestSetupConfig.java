package org.knappi.gachou.core.usecases.devtest.config;

import lombok.Getter;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
@Getter
public class DangerousTestSetupConfig {

    public static final String TOKEN = "gachou.dangerous-test-setup-access.token";

    @ConfigProperty(name = DangerousTestSetupConfig.TOKEN)
    Optional<String> expectedAccessToken;
}
