package org.knappi.gachou.core.usecases.authentication;

import org.knappi.gachou.core.usecases.authentication.model.EncryptedPassword;
import org.wildfly.security.password.Password;
import org.wildfly.security.password.PasswordFactory;
import org.wildfly.security.password.WildFlyElytronPasswordProvider;
import org.wildfly.security.password.interfaces.BCryptPassword;
import org.wildfly.security.password.spec.EncryptablePasswordSpec;
import org.wildfly.security.password.spec.IteratedSaltedPasswordAlgorithmSpec;
import org.wildfly.security.password.util.ModularCrypt;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import static org.knappi.gachou.core.usecases.authentication.Base64Util.decodeBase64;
import static org.knappi.gachou.core.usecases.authentication.Base64Util.encodeBase64;

class PasswordUtil {
    public static final int ITERATIONS = 10;

    public static boolean verify(EncryptedPassword expectedPassword, char[] guess) {
        byte[] salt = decodeBase64(expectedPassword.getSalt());
        int iterationCount = expectedPassword.getIterationCount();
        byte[] hashFromGuess = createBcryptPassword(salt, iterationCount, guess).getHash();
        byte[] expectedHash = decodeBase64(expectedPassword.getHash());
        return Arrays.equals(expectedHash, hashFromGuess);
    }

    static EncryptedPassword encryptPassword(char[] password) {
        BCryptPassword bcryptPassword = createBcryptPassword(createSalt(), ITERATIONS, password);
        return toEncryptedPassword(bcryptPassword);
    }

    private static EncryptedPassword toEncryptedPassword(BCryptPassword bcryptPassword) {
        return EncryptedPassword
                .builder()
                .hash(encodeBase64(bcryptPassword.getHash()))
                .salt(encodeBase64(bcryptPassword.getSalt()))
                .iterationCount(bcryptPassword.getIterationCount())
                .build();
    }

    public static EncryptedPassword parseModularCrypt(String base64ModularCryptString) {
        String modularCryptString = new String(Base64Util.decodeBase64(base64ModularCryptString),
                StandardCharsets.UTF_8
        );
        try {
            Password decoded = ModularCrypt.decode(modularCryptString);
            BCryptPassword bcrypt = decoded.castAs(BCryptPassword.class);
            return toEncryptedPassword(bcrypt);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException("Error parsing Module Crypt Format password", e);
        }
    }


    public static String encryptPasswordToModularCrypt(char[] password) {
        byte[] salt = createSalt();
        BCryptPassword bCryptPassword = createBcryptPassword(salt, ITERATIONS, password);
        try {
            String modularCrypt = ModularCrypt.encodeAsString(bCryptPassword);
            return Base64Util.encodeBase64(modularCrypt.getBytes(StandardCharsets.UTF_8));
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException("Error converting password to Modular Crypt Format", e);
        }
    }


    private static BCryptPassword createBcryptPassword(byte[] salt,
                                                       int iterations,
                                                       char[] password) {
        try {
            return tryCreateBcryptHash(salt, iterations, password);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Configuration error: Configured algorithm not found", e);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException("Configuration error: Invalid key spec", e);
        }
    }

    private static BCryptPassword tryCreateBcryptHash(byte[] salt, int iterations, char[] password)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        Password result = generatedPassword(salt, iterations, password);
        return result.castAs(BCryptPassword.class);
    }

    private static Password generatedPassword(byte[] salt, int iterations, char[] password)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        PasswordFactory instance = PasswordFactory.getInstance(BCryptPassword.ALGORITHM_BCRYPT,
                new WildFlyElytronPasswordProvider()
        );
        var algorithmSpec = new IteratedSaltedPasswordAlgorithmSpec(iterations, salt);
        var passwordSpec = new EncryptablePasswordSpec(password, algorithmSpec);
        return instance.generatePassword(passwordSpec);
    }

    private static byte[] createSalt() {
        byte[] salt = new byte[16];
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(salt);
        return salt;
    }

}
