package org.knappi.gachou.core.model.authentication;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class UserList {
    List<User> users;
}
