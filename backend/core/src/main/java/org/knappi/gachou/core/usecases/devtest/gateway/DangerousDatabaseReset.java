package org.knappi.gachou.core.usecases.devtest.gateway;

public interface DangerousDatabaseReset {
    void wipeData();
}
