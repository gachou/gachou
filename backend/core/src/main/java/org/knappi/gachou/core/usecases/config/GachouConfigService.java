package org.knappi.gachou.core.usecases.config;

import lombok.RequiredArgsConstructor;
import org.knappi.gachou.core.model.config.ConfigProperty;
import org.knappi.gachou.core.model.config.GachouConfiguration;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@ApplicationScoped
@RequiredArgsConstructor
public class GachouConfigService {


    private final Instance<ConfigMappingProvider> configMappingProviders;

    public GachouConfiguration buildConfig() {
        List<ConfigProperty> properties = new ArrayList<>();
        for (ConfigMappingProvider provider : configMappingProviders) {
            ConfigHelper.allConfigProperties(provider.configMapping()).forEach(properties::add);
        }
        properties.sort(Comparator.comparing(ConfigProperty::getKey));
        return GachouConfiguration.builder().properties(properties).build();
    }
}
