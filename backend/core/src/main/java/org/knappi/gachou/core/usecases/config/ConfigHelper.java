package org.knappi.gachou.core.usecases.config;

import io.smallrye.config.ConfigMappingInterface;
import io.smallrye.config.ConfigMappings;
import org.jetbrains.annotations.NotNull;
import org.knappi.gachou.core.model.config.ConfigProperty;

import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public class ConfigHelper {

    public static String getConfigKey(Class<?> clazz, String methodName) {
        return findConfigProperty(clazz, methodName)
                .orElseThrow(() -> new IllegalArgumentException(String.format(
                        "No configuration key found for method '%s' on class '%s'",
                        methodName,
                        clazz.getName()
                )))
                .getKey();
    }

    @NotNull
    private static Optional<ConfigProperty> findConfigProperty(Class<?> clazz, String methodName) {
        try {
            return allConfigProperties(clazz)
                    .filter(entry -> entry.getMethodName().equals(methodName))
                    .findFirst();
        } catch (Exception e) {
            throw new IllegalStateException(String.format(
                    "Error deriving config key from method '%s' on class '%s'",
                    methodName,
                    clazz.getName()
            ));
        }
    }

    static Stream<ConfigProperty> allConfigProperties(Class<?> clazz) {
        return ConfigMappings
                .getProperties(ConfigMappings.ConfigClassWithPrefix.configClassWithPrefix(clazz))
                .entrySet()
                .stream()
                .map(ConfigHelper::createConfigProperty);
    }

    private static ConfigProperty createConfigProperty(Map.Entry<String, ConfigMappingInterface.Property> entry) {
        return ConfigProperty
                .builder()
                .key(entry.getKey())
                .envVar(mapToEnvironmentVariable(entry.getKey()))
                .methodName(entry.getValue().getMethod().getName())
                .build();
    }

    private static String mapToEnvironmentVariable(String configKey) {
        return configKey.replaceAll("\\W","_").toUpperCase(Locale.ROOT);
    }
}
