package org.knappi.gachou.core.usecases.authentication.startup;

import io.quarkus.runtime.StartupEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.knappi.gachou.core.usecases.authentication.UserService;
import org.knappi.gachou.core.usecases.authentication.config.AdminUserConfig;
import org.knappi.gachou.core.usecases.config.ConfigHelper;

import javax.enterprise.event.Observes;
import javax.inject.Singleton;

@Singleton
@RequiredArgsConstructor
@Slf4j
public class SetAdminPassword {

    public static final String ENCRYPTED_PASSWORD = ConfigHelper.getConfigKey(AdminUserConfig.class,
            "encryptedPassword"
    );
    private final AdminUserConfig adminUserConfig;

    private final UserService userService;

    void onStart(@Observes StartupEvent ev) {
        adminUserConfig.encryptedPassword().ifPresentOrElse(this::resetAdminPassword,
                () -> log.warn(String.format(
                        "'%s' is not configured. Skipping admin password reset",
                        ENCRYPTED_PASSWORD
                ))
        );
    }

    private void resetAdminPassword(String modularCryptPassword) {
        log.info("Resetting admin password");
        userService.upsertUserWithModularCryptPassword(adminUserConfig.username(),
                modularCryptPassword
        );
    }
}
