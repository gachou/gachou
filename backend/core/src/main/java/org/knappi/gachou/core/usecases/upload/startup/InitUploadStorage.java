package org.knappi.gachou.core.usecases.upload.startup;

import io.quarkus.runtime.StartupEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.knappi.gachou.core.usecases.upload.gateway.UploadStorage;

import javax.enterprise.event.Observes;
import javax.inject.Singleton;

@Singleton
@RequiredArgsConstructor
@Slf4j
public class InitUploadStorage {

    private final UploadStorage uploadStorage;

    void onStart(@Observes StartupEvent ev) {
        uploadStorage.initUploadStorage();
    }
}
