package org.knappi.gachou.core.usecases.authentication.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class EncryptedPassword {
    String salt;
    String hash;
    int iterationCount;
}
