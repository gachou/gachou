package org.knappi.gachou.core.model.authentication;

import lombok.Getter;

@Getter
public class UserAlreadyExistsException extends RuntimeException {
    private final String username;

    public UserAlreadyExistsException(String username) {
        super(username + " already exists!");
        this.username = username;
    }

}
