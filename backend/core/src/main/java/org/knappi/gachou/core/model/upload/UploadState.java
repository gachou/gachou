package org.knappi.gachou.core.model.upload;

public enum UploadState {
    RUNNING,
    DONE,
}
