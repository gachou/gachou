package org.knappi.gachou.core.usecases.authentication;

import javax.inject.Singleton;

@Singleton
public class PasswordEncryptionService {
    public String encryptPasswordToString(char[] password) {
        return PasswordUtil.encryptPasswordToModularCrypt(password);
    }
}
