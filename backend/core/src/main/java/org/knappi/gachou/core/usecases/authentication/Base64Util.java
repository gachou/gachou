package org.knappi.gachou.core.usecases.authentication;

import java.util.Base64;

class Base64Util {
    static String encodeBase64(byte[] rawData) {
        return Base64.getEncoder().encodeToString(rawData);
    }

    static byte[] decodeBase64(String base64) {
        return Base64.getDecoder().decode(base64);
    }
}
