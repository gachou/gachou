# Contributing

## Development setup

### First steps

- Clone this repository
- Run `bin/init-repo.sh`. This will initialize pre-commit hooks for this repo
- If you use IntelliJ Idea, you should "create new module from existing source"
  for `web-ui`, `backend` and `e2e-testing`

## Running latest preview-image

There is a docker-compose-file that allows you to run the project in different
variations. The docker-images are built in the [main-branches CI-pipeline](https://gitlab.com/gachou/gachou/-/pipelines?page=1&scope=all&ref=main)
and stored in the [container registry](https://gitlab.com/gachou/gachou/container_registry),
tagged with `preview`.

### Run all services 

```bash
docker-compose --profile all pull
docker-compose --profile all up
```

### Run everything except web-ui

If you want to develop frontend-changes only, you can run
the frontend manually (see [the web-ui README](web-ui/README.md) for details)
and run the rest of the setup in docker-compose:

```bash
docker-compose --profile no-web-ui pull
docker-compose --profile no-web-ui up
```

### Run everything except backend

If you want to develop backend-changes only, you can run
the backend manually (see [the backend README](backend/README.md) for details)
and run the rest of the setup in docker-compose:

```bash
docker-compose --profile no-backend pull
docker-compose --profile no-backend up
```

### Run only the database

...and start frontend and web-ui yourself.

```bash
docker-compose --profile only-db pull
docker-compose --profile only-db up
```

### Running e2e-tests

With each of the previous options, you can run the end-to-end tests.

> If you run the backend yourself, you have to start it using
> 
> ```bash
> quarkus dev -Dgachou.dangerous-test-setup-access.token=e2e-access-token
> ```

To run the e2e-tests, go to the `e2e-testing` folder and run

```
yarn cypress:open
```
