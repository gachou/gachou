module.exports = {
  root: true,
  plugins: ["cypress"],
  extends: ["eslint:recommended", "plugin:cypress/recommended", "prettier"],
  ignorePatterns: [],
  parserOptions: {
    sourceType: "module",
    ecmaVersion: 2021,
  },
  env: {
    "cypress/globals": true,
  },
  rules: {
    eqeqeq: ["error", "always", { null: "ignore" }],
    "no-console": "error",
  },
  overrides: [
    // typescript files
    {
      files: "**/*.+(ts|tsx)",
      parser: "@typescript-eslint/parser",
      parserOptions: {
        project: "./tsconfig.json",
      },
      plugins: ["@typescript-eslint/eslint-plugin"],
      extends: [
        "plugin:@typescript-eslint/eslint-recommended",
        "plugin:@typescript-eslint/recommended",
      ],
      rules: {
        "@typescript-eslint/no-namespace": "off",
      },
    },
    {
      // Configuration files using NodeJS
      files: ["./.eslintrc.js"],
      parserOptions: {
        sourceType: "script",
        ecmaVersion: 2021,
      },
      env: {
        node: true,
      },
    },
    {
      // Configuration files using NodeJS + TypeScript
      files: ["./cypress/plugins/**/*.ts"],
      parser: "@typescript-eslint/parser",
      parserOptions: {
        project: "./tsconfig.node.json",
      },
      env: {
        node: true,
      },
    },
  ],
};
