# e2e-test

The main docs for running e2e-tests can be found in the main README.

## Running tests against other URLs

```
API_BASE_URL='http://backend.example.com' CYPRESS_BASE_URL='http://web-ui.example.com' yarn cypress:open
```
