describe("user-list and user-details page", () => {
  beforeEach(() => {
    cy.testSetup({
      testUsers: [
        { username: "admin", password: "admin" },
        { username: "bob", password: "pw-bob" },
      ],
    });
    cy.logout();
  });

  it("show a list of users", () => {
    cy.login("admin", "admin");
    cy.visit("/users");
    cy.findAllByTestId("user").as("users");
    cy.get("@users").eq(0).should("contain.text", "admin");
    cy.get("@users").eq(0).should("contain.text", "ADMIN");
    cy.get("@users").eq(1).should("contain.text", "bob");
    cy.get("@users").eq(1).should("contain.text", "USER");
    cy.screenshot("users-list", {
      capture: "viewport",
      overwrite: true,
    });
  });

  it("allows adding a user", () => {
    cy.login("admin", "admin");
    cy.visit("/users");
    cy.findByText("Add user", { exact: false }).should("be.visible").click();

    cy.findByLabelText("Username:").type("new-user");
    cy.findByLabelText("Password:").type("new-pass");
    cy.findByLabelText("Password confirmation:").type("new-pass");
    cy.screenshot("create-user-page", {
      capture: "viewport",
      overwrite: true,
    });
    cy.findByText("Create user", { selector: "button" }).click();

    cy.logout();
    cy.login("new-user", "new-pass");
  });

  it("allows changing a password", () => {
    cy.login("admin", "admin");
    cy.visit("/users");
    cy.findAllByTestId("user").eq(1).as("bob");
    cy.get("@bob").should("contain.text", "bob");
    cy.get("@bob").findByText("Change password", { exact: false }).click();
    cy.findByLabelText("Password:").type("new-pass");
    cy.findByLabelText("Password confirmation:").type("new-pass");
    cy.screenshot("edit-user-page", {
      capture: "viewport",
      overwrite: true,
    });
    cy.findByText("Save").click();

    cy.logout();
    cy.login("bob", "new-pass");
  });

  it("allows removing users", () => {
    cy.login("admin", "admin");
    cy.visit("/users");
    cy.findAllByTestId("user").eq(1).as("bob");
    cy.get("@bob").should("contain.text", "bob");
    cy.get("@bob").findByText("Delete", { exact: false }).click();
    cy.get("@bob").should("not.exist");

    cy.logout();
    cy.login("bob", "pw-bob", { allowFailure: true });
    cy.contains("Login failed");
  });
});
