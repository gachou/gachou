import { Filename, Fixture } from "../../support/commands/test-data";

describe("ths upload-page", () => {
  beforeEach(() => {
    cy.testSetup({
      testUsers: [
        { username: "bob", password: "pw-bob" },
        { username: "admin", password: "admin" },
      ],
    });
    cy.logout();
  });

  it("files can be uploaded by clicking the 'upload' button", () => {
    cy.login("bob", "pw-bob");
    cy.visit("/uploads");

    cy.findByLabelText("Add files", { exact: false }).uploadTestData([
      "basic/img_0119.jpg",
      "basic/2016-08-02__11-00-53-p1050073.jpg",
      "basic/IMG_20160401_202342.jpg",
      "basic/no-xmp-identifier.jpg",
    ]);

    cy.findByText("img_0119.jpg");
    cy.findByText("2016-08-02__11-00-53-p1050073.jpg");
    cy.findByText("IMG_20160401_202342.jpg");
    cy.findByText("no-xmp-identifier.jpg");
    cy.findAllByText("100%", { trim: true }).should("have.length", 4);
    cy.findByText("21.907 KB");

    getCancelButtonForFile("img_0119.jpg").click();
    cy.findByRole("table").should("not.contain.text", "img_0119.jpg");
  });
});

function getCancelButtonForFile(filename: Filename<Fixture>) {
  return cy.findByText(filename).closest("tr").findByText("Cancel");
}
