describe("login", () => {
  beforeEach(() => {
    cy.testSetup({
      testUsers: [
        { username: "bob", password: "pw-bob" },
        { username: "admin", password: "admin" },
      ],
    });
    cy.logout();
  });

  it("shows login page", () => {
    cy.visit("/");
    cy.contains("Please sign in");
  });

  it("access is granted on for the correct password, shows login page on logout", () => {
    cy.visit("/");
    // The login command is intentionally not used here. It may be optimized in the future
    // and not do the real testing anymore.
    cy.findByLabelText("Username").type("bob");
    cy.findByLabelText("Password").type("pw-bob");
    cy.findByText("Login").click();
    cy.contains("Homepage");

    // Should still be logged in after reload
    cy.reload();
    cy.contains("Homepage");

    // The logout command is intentionally not used here. We want to test the logout procedure
    // The command may be optimized in the future and not do any testing anymore.
    cy.findByTestId("user-menu").within(() => {
      cy.findByRole("button").contains("User:").click();
      cy.findByText("Logout").click();
    });
    cy.contains("Please sign in");
  });

  it("access is denied on for the wrong password", () => {
    cy.visit("/");
    // The login command is intentionally not used here. It may be optimized in the future
    // and not do the real testing anymore.
    cy.findByLabelText("Username").type("bob");
    cy.findByLabelText("Password").type("wrong-password");
    cy.findByText("Login").click();
    cy.contains("Login failed");
  });
});
