const MY_PASSWORD = "my-password";
describe("admin-user", () => {
  beforeEach(() => {
    cy.testSetup({
      testUsers: [],
    });
    cy.logout();
  });

  it("shows a modal with instructions for configuring an admin user", () => {
    cy.visit("/");
    cy.updateStatusNow();
    cy.contains("Create an admin user");

    // Make screenshot for docs
    cy.findByTestId("modal").within(() => {
      cy.findByPlaceholderText("Password").type(MY_PASSWORD);
      cy.findByText("Encrypt password").click();
      cy.findByTitle("Copy environment variable to clipboard")
        .find("button")
        .click();
    });
    cy.contains("Copied to clipboard");
    cy.screenshot("create-admin-user-modal", {
      capture: "viewport",
      overwrite: true,
    });

    // Copy password to clipboard
    cy.findByTestId("modal").within(() => {
      cy.findByTitle("Copy encrypted password to clipboard")
        .find("button")
        .click();
    });
    cy.contains("Copied to clipboard");
    // Verify other entries
    cy.getClipboardText().then((text) => {
      cy.findByTitle("Copy config entry to clipboard")
        .find("input")
        .should(
          "have.value",
          "gachou.auth.admin-user.encrypted-password=" + text
        );
      cy.findByTitle("Copy environment variable to clipboard")
        .find("input")
        .should(
          "have.value",
          "GACHOU_AUTH_ADMIN_USER_ENCRYPTED_PASSWORD=" + text
        );

      cy.testSetup({
        testUsers: [
          {
            username: "admin",
            password: text,
            passwordIsModularCryptFormat: true,
          },
        ],
      });
    });
    cy.updateStatusNow();
    cy.findByTestId("modal").should("not.exist");
    cy.login("admin", MY_PASSWORD);
  });
});
