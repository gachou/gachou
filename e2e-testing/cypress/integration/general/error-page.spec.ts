describe("error-page", () => {
  const uncaughtExceptionHandler = (): false | void => {
    return false;
  };

  beforeEach(() => {
    Cypress.on("uncaught:exception", uncaughtExceptionHandler);
    cy.testSetup({
      testUsers: [
        { username: "bob", password: "pw-bob" },
        { username: "admin", password: "admin" },
      ],
    });
  });

  afterEach(() => {
    Cypress.off("uncaught:exception", uncaughtExceptionHandler);
  });

  it("access is granted on for the correct password, shows login page on logout", () => {
    cy.login("bob", "pw-bob");
    cy.visit("/test/error");
    cy.contains("Something went wrong");
    cy.contains("Test-Error");
  });
});
