declare global {
  interface Window {
    updateStatusNow: () => Promise<void>;
  }

  namespace Cypress {
    interface Chainable {
      updateStatusNow: () => void;
    }
  }
}

Cypress.Commands.add("updateStatusNow", () =>
  cy.window().then((window) => window.updateStatusNow())
);

export {};
