import { loadFixture } from "@gachou/testdata";

export type Fixture = Parameters<typeof loadFixture>[0];
export type Filename<T extends string> = T extends `${string}/${infer Name}`
  ? Filename<Name>
  : T;

declare global {
  namespace Cypress {
    interface Chainable<Subject> {
      uploadTestData(name: Fixture | Fixture[]): Chainable<Subject>;
    }
  }
}

Cypress.Commands.add(
  "uploadTestData",
  { prevSubject: true },
  (subject, fixture) => {
    return cy
      .task<Fixture[]>("loadTestDataFixtures", corceToArray(fixture))
      .then((filePath) =>
        cy.wrap(subject).selectFile(filePath, { force: true })
      )
      .then(() => subject);
  }
);

function corceToArray<T>(item: T | T[]): T[] {
  return Array.isArray(item) ? item : [item];
}
