/// <reference types="cypress" />

declare global {
  namespace Cypress {
    interface Chainable {
      login(
        username: string,
        password: string,
        options?: { allowFailure?: boolean }
      ): void;
      logout(): void;
    }
  }
}

const AUTH_LOCAL_STORAGE_KEY = "gachou-auth";

Cypress.Commands.add(
  "login",
  (username, password, { allowFailure = false } = {}) => {
    Cypress.log({ message: "login" });
    cy.visit("/");
    cy.findByLabelText("Username").type(username);
    cy.findByLabelText("Password").type(password);
    cy.findByText("Login").click();
    if (!allowFailure) {
      cy.findByTestId("user-menu").contains("User: " + username);
    }
  }
);

Cypress.Commands.add("logout", () => {
  Cypress.log({ message: "logout" });
  localStorage.removeItem(AUTH_LOCAL_STORAGE_KEY);
});

export {};
