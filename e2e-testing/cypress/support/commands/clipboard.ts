declare global {
  interface Window {
    TEST_CLIPBOARD: string;
  }

  namespace Cypress {
    interface Chainable {
      getClipboardText(): Chainable<string>;
    }
  }
}

Cypress.Commands.add("getClipboardText", () => {
  return cy.window().then((window) => {
    const result = window.TEST_CLIPBOARD;
    Cypress.log({ displayName: "clipboard: ", message: result });
    return result;
  });
});

export class MockClipBoard extends EventTarget implements Clipboard {
  readonly window: Cypress.AUTWindow;

  constructor(window: Cypress.AUTWindow) {
    super();
    this.window = window;
  }

  async read(): Promise<ClipboardItems> {
    throw new Error("clipboard: read() not implemented");
  }

  async readText(): Promise<string> {
    return this.window.TEST_CLIPBOARD || "";
  }

  async write(): Promise<void> {
    throw new Error("clipboard: write() not implemented");
  }

  async writeText(data: string): Promise<void> {
    this.window.TEST_CLIPBOARD = data;
  }

  static install(window: Cypress.AUTWindow) {
    Object.defineProperty(window.navigator, "clipboard", {
      value: new MockClipBoard(window),
      writable: true,
      configurable: true,
    });
  }
}

export function setupMockClipboard() {
  Cypress.on("window:before:load", (window) => {
    MockClipBoard.install(window);
  });
}
