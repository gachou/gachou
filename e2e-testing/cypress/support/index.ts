import "@testing-library/cypress/add-commands";
import "./commands";
import { setupMockClipboard } from "./commands/clipboard";

beforeEach(() => {
  setupMockClipboard();
});
