/* eslint-disable no-console */

import * as Cypress from "cypress";
import { loadFixture } from "@gachou/testdata";
import { Fixtures } from "@gachou/testdata/dist/generated";
import * as path from "path";
import * as fs from "fs";

const testDataDir = path.join(__dirname, "..", "..", "gachou-testdata");
if (!fs.existsSync(testDataDir)) {
  fs.mkdirSync(testDataDir);
}

const pluginConfig: Cypress.PluginConfig = (on, config) => {
  console.log("loading plugin");
  if (process.env.API_BASE_URL != null) {
    config.env.API_BASE_URL = process.env.API_BASE_URL;
    console.log("Overriding API_BASE_URL with " + config.env.API_BASE_URL);
  }

  on("task", {
    loadTestDataFixture,
    loadTestDataFixtures,
  });
  return config;
};

async function loadTestDataFixture(fixture: keyof Fixtures) {
  const target = path.join(testDataDir, fixture);
  return loadFixture(fixture).andCopyTo(target);
}

async function loadTestDataFixtures(fixtures: Array<keyof Fixtures>) {
  return Promise.all(fixtures.map(loadTestDataFixture));
}

module.exports = pluginConfig;
