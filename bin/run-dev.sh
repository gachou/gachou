#!/bin/bash

export GACHOU_DANGEROUS_TEST_SETUP_ACCESS_TOKEN=e2e-access-token

x-terminal-emulator -e "docker-compose --profile only-db up" &
( cd backend && x-terminal-emulator -e "quarkus dev") &
( cd web-ui && x-terminal-emulator -e "yarn dev" ) &
