#!/bin/bash

set -e

(
  echo "Building backend"
  cd backend
  mvn clean -Dmaven.test.skip install
  docker build -t registry.gitlab.com/gachou/gachou/backend:local -f docker/Dockerfile .
)

(
  echo "Building frontend"
  cd web-ui
  yarn build --emptyOutDir
  docker build -t registry.gitlab.com/gachou/gachou/web-ui:local -f docker/Dockerfile .
)