#!/bin/bash

BRANCH="$( git rev-parse --abbrev-ref HEAD )"

if [[ "${BRANCH}" = "main" ]] ; then
  export DOCKER_IMAGE_TAG=preview
else
  export DOCKER_IMAGE_TAG=mr--${BRANCH}
fi

echo "DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG}"

docker-compose --profile all pull
x-terminal-emulator -e "docker-compose --profile all up" &